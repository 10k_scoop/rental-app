import {
  GET_CARS,
  GET_ERRORS,
  GET_HOST_CARS,
  GET_USER_DETAILS,
  POST_CARS,
  SEND_MESSAGE,
} from "../../types/types";
import * as firebase from "firebase";
const baseUrl = "https://aspectlifestyle.herokuapp.com/";
//const baseUrl = "http://172.20.10.6:5000/";

// Backend Api
// export const postCar = (data, setLoading, setStatus) => async (dispatch) => {
//   try {

//     await fetch(baseUrl + "postACar", {
//       method: "POST",
//       headers: {
//         Accept: "application/json",
//         "Content-Type": "application/json",
//       },
//       body: JSON.stringify(data),
//     })
//       .then((response) => response.text())
//       .then(async (res) => {
//         setLoading(false);
//         setStatus(JSON.parse(res));
//         dispatch({ type: POST_CARS, payload: res });
//       })
//       .catch((err) => {
//         setLoading(false);
//         console.log("error happened");
//         console.log(err);
//         setStatus({ msg: err.message, code: 500 });
//         dispatch({ type: GET_ERRORS, payload: err.message });
//       });
//   } catch (err) {
//     console.log("error happened ouside");
//     console.log(err);
//     setLoading(false);
//     setStatus({ msg: "error", code: 500 });
//     dispatch({ type: GET_ERRORS, payload: err.message });
//   }
// };

export const getUserDetails = (id, setLoading) => async (dispatch) => {
  try {
    await fetch(baseUrl + "user", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ uid: id }),
    })
      .then((response) => response.text())
      .then(async (res) => {
        setLoading(false);

        dispatch({ type: GET_USER_DETAILS, payload: JSON.parse(res) });
      })
      .catch((err) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: err.message });
      });
  } catch (err) {
    console.log(err);
    setLoading(false);
    dispatch({ type: GET_ERRORS, payload: err.message });
  }
};

export const getHostCars = (email, setLoading) => async (dispatch) => {
  try {
    await fetch(baseUrl + "getHostCars?email=" + email, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.text())
      .then(async (res) => {
        setLoading(false);
        dispatch({ type: GET_HOST_CARS, payload: JSON.parse(res) });
      })
      .catch((err) => {
        setLoading(false);
        console.log(err.message);
        dispatch({ type: GET_ERRORS, payload: err.message });
      });
  } catch (err) {
    console.log(err);
    setLoading(false);
    dispatch({ type: GET_ERRORS, payload: err.message });
  }
};

export const getCars = (setStatus, setLoading) => async (dispatch) => {
  try {
    await fetch(baseUrl + "getCars", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: { token: "123" },
    })
      .then((response) => response.text())
      .then(async (res) => {
        setLoading(false);
        dispatch({ type: GET_CARS, payload: JSON.parse(res) });
      })
      .catch((err) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: err.message });
      });
  } catch (err) {
    console.log(err);
    setLoading(false);
    dispatch({ type: GET_ERRORS, payload: err.message });
  }
};

export const postCar = (data, setLoading, setStatus) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    let front = firebase.storage().ref("images/cars/front" + data.id + ".jpg");
    let rear = firebase.storage().ref("images/cars/rear" + data.id + ".jpg");
    let side = firebase.storage().ref("images/cars/side" + data.id + ".jpg");
    let interior = firebase
      .storage()
      .ref("images/cars/interior" + data.id + ".jpg");

    front
      .put(data.front)
      .then((snapshot) => {
        front
          .getDownloadURL()
          .then((url) => {
            data.front = url;
            rear
              .put(data.rear)
              .then((snapshot) => {
                rear
                  .getDownloadURL()
                  .then((url) => {
                    data.rear = url;
                    side
                      .put(data.side)
                      .then((snapshot) => {
                        side
                          .getDownloadURL()
                          .then((url) => {
                            data.side = url;
                            interior
                              .put(data.interior)
                              .then((snapshot) => {
                                interior
                                  .getDownloadURL()
                                  .then((url) => {
                                    data.interior = url;
                                    db.collection("cars")
                                      .add(data)
                                      .then((res) => {
                                        setStatus({
                                          msg: "car added",
                                          code: 200,
                                        });
                                        dispatch({
                                          type: POST_CARS,
                                          payload: {
                                            msg: "car added",
                                            code: 200,
                                          },
                                        });
                                        setLoading(false);
                                      })
                                      .catch((error) => {
                                        dispatch({
                                          type: GET_ERRORS,
                                          payload: {
                                            msg: "car add Error",
                                            code: 500,
                                          },
                                        });
                                        setLoading(false);
                                      });
                                  })
                                  .catch((e) => {
                                    setLoading(false);
                                    console.log(e.message);
                                    console.log("error interior download url");
                                  });
                              })
                              .catch((e) => {
                                setLoading(false);
                                console.log("error interior upload");
                              });
                          })
                          .catch((e) => {
                            setLoading(false);
                            console.log("error side download url");
                          });
                      })
                      .catch((e) => {
                        setLoading(false);
                        console.log("error side upload");
                      });
                  })
                  .catch((e) => {
                    setLoading(false);
                    console.log("error rear download url");
                  });
              })
              .catch((e) => {
                setLoading(false);
                console.log("error rear upload");
              });
          })
          .catch((e) => {
            setLoading(false);
            console.log("error front download url");
          });
      })
      .catch((e) => {
        setLoading(false);
        console.log("error front upload");
      });
  } catch (err) {
    console.log(err.message);
    setLoading(false);
    setStatus({ msg: "error", code: 500 });
    dispatch({ type: GET_ERRORS, payload: err.message });
  }
};

export const sendMessage = (data, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("messages")
      .add(data)
      .then((docRef) => {
        dispatch({ type: SEND_MESSAGE, payload: "Message Sent" });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
      });
  } catch (err) {
    console.log(err);
    setLoading(false);
    dispatch({ type: GET_ERRORS, payload: err.message });
  }
};

export const getMessage =
  (convoId, setLoading, setMsgs, user1, user2) => async (dispatch) => {
    var db = firebase.firestore();
    try {
      db.collection("messages")
        .where("convoId", "==", convoId)
        .orderBy("time", "desc")
        .limit(3)
        .onSnapshot((querySnapshot) => {
          var msgs = [];
          querySnapshot.forEach((doc) => {
            msgs.push(doc.data());
          });
          setLoading(false);
          setMsgs(msgs);
        });
    } catch (err) {
      console.log(err);
      setLoading(false);
      dispatch({ type: GET_ERRORS, payload: err.message });
    }
  };
export const payment = (convoId, setLoading) => async (dispatch) => {

}
