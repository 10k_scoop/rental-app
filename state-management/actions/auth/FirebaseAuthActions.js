import {
  DO_LOGIN,
  GET_ERRORS,
  DO_LOGOUT,
  GET_USER_DETAILS,
  DO_SIGNUP,
  PHONE_VERFICATION,
  PHONE_VERFICATION_CODE,
} from "../../types/types";
import * as firebase from "firebase";

const baseUrl = "https://aspectlifestyle.herokuapp.com/";
//const baseUrl="http://172.20.10.6:5000/"
export const verifyPhoneNumber =
  (number, recaptchaVerifier, setVerificationId, setVerifiedStatus) =>
  async (dispatch) => {
    try {
      const phoneProvider = new firebase.auth.PhoneAuthProvider();
      const verificationId = await phoneProvider.verifyPhoneNumber(
        "+" + number,
        recaptchaVerifier.current
      );
      setVerificationId(verificationId);
      setVerifiedStatus({ res: "Code Sent", code: 201 });
    } catch (err) {
      dispatch({ type: GET_ERRORS, payload: { msg: err.message } });
      setVerifiedStatus({ res: "Code Error", code: 500 });
    }
  };

export const verifyCode =
  (code, id, setLoading, setVerifiedStatus) => async (dispatch) => {
    try {
      const credential = firebase.auth.PhoneAuthProvider.credential(id, code);
      const authResult = await firebase.auth().signInWithCredential(credential);
      getUserData(authResult.user.uid, setLoading, setVerifiedStatus);
    } catch (err) {
      console.log(err.message)
      setLoading(false);
      dispatch({ type: GET_ERRORS, payload: { msg: err.message } });
      setVerifiedStatus({ res: err.message, code: 500, data: null });
    }
  };

export const logout = (setStatus) => async (dispatch) => {
  firebase
    .auth()
    .signOut()
    .then(async () => {
      // Sign-out successful.
      setStatus(200);
    })
    .catch((error) => {
      // An error happened.
      setStatus(500);
    });
};

export const verifyEmail =
  (data, setVerifiedStatus, setEmailLoading) => async (dispatch) => {
    try {
      await fetch(baseUrl + "verifyEmail", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.text())
        .then(async (res) => {
          if (res.msg == "Email_already_verified") {
            setVerifiedStatus({ res: "Email_already_verified", code: 200 });
            setEmailLoading(false);
          } else {
            getUserDataByEmail(data, setVerifiedStatus, res, setEmailLoading);
          }
        })
        .catch((err) => {
          setEmailLoading(false);
          setVerifiedStatus({ res: err.message, code: 500 });
        });
    } catch (err) {
      console.log(err);
      setEmailLoading(false);
      setVerifiedStatus({ res: err.message, code: 500 });
    }
  };
export const createUserWithEmailAndPassword =
  (data, email, password, setLoading, setStatus) => async (dispatch) => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        // Signed in
        var user = userCredential.user;
        console.log(user.uid);
        createUserProfile(data, user.uid, setLoading, setStatus);
        // ...
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        setLoading(false);
        setStatus({ msg: errorMessage, code: 500 });
        // ..
      });
  };

const createUserProfile = async (data, uid, setLoading, setStatus) => {
  try {
    await fetch(baseUrl + "createLinkedUser", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ uid: uid, data }),
    })
      .then((response) => response.text())
      .then(async (res) => {
        setLoading(false);
        setStatus(JSON.parse(res));
      })
      .catch((err) => {
        setLoading(false);
        console.log("err");
        setStatus({ msg: err.message, code: 500 });
      });
  } catch (err) {
    console.log(err);
    setLoading(false);
    setStatus({ msg: "error", code: 500 });
  }
};

//Link two accounts
export const linkAccountToEmailAndPassword =
  (data, email, password, setLoading, setStatus) => async (dispatch) => {
    try {
      const credential = firebase.auth.EmailAuthProvider.credential(
        email,
        password
      );
      const user = firebase.auth().currentUser;
      user
        .linkWithCredential(credential)
        .then((usercred) => {
          var user = usercred.user.uid;
          console.log("Account linking success");
          createUserProfile(data, user, setLoading, setStatus);
        })
        .catch((error) => {
          console.log("Account linking error", error);
          setLoading(false);
        });
    } catch (error) {
      console.log(error.message);
      setLoading(false);
    }
  };

const getUserDataByEmail = async (
  data,
  setVerifiedStatus,
  server_res,
  setEmailLoading
) => {
  console.log(data)
  try {
    await fetch(baseUrl + "userByEmail", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.text())
      .then(async (res) => {
        setVerifiedStatus({
          res: "Code Sent",
          code: 201,
          server_res: JSON.parse(server_res),
          userData: JSON.parse(res),
        });
        setEmailLoading(false);
      })
      .catch((err) => {
        console.log(err.message)
        setVerifiedStatus({ res: "Something went Wrong!", code: 500, data: {} });
        setEmailLoading(false);
      });
  } catch (err) {
    setVerifiedStatus({ res: "Code send failed try again!", code: 500, data: {} });
    setEmailLoading(false);
  }
};

const getUserData = async (id, setLoading, setVerifiedStatus) => {
  try {
    await fetch(baseUrl + "user", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ uid: id }),
    })
      .then((response) => response.text())
      .then(async (res) => {
        setLoading(false);
        setVerifiedStatus({
          res: "Verification Successfull, Please continue to next screen...",
          code: 200,
          data: id,
          userData: JSON.parse(res),
        });
      })
      .catch((err) => {
        setLoading(false);
        setVerifiedStatus({ res: err.message, code: 500, data: id });
      });
  } catch (err) {
    console.log(err);
    setLoading(false);
    setVerifiedStatus({ res: err.message, code: 500, data: id });
  }
};
export const loginWithEmail =
  (data, setLoading, setVerifiedStatus) => async (dispatch) => {
    try {
      firebase
        .auth()
        .signInWithEmailAndPassword(data.email, data.password)
        .then((userCredential) => {
          // Signed in
          var user = userCredential.user;
          setLoading(false);
          setVerifiedStatus({ msg: "LoggedInWithEmail", code: 200 });
          // ...
        })
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          setLoading(false);
          setVerifiedStatus({ msg: errorMessage, code: 200 });
        });
    } catch (error) {
      setLoading(false);
      setVerifiedStatus({ msg: errorMessage, code: 200 });
    }
  };
