import {
  DO_LOGIN,
  DO_LOGOUT,
  DO_SIGNUP,
  GET_USER_DETAILS,
  PHONE_VERFICATION,
  PHONE_VERFICATION_CODE,
  POST_CARS,
  GET_HOST_CARS,
  GET_CARS,
  SEND_MESSAGE,
} from "../types/types";
const initialState = {
  login_details: null,
  logout: null,
  signup_details: null,
  get_user_details: null,
  phone_verification: null,
  phone_verification_code: null,
  post_cars: null,
  get_host_cars: null,
  get_cars: null,
  send_message: null,
};
const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case DO_LOGIN:
      return {
        ...state,
        login_details: action.payload,
      };
    case DO_LOGOUT:
      return {
        ...state,
        logout: action.payload,
      };
    case DO_SIGNUP:
      return {
        ...state,
        signup_details: action.payload,
      };

    case GET_USER_DETAILS:
      return {
        ...state,
        get_user_details: action.payload,
      };
    case PHONE_VERFICATION:
      return {
        ...state,
        phone_verification: action.payload,
      };
    case PHONE_VERFICATION_CODE:
      return {
        ...state,
        phone_verification_code: action.payload,
      };
    case POST_CARS:
      return {
        ...state,
        post_cars: action.payload,
      };
    case GET_HOST_CARS:
      return {
        ...state,
        get_host_cars: action.payload,
      };
    case GET_CARS:
      return {
        ...state,
        get_cars: action.payload,
      };
    case SEND_MESSAGE:
      return {
        ...state,
        send_message: action.payload,
      };
    default:
      return state;
  }
};
export default mainReducer;
