import React, { useEffect, useRef, useState } from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../components/BottomMenu";
import {
  AntDesign,
  FontAwesome,
  FontAwesome5,
  Ionicons,
  MaterialCommunityIcons,
  MaterialIcons,
} from "@expo/vector-icons";
import * as Animatable from "react-native-animatable";
import VisaCard from "../../components/VisaCard";
import { LinearGradient } from "expo-linear-gradient";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import * as firebase from "firebase";
const Settings = (props) => {
  const PaymemntpopUp = useRef(0);
  const [showPopup, setShowPopup] = useState(false);
  const [indicatorPos, setIndicatorPos] = useState(0);
  const [status, setStatus] = useState(null);
  const user = firebase.auth().currentUser;
  const onPaymentOpen = async () => {
    if (!showPopup) {
      setShowPopup(true);
      if (PaymemntpopUp.current) await PaymemntpopUp.current.bounceIn(500);
    } else {
      if (PaymemntpopUp.current) await PaymemntpopUp.current.bounceOut(500);
      setShowPopup(false);
    }
  };
  const onLogout = () => {
    props.logout(setStatus);
  };
  useEffect(() => {
    if (status == 200) {
      props.navigation.navigate("LoadingScreen");
    } else if (status == 500) {
      alert("Something went wrong");
    } else {
    }
  }, [status]);

  return (
    <View style={styles.container}>
      <View style={styles.sectionWrapper}>
        {/*Header */}
        <View style={styles.header}>
          <View style={styles.profileWrapper}>
            <View style={styles.profileImg}>
              <Image
                source={require("../../assets/Images/img38.png")}
                resizeMode="cover"
                style={{ width: "100%", height: "100%" }}
              />
            </View>
            <View style={styles.verifiedBadge}>
              <Image
                source={require("../../assets/Images/ic_verified.png")}
                resizeMode="cover"
                style={{ width: "100%", height: "100%" }}
              />
            </View>
          </View>
          <Text style={styles.profileTitle}>Exotic Motors</Text>
        </View>

        {/* Action Buttons */}
        <ScrollView>
          <View style={styles.actionWrapper}>
            <View style={styles.actionsRow}>
              <TouchableOpacity
                style={styles.actionsBtn}
                onPress={() => onPaymentOpen()}
              >
                <MaterialIcons name="payment" size={rf(35)} color="#0F73EE" />
                <Text style={styles.actionsBtnTitle}>
                  Add Payment{"\n"} Method
                </Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.actionsBtn}>
                <MaterialCommunityIcons
                  name="folder-multiple-plus"
                  size={rf(32)}
                  color="#0F73EE"
                />
                <Text style={styles.actionsBtnTitle}>
                  Add Insurance{"\n"} Info
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.actionsRow}>
              <TouchableOpacity style={styles.actionsBtn}>
                <FontAwesome name="user-circle" size={rf(30)} color="#0F73EE" />
                <Text style={styles.actionsBtnTitle}>Profile</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.actionsBtn}>
                <FontAwesome name="dollar" size={rf(28)} color="#0F73EE" />
                <Text style={styles.actionsBtnTitle}>
                  Invite {"\n"} Friends & Family
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.actionsRow}>
              <TouchableOpacity style={styles.actionsBtn}>
                <Ionicons
                  name="md-help-buoy-outline"
                  size={rf(30)}
                  color="#0F73EE"
                />
                <Text style={styles.actionsBtnTitle}>Help</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.actionsBtn}>
                <MaterialCommunityIcons
                  name="cog-outline"
                  size={rf(30)}
                  color="#0F73EE"
                />
                <Text style={styles.actionsBtnTitle}>Settings</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.actionsRow}>
              <TouchableOpacity style={styles.actionsBtn}>
                <MaterialIcons
                  name="switch-camera"
                  size={rf(32)}
                  color="#0F73EE"
                />
                <Text style={styles.actionsBtnTitle}>
                  Switch to {"\n"}Dark Mode
                </Text>
              </TouchableOpacity>
              {user && (
                <TouchableOpacity style={styles.actionsBtn} onPress={onLogout}>
                  <AntDesign name="logout" size={rf(30)} color="#0F73EE" />
                  <Text style={styles.actionsBtnTitle}>Logout</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
          {/* Action Buttons */}
        </ScrollView>
      </View>

      {/*Bottom Menu */}
      <BottomMenu
        navigation={props.navigation}
        activeItem="Settings"
        user={user}
        userData={props.get_user_details}
      />

      {/* Payment Popup */}

      <Animatable.View
        ref={PaymemntpopUp}
        style={[styles.paymentSelectionMain, { zIndex: showPopup ? 999 : -1 }]}
      >
        {showPopup && (
          <View style={styles.paymentSelectionMain}>
            <TouchableOpacity
              style={styles.BGLayer1}
              onPress={() => onPaymentOpen()}
              activeOpacity={0}
            ></TouchableOpacity>
            <View style={styles.paymentSelectionWrapper}>
              {/* borderBar */}
              <View style={styles.borderBar}></View>
              {/* Row Head */}
              <View style={styles.paymentSectionHead}>
                <Text style={styles.paymentSectionTitle}>Payment</Text>
                <TouchableOpacity
                  style={{ flexDirection: "row" }}
                  onPress={() => {
                    setShowPopup(false);
                    props.navigation.navigate("AddPaymentMethod");
                  }}
                >
                  <AntDesign
                    name="plus"
                    size={rf(13)}
                    color="black"
                    style={{ right: 10 }}
                  />
                  <Text style={styles.paymentSectionHeadTagLine}>
                    Add New Method
                  </Text>
                </TouchableOpacity>
              </View>
              {/* Row Head */}
              {/* Cards Selection */}
              <View style={styles.visaCardWrapper}>
                <ScrollView
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  pagingEnabled
                  onScroll={(e) =>
                    setIndicatorPos(e.nativeEvent.contentOffset.x)
                  }
                >
                  <VisaCard bgColor="#F26C68" />
                  <VisaCard bgColor="#8688BC" />
                  <VisaCard bgColor="#7AA0DA" />
                </ScrollView>
              </View>
              {/* Cards Selection */}
              {/* Cards Indicators */}
              <View style={styles.cardIndicatorWrapper}>
                <View
                  style={[
                    styles.cardIndicator,
                    {
                      backgroundColor:
                        indicatorPos < 200 ? "#989898" : "#D8D8D8",
                    },
                  ]}
                ></View>
                <View
                  style={[
                    styles.cardIndicator,
                    {
                      backgroundColor:
                        indicatorPos > 220 && indicatorPos < 460
                          ? "#989898"
                          : "#D8D8D8",
                    },
                  ]}
                ></View>
                <View
                  style={[
                    styles.cardIndicator,
                    {
                      backgroundColor:
                        indicatorPos > 500 ? "#989898" : "#D8D8D8",
                    },
                  ]}
                ></View>
              </View>
              {/* Cards Indicators */}
              {/* payment Button */}
              <TouchableOpacity style={styles.paymentButton}>
                <LinearGradient
                  // Button Linear Gradient
                  colors={["#0F73EE", "#C644FC"]}
                  start={{ x: 0, y: 0.75 }}
                  end={{ x: 1.7, y: 0.25 }}
                  style={{
                    width: "110%",
                    height: "110%",
                    position: "absolute",
                  }}
                />
                <Text style={styles.paymentButtonText}>
                  Select Payment Method
                </Text>
              </TouchableOpacity>
              {/* payment Button */}
            </View>
          </View>
        )}
      </Animatable.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FF",
  },
  sectionWrapper: {
    flex: 1,
  },
  header: {
    width: wp("100%"),
    height: hp("35%"),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
    paddingTop: hp("5%"),
  },
  profileImg: {
    width: wp("25%"),
    height: wp("25%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  verifiedBadge: {
    width: 25,
    height: 25,
    position: "absolute",
    right: 3,
    bottom: -3,
  },
  profileWrapper: {
    width: wp("25%"),
    height: wp("25%"),
  },
  profileTitle: {
    fontSize: rf(22),
    fontFamily: "MM",
    color: "#020433",
    marginTop: hp("3%"),
  },
  profileTagline: {
    fontSize: rf(13),
    fontFamily: "MR",
    color: "#404B69",
    marginTop: hp("0.5%"),
  },
  actionWrapper: {
    width: wp("100%"),
    paddingHorizontal: wp("7%"),
    marginTop: hp("1%"),
    flex: 1,
    marginBottom: hp("10%"),
  },
  actionsRow: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: hp("2%"),
  },
  actionsBtn: {
    width: "40%",
    height: hp("14%"),
    backgroundColor: "#fff",
    borderRadius: 12,
    alignItems: "center",
    justifyContent: "center",
  },
  iconWrapper: {
    width: "50%",
    height: "30%",
  },
  actionsBtnTitle: {
    fontSize: rf(12),
    fontFamily: "MB",
    color: "#020433",
    textAlign: "center",
    marginTop: "7%",
    top: 5,
  },
  paymentBoxText: {
    fontFamily: "MR",
    fontSize: rf(15),
  },
  paymentSelectionWrapper: {
    width: wp("100%"),
    height: hp("62%"),
    position: "absolute",
    bottom: 0,
    borderColor: "#CDCDCD",
    overflow: "hidden",
    backgroundColor: "#F9F9FB",
    borderWidth: 1,
    borderColor: "#CDCDCD",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    bottom: -hp("8%"),
  },
  visaCardWrapper: {
    marginTop: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
  },
  paymentSelectionMain: {
    width: wp("100%"),
    height: "100%",
    position: "absolute",
  },
  BGLayer1: {
    width: wp("100%"),
    height: "100%",
    backgroundColor: "#222",
    position: "absolute",
    opacity: 0,
  },
  borderBar: {
    width: "15%",
    height: 3,
    backgroundColor: "#CDCDCD",
    alignSelf: "center",
    borderRadius: 10,
    marginVertical: hp("2%"),
  },
  paymentSectionHead: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
    paddingHorizontal: wp("4%"),
  },
  paymentSectionTitle: {
    fontSize: rf(18),
    fontFamily: "MB",
    color: "#424347",
  },
  paymentSectionHeadTagLine: {
    fontSize: rf(12),
    fontFamily: "MR",
    color: "#3E3E3E",
  },

  cardIndicatorWrapper: {
    marginVertical: hp("3%"),
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  cardIndicator: {
    width: 8,
    height: 8,
    borderRadius: 10,
    marginHorizontal: 3,
  },
  paymentButton: {
    width: wp("80%"),
    height: hp("7%"),
    backgroundColor: "#F5CA48",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    bottom: 5,
    overflow: "hidden",
  },
  paymentButtonText: {
    fontSize: rf(15),
    fontFamily: "MB",
    color: "#fff",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details:state.main.get_user_details
});
export default connect(mapStateToProps, { logout })(Settings);
