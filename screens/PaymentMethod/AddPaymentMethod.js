import {
  AntDesign,
  Entypo,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import React, { useRef, useState } from "react";
import {
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import BackButton from "../../components/BackButton";
import BottomMenu from "../../components/BottomMenu";
import SelectedButtons from "../../components/SelectionButtons";
import { CardField, StripeProvider, useConfirmPayment } from '@stripe/stripe-react-native';

import { connect } from "react-redux";
import * as firebase from "firebase";
import VisaCard from "../../components/VisaCard";

const AddPaymentMethod = (props) => {

  const baseUrl = "https://aspectlifestyle.herokuapp.com/";
  //const baseUrl="http://172.20.10.6:5000/"

  const [card, setCard] = useState({});
  const [loader, setLoader] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  const { confirmPayment, loading } = useConfirmPayment();
  const user = firebase.auth().currentUser;
console.log(props.route.params?.TripDates.startDate)
  const fetchPaymentIntentClientSecret = async () => {
    try {
      let orderData = {
        currency: 'usd',
        amount: eval(props.route.params?.data?.pricePerDay * 100),
        hostName: props.route.params?.data?.hostName,
        host: props.route.params?.data?.host,
        id: props.route.params?.data?.id,
        startDate: props.route.params?.TripDates.startDate,
        endDate: props.route.params?.TripDates.endDate
      }
      const response = await fetch(baseUrl + 'create-payment-intent', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(orderData),
      });
      const { clientSecret, code } = await response.json();
      return clientSecret;
    } catch (e) {
      setErrorMsg("Something went wrong!")
      console.log(e.message)
    }
  };

  const handlePayPress = async () => {

    try {
      setLoader(true)
      if (!card.complete) {
        return;
      }
      // Gather the customer's billing information (e.g., email)
      const billingDetails = {
        email: user.email ? user.email : props.get_user_details.data.email,
        fullname: props.get_user_details.data.fullName,
        id: props.get_user_details.data.id,
        currency: 'usd',
        startDate: props.route.params?.TripDates.startDate,
        endDate: props.route.params?.TripDates.endDate,
        item: props.route.params.data
      };
      // Fetch the intent client secret from the backend
      const clientSecret = await fetchPaymentIntentClientSecret();

      // Confirm the payment with the card details
      const { paymentIntent, error } = await confirmPayment(clientSecret, {
        type: 'Card',
        billingDetails,
      });

      if (error) {
        console.log('Payment confirmation error', error.message);
        setErrorMsg("Payment confirmation error, Try again!")
        setLoader(false)
      } else if (paymentIntent) {
        setErrorMsg("")
        var db = firebase.firestore();
        db.collection("orders")
          .add(billingDetails)
          .then((docRef) => {
            console.log('Success from promise');
            if (paymentIntent.status == "Succeeded") {
              props.navigation.navigate("Booked", { data: billingDetails })
            }
            setLoader(false)
          })
          .catch((error) => {
            setLoader(false)
            console.log(error.message)
          });
      }
    } catch (e) {
      setErrorMsg("Something went wrong!")
      console.log(e.message)
    }

  };


  return (
    <View style={styles.container}>
      {/* Header */}
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === "ios" ? "padding" : "height"}
      >
        <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='always'>
          <View style={{ flex: 1, height: hp("82%") }}>
            {/* Header */}
            <View style={styles.title}>
              <View style={{ marginVertical: hp("3%") }}>
                <BackButton navigation={props.navigation} />
              </View>
              <Text style={[styles.titleTagline, { color: 'red' }]}>{errorMsg} </Text>
              <Text style={styles.titleHeading}>Payment</Text>
            </View>

            {/* Header */}
            <View style={{ marginVertical: 20 }}>
              <VisaCard bgColor="orange" last4={card.last4} expiry={card.expiryMonth + "/" + card.expiryYear} />
            </View>
            {/* Form */}
            <View style={{ flex: 1 }}>
              <StripeProvider
                publishableKey="pk_test_51Jyiv3A35MqmL7JoKn74Emihnpv3885uOTuNdyRcb7GdInyUeYTwdYSkPINrCi9rDGRb0kICvTxBaJNk5VSbYq0S00awq9kMoq"
              keyboardShouldPersistTaps="never"
              >
                <CardField
                  postalCodeEnabled={false}
                  placeholder={{
                    number: '4242 4242 4242 4242',
                  }}
                  cardStyle={{
                    backgroundColor: '#FFFFFF',
                    textColor: '#000000',
                  }}

                  style={{
                    width: '88%',
                    height: 50,
                    marginHorizontal: wp('6%'),
                    borderWidth: 1,
                    borderColor: '#cdcdcd',
                    borderRadius: 10
                  }}
                  onCardChange={(cardDetails) => {
                    console.log('cardDetails', cardDetails);
                    setCard(cardDetails)
                  }}
                  onFocus={(focusedField) => {
                    console.log('focusField', focusedField);
                  }}
                />
              </StripeProvider>
            </View>

            {/* Form */}

            <TouchableOpacity
              onPress={() => card.complete ? handlePayPress() : null}
              style={styles.addPaymentMethodButton}
            >
              <LinearGradient
                // Button Linear Gradient
                colors={["#0F73EE", "#C644FC"]}
                start={{ x: 0, y: 0.75 }}
                end={{ x: 1.7, y: 0.25 }}
                style={{
                  width: "110%",
                  height: "110%",
                  position: "absolute",
                  opacity: card.complete == true ? 1 : 0.5
                }}
              />
              <View

                style={{ flexDirection: "row" }}
              >
                {!loader ?
                  <Text style={styles.addPaymentMethodButtonText}>
                    Pay
                  </Text>
                  :
                  <ActivityIndicator size="small" color="white" />
                }
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>

      {/* Bottom Menu */}
      <BottomMenu
        navigation={props.navigation}
        onSavedPress={() => props.navigation.navigate("Saved")}
        activeItem="Settings"
      />
      {/* Bottom Menu */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F9F9FB",
  },
  titleTagline: {
    fontFamily: "MR",
    fontSize: rf(13),
    marginTop: 10,
  },
  titleHeading: {
    fontFamily: "MB",
    fontSize: rf(26),
  },
  title: {
    height: hp("25%"),
    backgroundColor: "white",
    paddingHorizontal: wp("5%"),
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderWidth: 2,
    borderColor: "#0F73EE",
    paddingTop: hp("2%"),
  },
  formWrapper: {
    paddingHorizontal: wp("5%"),
    marginVertical: hp("3%"),
    marginBottom: hp("4%"),
  },
  inputWrapper: {
    width: "100%",
    height: hp("13%"),
  },
  inputWrapperInner: {
    backgroundColor: "#EAECEE",
    width: "100%",
    height: hp("7%"),
    paddingLeft: wp("4%"),
    paddingRight: wp("7%"),
    borderRadius: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  inputLabel: {
    fontSize: rf(13),
    fontFamily: "MB",
    marginBottom: hp("1%"),
  },
  inputField: {
    color: "#929DA9",
    fontSize: rf(13),
    fontFamily: "MM",
    flex: 1,
  },
  addPaymentMethodButton: {
    width: wp("90%"),
    height: hp("8%"),
    backgroundColor: "#F5CA48",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp("0%"),
    borderRadius: 20,
    alignSelf: "center",
    overflow: "hidden",
  },
  addPaymentMethodButtonText: {
    fontFamily: "MB",
    fontSize: rf(15),
    color: "#fff",
    marginLeft: 10,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details
});
export default connect(mapStateToProps, {})(AddPaymentMethod);
