import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";

export default function Card(props) {
  return (
    <TouchableOpacity style={styles.cardWrapper} onPress={props.onPress}>
      <View style={styles.cardWrapperThumbnail}>
        <Image
          source={props.data.thumbnail}
          resizeMode="cover"
          style={{ width: "100%", height: "100%" }}
        />
        <TouchableOpacity style={styles.heartIcon}>
          <Ionicons name="ios-heart" size={rf(22)} color="#F44E4E" />
        </TouchableOpacity>
      </View>

      <View style={styles.cardWrapperInfo}>
        <View style={styles.cardVerifiedWrapper}>
          <MaterialIcons name="verified-user" size={rf(14)} color="#08C299" />
          <Text style={styles.cardVerifiedText}>VERIFIED</Text>
        </View>

        <Text style={styles.cardItemTitle}>{props.data.title}</Text>
        <TouchableOpacity style={styles.cardBtn}>
          <LinearGradient
            // Button Linear Gradient
            colors={["#0F73EE", "#C644FC"]}
            // start={{ x: 0.44, y: 0.1 }}
            end={{ x: 1.4, y: 2 }}
            style={styles.layer}
          />
          <Text style={styles.cardItemPrice}>{props.data.price}</Text>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardWrapper: {
    width: "100%",
    height: hp("48%"),
    alignItems: "flex-start",
  },
  cardWrapperThumbnail: {
    width: "100%",
    height: "70%",
    overflow: "hidden",
  },
  cardVerifiedWrapper: {
    flexDirection: "row",
    alignItems: "center",
  },
  cardVerifiedText: {
    fontSize: rf(10),
    fontFamily: "MM",
    color: "#08C299",
    left: 6,
  },
  cardWrapperInfo: {
    marginTop: "5%",
    paddingHorizontal: wp("5%"),
  },
  cardItemTitle: {
    fontSize: rf(18),
    fontFamily: "MB",
    color: "#020433",
    marginTop: 10,
  },
  cardItemPrice: {
    fontSize: rf(13),
    fontFamily: "MS",
    color: "#fff",
  },
  heartIcon: {
    position: "absolute",
    right: "5%",
    top: "5%",
  },
  cardBtn: {
    width: wp("48%"),
    height: hp("4%"),
    borderRadius: 5,
    backgroundColor: "#222",
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    top: 10,
  },
  layer: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
});
