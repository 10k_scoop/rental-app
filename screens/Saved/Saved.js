import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../components/BottomMenu";
import { FeaturedListData } from "../../constants/FeaturedListData";
import Card from "./components/Card";
import { SelectedForYouData } from "../../constants/SelectedForYouData";
export default function Saved(props) {
  const [activatedTab, setActivatedTab] = useState("cars");

  return (
    <View style={styles.container}>
      {/*Header */}

      <View style={styles.header}>
        <Text style={styles.title}>Saved</Text>
        {/* Tabs */}
        <View style={styles.tabWrapper}>
          <TouchableOpacity
            onPress={() => setActivatedTab("cars")}
            style={[
              styles.tabItem,
              {
                borderBottomWidth: activatedTab == "cars" ? 2 : 0,
              },
            ]}
          >
            <Text
              style={[
                styles.tabItemText,
                { color: activatedTab == "cars" ? "#0F73EE" : "#DADADA" },
              ]}
            >
              Cars
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setActivatedTab("homes")}
            style={[
              styles.tabItem,
              {
                borderBottomWidth: activatedTab == "homes" ? 2 : 0,
              },
            ]}
          >
            <Text
              style={[
                styles.tabItemText,
                { color: activatedTab == "homes" ? "#0F73EE" : "#DADADA" },
              ]}
            >
              Homes
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setActivatedTab("experiences")}
            style={[
              styles.tabItem,
              {
                borderBottomWidth: activatedTab == "experiences" ? 2 : 0,
              },
            ]}
          >
            <Text
              style={[
                styles.tabItemText,
                {
                  color: activatedTab == "experiences" ? "#0F73EE" : "#DADADA",
                },
              ]}
            >
              Experiences
            </Text>
          </TouchableOpacity>
        </View>
        {/* Tabs */}
      </View>
      {/* Header */}

      <View style={styles.section}>
        <ScrollView>
          {activatedTab == "cars" ? (
            <View style={styles.cardWrapperMain}>
              {FeaturedListData.map((item, index) => {
                return (
                  <Card
                    key={index}
                    data={item}
                    onPress={() =>
                      props.navigation.navigate("CarDetails", { data: item })
                    }
                  />
                );
              })}
            </View>
          ) : activatedTab == "homes" ? (
            <View style={styles.cardWrapperMain}>
              {SelectedForYouData.map((item, index) => {
                return (
                  <Card
                    key={index}
                    data={item}
                    onPress={() =>
                      props.navigation.navigate("CarDetails", { data: item })
                    }
                  />
                );
              })}
            </View>
          ) : (
            <Text
              style={{
                fontSize: rf(15),
                fontFamily: "MM",
                textAlign: "center",
                top: 20,
              }}
            >
              No data found
            </Text>
          )}
        </ScrollView>
      </View>

      {/*Bottom Menu */}
      <BottomMenu navigation={props.navigation} activeItem="Saved" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  header: {
    width: wp("100%"),
    height: hp("20%"),
    alignItems: "center",
    justifyContent: "flex-end",
    backgroundColor: "#fff",
    paddingTop: hp("5%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  title: {
    fontSize: rf(16),
    fontFamily: "MM",
    color: "#020433",
    flex: 1,
    top: "25%",
  },
  tabWrapper: {
    width: "100%",
    flexDirection: "row",
  },
  tabItem: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#0F73EE",
    height: hp("5%"),
  },
  tabItemText: {
    fontSize: rf(13),
    fontFamily: "MM",
    color: "#0F73EE",
  },
  section: {
    width: "100%",
    marginTop: 10,
    flex: 1,
    marginBottom: hp("10%"),
  },
});
