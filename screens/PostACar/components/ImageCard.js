import React, { useRef, useState } from "react";
import { Image, StyleSheet, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function ImageCard(props) {
  return (
    <TouchableOpacity style={styles.addNewImageCard}>
      <Image
        source={{ uri: props.url }}
        resizeMode="cover"
        style={{ width: "100%", height: "100%", borderRadius: 8 }}
      />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  addNewImageCard: {
    width: wp("40"),
    height: hp("18%"),
    borderRadius: 8,
    marginTop: hp("2%"),
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    backgroundColor: "red",
  },
});
