import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, MaterialIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import { FancyAlert } from "react-native-expo-fancy-alerts";
export default function Popup(props) {
  return (
    <FancyAlert
      visible={props.visible}
      icon={
        <View
          style={{
            flex: 1,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 50,
            width: "100%",
            overflow: "hidden",
          }}
        >
          <LinearGradient
            // Button Linear Gradient
            colors={["#0F73EE", "#C644FC"]}
            // start={{ x: 0.44, y: 0.1 }}
            end={{ x: 1.4, y: 2 }}
            style={{ width: "100%", height: "100%", position: "absolute" }}
          />
          <MaterialIcons name="celebration" size={24} color="white" />
        </View>
      }
      style={{ backgroundColor: "white" }}
    >
      <Text
        style={{
          marginTop: -16,
          marginBottom: 32,
          fontFamily: "MB",
          fontSize: rf(14),
        }}
      >
        {props?.msg || 'Car Listed Successfully'}
      </Text>
      <TouchableOpacity
        style={[styles.doneBtnWrapper, { width: "100%", marginBottom: "5%" }]}
        onPress={props.onPress}
      >
        <LinearGradient
          // Button Linear Gradient
          colors={["#0F73EE", "#C644FC"]}
          // start={{ x: 0.44, y: 0.1 }}
          end={{ x: 1.4, y: 2 }}
          style={[styles.doneBtn, { height: hp("5%") }]}
        >
          <Text style={styles.doneBtnText}>Continue</Text>
        </LinearGradient>
      </TouchableOpacity>
    </FancyAlert>
  );
}

const styles = StyleSheet.create({
  doneBtnWrapper: {
    justifyContent: "flex-end",
    paddingHorizontal: wp("5%"),
    alignItems: "center",
  },
  doneBtn: {
    width: "100%",
    height: hp("6%"),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  doneBtnText: {
    fontFamily: "MS",
    fontSize: rf(14),
    color: "#fff",
  },
});
