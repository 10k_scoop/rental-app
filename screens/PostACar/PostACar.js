import React, { useEffect, useRef, useState } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  prompt,
  Image,
  KeyboardAvoidingView,
} from "react-native";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "../../components/Header";
import TextField from "./components/TextField";
import { ScrollView } from "react-native-gesture-handler";
import { AntDesign } from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import { LinearGradient } from "expo-linear-gradient";
import { connect } from "react-redux";
import { postCar } from "../../state-management/actions/Features/Actions";
import Popup from "./components/Popup";
import * as firebase from "firebase";
import * as Location from "expo-location";
const PostACar = (props) => {
  const [title, setTitle] = useState(null);
  const [pricePerDay, setPricePerDay] = useState(null);
  const [milesAllowPerDay, setMilesAllowPerDay] = useState(null);
  const [pricePerMileOverLimit, setPricePerMileOverLimit] = useState(null);
  const [make, setMake] = useState(null);
  const [model, setModel] = useState(null);
  const [tripDetails, setTripDetails] = useState(null);
  const [vin, setVin] = useState(null);
  const [year, setYear] = useState(null);
  const [fuelType, setFuelType] = useState(null);
  const [engine, setEngine] = useState(null);
  const [transmission, setTransmission] = useState(null);
  const [color, setColor] = useState(null);
  const [description, setDescription] = useState(null);
  const [country, setCountry] = useState(null);

  const [state, setState] = useState(null);
  const [city, setCity] = useState(null);
  const [zipCode, setZipCode] = useState(null);
  const [streetAdress, setStreetAdress] = useState(null);

  const [front, setfront] = useState(null);
  const [side, setSide] = useState(null);
  const [rear, setRear] = useState(null);
  const [interior, setInterior] = useState(null);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState({});
  const [showErrorPop, setShowErrorPop] = useState(false);
  const [error, setError] = useState(false);
  const [visible, setVisible] = useState(false);
  const user = firebase.auth().currentUser;
  const [errorPopMsg, setErrorPopMsg] = useState("");
  const [selectedLocation, setSelectedLocation] = useState();

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);
  useEffect(() => {
    if (status?.msg == "car added") {
      setVisible(true);
    }
  }, [status]);

  const onSubmit = async () => {
    if (front != null && rear != null && side != null && interior != null) {
      const response1 = await fetch(front);
      const frontImg = await response1.blob();
      const response2 = await fetch(rear);
      const sideImg = await response2.blob();
      const response3 = await fetch(side);
      const rearImg = await response3.blob();
      const response4 = await fetch(interior);
      const interiorImg = await response4.blob();

      //let getLoc = await Location.geocodeAsync(city);

      let data = {
        latitude: selectedLocation.latitude,
        longitude: selectedLocation.longitude,
        title: title,
        pricePerDay: pricePerDay,
        milesAllowPerDay: milesAllowPerDay,
        pricePerMileOverLimit: pricePerMileOverLimit,
        tripDetails: tripDetails,
        make: make,
        model: model,
        year: year,
        vin: vin,
        fuelType: fuelType,
        engine: engine,
        transmission: transmission,
        color: color,
        description: description,
        front: frontImg,
        side: sideImg,
        rear: rearImg,
        interior: interiorImg,
        country: country,
        state: state,
        city: city,
        zipCode: zipCode,
        streetAdress: streetAdress,
        host: user.email,
        hostName: props.get_user_details?.data?.fullName,
        id: user.email + Math.floor(Math.random() * 15457525),
      };
      if (
        title == null ||
        pricePerDay == null ||
        milesAllowPerDay == null ||
        pricePerMileOverLimit == null ||
        tripDetails == null ||
        make == null ||
        model == null ||
        year == null ||
        vin == null ||
        fuelType == null ||
        engine == null ||
        transmission == null ||
        description == null ||
        front == null ||
        interior == null ||
        side == null ||
        rear == null ||
        country == null ||
        state == null ||
        streetAdress == null ||
        zipCode == null
      ) {
        if (user.email == null || user.email == "") {
          setShowErrorPop(true);
          setErrorPopMsg("Something Went Wrong Try again!");
        } else {
          setError(null);
          setLoading(true);
          props.postCar(data, setLoading, setStatus);
        }
      } else {
        setError("Fill all details");
      }
      setError(null);
      setLoading(true);
      props.postCar(data, setLoading, setStatus);
    } else {
      console.log("add images");
    }
  };

  const pickImage = async (type) => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0.4,
    });

    if (!result.cancelled) {
      type == "front"
        ? setfront(result.uri)
        : type == "side"
        ? setSide(result.uri)
        : type == "rear"
        ? setRear(result.uri)
        : setInterior(result.uri);
    }
  };

  return (
    <View style={styles.container}>
      <Header navigation={props.navigation} title="List car" />
      <Popup
        navigation={props.navigation}
        visible={visible}
        onPress={() => {
          setVisible(false);
          props.navigation.replace("HostCenter");
        }}
      />

      <Popup
        navigation={props.navigation}
        visible={showErrorPop}
        onPress={() => {
          setShowErrorPop(false);
          props.navigation.replace("HostCenter");
        }}
        msg={errorPopMsg}
      />

      <View style={styles.section1}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.section1Title}>List{"\n"}Your Car</Text>
          {error && (
            <Text
              style={[
                styles.section1Title,
                { color: "red", fontSize: 20, top: 5 },
              ]}
            >
              {error}
            </Text>
          )}
          {/* Title field */}
          <TextField
            label="Title"
            onChangeText={(val) => setTitle(val)}
            value={"Title for your car"}
            keyboardType="default"
          />
          {/* Title field */}
          <Text style={styles.section1Title2}>Price</Text>
          {/* Price Field */}
          <TextField
            label="Price per day "
            onChangeText={(val) => setPricePerDay(val)}
            value={"$"}
            keyboardType="number-pad"
          />

          {/* Miles Field */}
          <TextField
            label="Miles allows per day"
            onChangeText={(val) => setMilesAllowPerDay(val)}
            value={"75"}
            keyboardType="number-pad"
          />
          {/* Price per mile over limit Field */}
          <TextField
            label="Price per mile over limit "
            onChangeText={(val) => setPricePerMileOverLimit(val)}
            value={"$1"}
            keyboardType="number-pad"
          />
          {/* Trip Details Field */}
          <TextField
            label="Trip Details"
            onChangeText={(val) => setTripDetails(val)}
            value="Trip Details"
            multiline
            keyboardType="default"
          />
          <Text style={styles.section1Title2}>Vehicle information</Text>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            {/* Make  Field */}
            <TextField
              label="Make "
              onChangeText={(val) => setMake(val)}
              value={"Enter Make"}
              keyboardType="default"
              width="48%"
            />
            {/* Model  Field */}
            <TextField
              label="Model "
              onChangeText={(val) => setModel(val)}
              value={"Enter Model"}
              keyboardType="default"
              width="48%"
            />
          </View>
          {/* Year  Field */}
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <TextField
              label="Year"
              onChangeText={(val) => setYear(val)}
              value={"2022"}
              keyboardType="number-pad"
              width="48%"
            />
            <TextField
              label="VIN"
              onChangeText={(val) => setVin(val)}
              value={"Enter VIN code"}
              keyboardType="number-pad"
              width="48%"
            />
          </View>

          <TextField
            label="Fuel type"
            onChangeText={(val) => setFuelType(val)}
            value={"Octane"}
            keyboardType="default"
          />
          <TextField
            label="Engine"
            onChangeText={(val) => setEngine(val)}
            value={"Enter engine model"}
            keyboardType="default"
          />
          <TextField
            label="Transmission "
            onChangeText={(val) => setTransmission(val)}
            value={"Automatic"}
            keyboardType="default"
          />
          <TextField
            label="Color "
            onChangeText={(val) => setColor(val)}
            value={"Enter color"}
            keyboardType="default"
          />

          {/* About Me Field */}
          <TextField
            label="Description"
            onChangeText={(val) => setDescription(val)}
            value="Describe your model"
            multiline
            keyboardType="default"
          />
          <Text style={styles.section1Title2}>Photos / video</Text>
          {/*Add Images Section */}
          <View style={styles.imagesSection}>
            {/*Add New Image Card */}
            <View style={styles.imagesWrapper}>
              <TouchableOpacity
                style={styles.addNewImageCard}
                onPress={() => pickImage("front")}
              >
                {front && (
                  <Image source={{ uri: front }} style={styles.carImages} />
                )}
                <Text style={styles.imagesSectionTitle}>Front photo</Text>
                <AntDesign name="plus" size={rf(30)} color="#0F73EE" />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.addNewImageCard}
                onPress={() => pickImage("side")}
              >
                {side && (
                  <Image source={{ uri: side }} style={styles.carImages} />
                )}
                <Text style={styles.imagesSectionTitle}>Side photo</Text>
                <AntDesign name="plus" size={rf(30)} color="#0F73EE" />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.addNewImageCard}
                onPress={() => pickImage("rear")}
              >
                {rear && (
                  <Image source={{ uri: rear }} style={styles.carImages} />
                )}
                <Text style={styles.imagesSectionTitle}>Rear photo</Text>
                <AntDesign name="plus" size={rf(30)} color="#0F73EE" />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.addNewImageCard}
                onPress={() => pickImage("interior")}
              >
                {interior && (
                  <Image source={{ uri: interior }} style={styles.carImages} />
                )}
                <Text style={styles.imagesSectionTitle}>Interior photo</Text>
                <AntDesign name="plus" size={rf(30)} color="#0F73EE" />
              </TouchableOpacity>
            </View>
          </View>
          {/*Add Images Section */}

          {/*Location Section */}
          <Text style={styles.section1Title2}>Location</Text>
          <View style={{ marginBottom: hp("2%") }}>
            {/* Location  Field */}
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <TextField
                label="Zip code"
                onChangeText={(val) => setZipCode(val)}
                value={"Enter Zip code"}
                keyboardType="number-pad"
                width="48%"
              />
              <TextField
                label="Street address"
                onChangeText={(val) => setStreetAdress(val)}
                multiline
                keyboardType="default"
              />
            </View>
            {/*Street address Field */}
          </View>
          <Text style={styles.section1Title2}>Select Location</Text>
          <View style={styles.mapWrapper}>
            <MapView
              region={{
                latitude: 37.0902,
                longitude: -95.7129,
                latitudeDelta: 0.1,
                longitudeDelta: 0.17,
              }}
              style={{ flex: 1 }}
              showsUserLocation
            >
              <Marker
                draggable
                coordinate={{
                  latitude: 37.0902,
                  longitude: -95.7129,
                  latitudeDelta: 0.1,
                  longitudeDelta: 0.17,
                }}
                onDragEnd={(e) => setSelectedLocation(e.nativeEvent.coordinate)}
              />
            </MapView>
          </View>
        </ScrollView>
        {/* Next Button */}
        <View style={styles.BottomButtonWrapper}>
          <TouchableOpacity
            style={styles.doneBtnWrapper}
            onPress={() => (loading ? null : onSubmit())}
          >
            <LinearGradient
              // Button Linear Gradient
              colors={["#0F73EE", "#C644FC"]}
              // start={{ x: 0.44, y: 0.1 }}
              end={{ x: 1.4, y: 2 }}
              style={styles.doneBtn}
            >
              {loading ? (
                <ActivityIndicator color="white" size="small" />
              ) : (
                <Text style={styles.doneBtnText}>Submit</Text>
              )}
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  section1: {
    marginTop: hp("4%"),
    paddingHorizontal: wp("5%"),
    paddingHorizontal: wp("5%"),
    width: wp("100%"),
    flex: 1,
  },
  mapWrapper: {
    width: wp("100%"),
    height: hp("40%"),
    marginTop: 10,
  },
  section1Title: {
    fontSize: rf(28),
    color: "#020433",
    fontFamily: "MB",
  },
  section1Title2: {
    fontSize: rf(16),
    color: "#020433",
    fontFamily: "MB",
    marginVertical: 5,
    top: 10,
  },
  imagesSection: {
    width: "100%",
  },
  imagesSectionTitle: {
    fontSize: rf(12),
    color: "red",
    fontFamily: "MM",
    position: "absolute",
    top: "20%",
  },
  imagesSectionTitleInner: {
    fontSize: rf(12),
    color: "#FC2F39",
    fontFamily: "MR",
  },
  addNewImageCard: {
    width: wp("40"),
    height: hp("18%"),
    backgroundColor: "#fff",
    borderWidth: 2,
    borderRadius: 8,
    borderColor: "#0F73EE",
    borderStyle: "dashed",
    marginTop: hp("2%"),
    alignItems: "center",
    justifyContent: "center",
  },
  imagesWrapper: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  doneBtnWrapper: {
    justifyContent: "flex-end",
    paddingHorizontal: wp("5%"),
    alignItems: "center",
  },
  doneBtn: {
    width: "100%",
    height: hp("6%"),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  doneBtnText: {
    fontFamily: "MS",
    fontSize: rf(14),
    color: "#fff",
  },
  BottomButtonWrapper: {
    width: wp("100%"),
    backgroundColor: "#FFFFFF",
    height: hp("10%"),
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    right: wp("5%"),
  },
  carImages: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  post_cars: state.main.post_cars,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { postCar })(PostACar);
