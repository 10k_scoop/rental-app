import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import BottomMenu from "../../components/BottomMenu";

export default function TransactionHistory(props) {
  return (
    <View style={styles.container}>
      <Header title="Transaction History" navigation={props.navigation} />

      <View style={styles.sectionWrapper}></View>
      {/*Bottom Menu */}
      <BottomMenu
        navigation={props.navigation}
        onSavedPress={() => props.navigation.navigate("Saved")}
        activeItem="Host"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  sectionWrapper: {
    flex: 1,
    marginTop: hp("2%"),
    paddingHorizontal: wp("4%"),
  },
});
