import React, { useEffect, useState } from "react";
import {
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo, EvilIcons, SimpleLineIcons } from "@expo/vector-icons";
import BottomMenu from "../../components/BottomMenu";
import Card from "./components/Card";
import { ScrollView } from "react-native-gesture-handler";
import { LinearGradient } from "expo-linear-gradient";
import { FeaturedListData } from "../../constants/FeaturedListData";
import { connect } from "react-redux";
import { getCars } from "../../state-management/actions/Features/Actions";
const ListView = (props) => {
  const [cars, setCars] = useState([]);
  const [status, setStatus] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    props.getCars(setStatus, setLoading);
  }, []);

  useEffect(() => {
    setCars(props.get_cars?.data);
    setStatus(props.get_cars?.code)
    console.log(props.get_cars);
  }, [props]);

  return (
    <View style={styles.container}>
      {/* Header */}
      <TouchableOpacity style={styles.header}>
        <View style={styles.headerIcon}>
          <View style={styles.headerIconLayer}></View>
          <SimpleLineIcons name="location-pin" size={rf(15)} color="#3F4752" />
        </View>
        <Text style={styles.locationText}>Tampa, FL</Text>
        <EvilIcons name="chevron-down" size={rf(22)} color="#3F4752" />
      </TouchableOpacity>
      {/* Header */}

      {loading ? (
        <ActivityIndicator color="black" size="large" />
      ) : (
        <ScrollView>
          <View style={styles.cardWrapperMain}>
            {status==200? (
              <>
                {cars.map((item, index) => {
                  return (
                    <Card
                      key={index}
                      data={item}
                      onPress={() =>
                        props.navigation.navigate("CarDetails", { data: item })
                      }
                    />
                  );
                })}
              </>
            ) : (
              <Text style={{ color: "#222", fontSize:23 }}>No cars found</Text>
            )}
          </View>
        </ScrollView>
      )}
      {/* Floating Action Buttons */}
      <View style={styles.floatingActionsWrapper}>
        <LinearGradient
          // Button Linear Gradient
          colors={["#0F73EE", "#C644FC"]}
          style={{
            width: "110%",
            height: "110%",
            position: "absolute",
          }}
        />
        <View style={styles.borderLayer}>
          <TouchableOpacity
            style={styles.floatingActionBtn}
            onPress={() => props.navigation.navigate("Map")}
          >
            <Text style={styles.floatingActionBtnText}>Map</Text>
            <Entypo name="map" size={rf(15)} color="black" />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.floatingActionBtn}
            onPress={() => props.navigation.navigate("Filter")}
          >
            <Text style={styles.floatingActionBtnText}>Filter</Text>
            <Image
              source={require("../../assets/Images/ic_filter.png")}
              resizeMode="contain"
              style={{ width: 20, height: 20 }}
            />
          </TouchableOpacity>
        </View>
      </View>

      {/* Bottom Menu */}
      <BottomMenu
        navigation={props.navigation}
        onSavedPress={() => props.navigation.navigate("Saved")}
        activeItem="Home"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  header: {
    backgroundColor: "#fff",
    width: wp("100%"),
    height: (Platform.OS = "ios" ? hp("8%") : hp("7%")),
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    marginTop: (Platform.OS = "ios"
      ? StatusBar.currentHeight + hp("4%")
      : StatusBar.currentHeight + hp("1%")),
    paddingHorizontal: wp("6%"),
  },
  headerIcon: {
    width: hp("4.5%"),
    height: hp("4.5%"),
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
  },
  headerIconLayer: {
    width: "100%",
    height: "100%",
    borderRadius: 100,
    backgroundColor: "#3F4752",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    opacity: 0.1,
  },
  locationText: {
    fontFamily: "MR",
    fontSize: rf(15),
    color: "#404B69",
    marginHorizontal: wp("2%"),
  },
  cardWrapperMain: {
    width: wp("100%"),
    paddingHorizontal: wp("6%"),
    alignItems: "center",
    marginTop: hp("2%"),
    marginBottom: hp("15%"),
  },
  floatingActionsWrapper: {
    width: wp("45%"),
    height: hp("5%"),
    borderRadius: 100,
    position: "absolute",
    bottom: hp("10%"),
    flexDirection: "row",
    alignSelf: "center",
    padding: 2,
    overflow: "hidden",
  },
  floatingActionBtn: {
    marginHorizontal: 10,
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
  },
  floatingActionBtnText: {
    marginHorizontal: 3,
    fontSize: rf(13),
    fontFamily: "MM",
    color: "#020433",
    right: 5,
  },
  borderLayer: {
    width: "100%",
    height: "100%",
    borderRadius: 100,
    backgroundColor: "white",
    flexDirection: "row",
    padding: 5,
    alignItems: "center",
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_cars: state.main.get_cars,
});
export default connect(mapStateToProps, { getCars })(ListView);
