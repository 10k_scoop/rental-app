import React, { useState } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import { SliderBox } from "react-native-image-slider-box";
export default function Card(props) {
  var images = [
    {uri:props.data?.front},
    {uri:props.data?.rear},
    {uri:props.data?.side},
    {uri:props.data?.interior},
  ];
  return (
    <TouchableOpacity style={styles.cardWrapper} onPress={props.onPress}>
      <View style={styles.cardWrapperThumbnail}>
        <View style={styles.cardScrolllWrapper}>
          <SliderBox
            images={images}
            sliderBoxHeight={hp("28%")}
            dotStyle={{
              width: 8,
              height: 8,
              borderRadius: 15,
              marginHorizontal: 0,
              padding: 0,
              margin: 0,
            }}
            dotColor="#fff"
            inactiveDotColor="#90A4AE"
          />
        </View>
        <TouchableOpacity style={styles.heartIcon}>
          <Ionicons name="ios-heart" size={rf(22)} color="#F44E4E" />
        </TouchableOpacity>
      </View>

      <View style={styles.cardWrapperInfo}>
        <View style={styles.cardVerifiedWrapper}>
          <MaterialIcons name="verified-user" size={rf(14)} color="#08C299" />
          <Text style={styles.cardVerifiedText}>VERIFIED</Text>
        </View>

        <Text style={styles.cardItemTitle}>{props.data.title}</Text>
        <Text style={styles.cardItemPrice}>
          {"$ " + props.data.pricePerDay}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardWrapper: {
    width: "100%",
    height: hp("40%"),
    alignItems: "flex-start",
    backgroundColor: "#fff",
    marginBottom: hp("1%"),
    borderRadius: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    paddingBottom: 10,
  },
  cardWrapperThumbnail: {
    width: "100%",
    height: "70%",
    overflow: "hidden",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  cardScrolllWrapper: {
    width: wp("90%"),
    height: "100%",
    overflow: "hidden",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  cardVerifiedWrapper: {
    flexDirection: "row",
    alignItems: "center",
  },
  cardVerifiedText: {
    fontSize: rf(10),
    fontFamily: "MM",
    color: "#08C299",
    left: 6,
  },
  cardWrapperInfo: {
    marginTop: "5%",
    marginLeft: wp("3%"),
  },
  cardItemTitle: {
    fontSize: rf(18),
    fontFamily: "MB",
    color: "#020433",
    marginTop: "2%",
  },
  cardItemPrice: {
    fontSize: rf(14),
    fontFamily: "MR",
    color: "#020433",
    top: 5,
  },
  heartIcon: {
    position: "absolute",
    right: "5%",
    top: "5%",
  },
  indicatorsWrapper: {
    flexDirection: "row",
    position: "absolute",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "20%",
  },
  indicator: {
    width: 6,
    height: 6,
    backgroundColor: "white",
    borderRadius: 100,
    marginRight: 5,
  },
});
