import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
export default function TripDetails(props) {
  return (
    <View style={styles.container}>
      <Header navigation={props.navigation} title="Trip Detail" />

      <View style={styles.sectionWrapper}>
        <ScrollView>
          <View style={styles.thumbnail}>
            <View style={styles.listingbox}>
              <View style={styles.greenCircle}></View>
              <Text style={styles.thumbnailText1}>Listings</Text>
            </View>
            <Image
              source={require("../../assets/Images/img1.jpg")}
              resizeMode="cover"
              style={{ width: "100%", height: "100%" }}
            />
            <TouchableOpacity style={styles.thumbnailText2Wrapper}>
              <Text style={styles.thumbnailText2}>View Listings</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.infoWrapper}>
            {/* Section 1 */}
            <View style={styles.infoSection}>
              <Text style={styles.modelName}>BMW 3-Series 2021</Text>
              <Text style={styles.modelCode}>1847r8 M345i</Text>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={styles.modelstartText}>5.0</Text>
                <View style={styles.stars}>
                  <Image
                    source={require("../../assets/Images/Star.png")}
                    resizeMode="cover"
                    style={{
                      width: "100%",
                      height: "100%",
                    }}
                  />
                </View>
                <Text style={styles.modelstartText}>3 trips</Text>
              </View>
            </View>
            {/* Section 1 */}

            {/* Section 2 */}
            <View style={styles.infoSection}>
              <Text style={styles.sectionText1}>Trip logistics</Text>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 5,
                }}
              >
                <Text style={styles.sectionText2}>Pick up location</Text>
                <Text style={styles.sectionText3}>LA Airport</Text>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 5,
                }}
              >
                <Text style={styles.sectionText2}>Drop off location</Text>
                <Text style={styles.sectionText3}>Exotic Motors</Text>
              </View>
            </View>
            {/* Section 2 */}

            {/* Section 2 */}
            <View style={styles.infoSection}>
              <Text style={styles.sectionText1}>Trip notes</Text>
              <Text style={[styles.sectionText2, { top: 10 }]}>
                Please be careful when going up or down steep hills and be aware
                of a slight blind spot.{" "}
              </Text>
            </View>
            {/* Section 2 */}

            {/* Section 3 */}
            <View style={styles.bookingDetailsSection}>
              <View style={styles.bookingDetailsRow}>
                <Text style={styles.bookingDetailsRowText1}>
                  Booking amount
                </Text>
                <Text style={styles.bookingDetailsRowText2}>$650</Text>
              </View>

              <View style={styles.bookingDetailsRow}>
                <Text style={styles.bookingDetailsRowText1}>Service free</Text>
                <Text
                  style={[styles.bookingDetailsRowText2, { color: "#0F73EE" }]}
                >
                  Free
                </Text>
              </View>

              <View style={styles.bookingDetailsRow}>
                <Text style={styles.bookingDetailsRowText1}>Add ons</Text>
                <Text style={styles.bookingDetailsRowText2}>$250</Text>
              </View>
            </View>
            {/* Section 3 */}
            <View style={[styles.bookingDetailsRow, { marginTop: hp("2%") }]}>
              <Text style={styles.bookingDetailsRowText1}>Booking amount</Text>
              <Text style={styles.bookingDetailsRowText2}>$950</Text>
            </View>
          </View>
        </ScrollView>
      </View>
      {/*Bottom Menu */}
      <BottomMenu
        navigation={props.navigation}
        onSavedPress={() => props.navigation.navigate("Saved")}
        activeItem="Host"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  sectionWrapper: {
    flex: 1,
  },
  thumbnail: {
    width: "100%",
    height: hp("33%"),
  },
  listingbox: {
    width: "30%",
    height: hp("3%"),
    borderRadius: 100,
    backgroundColor: "#fff",
    position: "absolute",
    zIndex: 9999,
    top: 10,
    left: 10,
    alignItems: "center",
    paddingHorizontal: 10,
    flexDirection: "row",
  },
  greenCircle: {
    width: 10,
    height: 10,
    borderRadius: 100,
    backgroundColor: "lightgreen",
    marginRight: 10,
  },
  thumbnailText1: {
    fontSize: rf(12),
    fontFamily: "MB",
    color: "#404B69",
  },
  thumbnailText2Wrapper: {
    position: "absolute",
    bottom: 5,
    alignItems: "center",
    width: "100%",
  },
  thumbnailText2: {
    fontSize: rf(14),
    fontFamily: "MM",
    color: "#fff",
  },
  infoWrapper: {
    marginVertical: hp("2%"),
    paddingHorizontal: wp("5%"),
  },
  modelName: {
    fontSize: rf(18),
    fontFamily: "MB",
  },
  modelCode: {
    fontSize: rf(13),
    fontFamily: "MM",
    color: "grey",
    marginVertical: 5,
  },
  modelstartText: {
    fontSize: rf(10),
    fontFamily: "MB",
  },
  stars: {
    width: 10,
    height: 10,
    marginHorizontal: 5,
    marginVertical: 3,
  },
  infoSection: {
    width: "100%",
    height: hp("10%"),
    borderColor: "#DCDCDC",
    borderBottomWidth: 1,
    paddingTop: 10,
  },
  bookingDetailsSection: {
    width: "100%",
    borderColor: "#DCDCDC",
    borderBottomWidth: 1,
    paddingTop: 10,
  },
  sectionText1: {
    fontSize: rf(13),
    fontFamily: "MS",
    color: "#404B69",
  },
  sectionText2: {
    fontSize: rf(12),
    fontFamily: "MR",
    color: "#404B69",
  },
  sectionText3: {
    fontSize: rf(12),
    fontFamily: "MB",
    color: "#0F73EE",
  },
  bookingDetailsRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 6,
  },
  bookingDetailsRowText1: {
    fontSize: rf(12),
    fontFamily: "MR",
    color: "#404B69",
  },
  bookingDetailsRowText2: {
    fontSize: rf(12),
    fontFamily: "MB",
    color: "#020433",
  },
});
