import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { colors } from "../../../constants/colors";

export default function ExploreCarCards(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View style={styles.thumbnail}>
        <Image
          source={props.data.thumbnail}
          resize="contain"
          style={{ width: "100%", height: "100%" }}
        />
      </View>

      <View style={styles.categoryNameWrapper}>
        <Text style={styles.categoryName}>{props.data.category}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("66%"),
    height: hp("23%"),
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#DADADA",
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginLeft: wp("5%"),
  },
  thumbnail: {
    width: "100%",
    height: "80%",
    overflow: "hidden",
    borderWidth: 1,
    borderColor: "#979797",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  categoryName: {
    fontSize: rf(13),
    fontFamily: "MB",
    color: colors.textDark,
    marginLeft: wp("3%"),
  },
  categoryNameWrapper: {
    flex: 1,
    justifyContent: "center",
  },
});
