import { Feather } from "@expo/vector-icons";
import React from "react";
import { StyleSheet, Text, TextInput, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function SearchBar(props) {
  return (
    <View style={styles.container}>
      <Feather name="search" size={rf(16)} color="#404B69" />
      <TextInput
        style={styles.input}
        placeholder="Try “Los Angeles” or “McLaren”"
        placeholderTextColor="#9797"
        onFocus={props.onPressIn}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    width: wp("88%"),
    height: hp("5%"),
    borderRadius: 100,
    padding: 10,
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "row",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2.5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 8,
  },
  input: {
    width: "80%",
    height: "80%",
    fontSize: rf(14),
    fontFamily: "MM",
  },
});
