import React, { useState } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, MaterialIcons } from "@expo/vector-icons";
import CountryPicker from "react-native-country-picker-modal";
import { LinearGradient } from "expo-linear-gradient";
import { colors } from "../../../constants/colors";
export default function LoginPopup(props) {
  const [countryCode, setCountryCode] = useState("US");
  const [country, setCountry] = useState(null);
  const [withFlag, setWithFlag] = useState(true);
  const [withFilter, setWithFilter] = useState(true);
  const [withCallingCode, setWithCallingCode] = useState(1);
  const [phone, setPhone] = useState("");

  const onSelect = (country) => {
    setCountryCode(country.cca2);
    setCountry(country);
    console.log(country.callingCode);
    setWithCallingCode(country.callingCode);
    props.countryCode(country.callingCode[0]);
  };

  const onPhoneChange = (val) => {
    var digit = val;
    setPhone(digit.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    props.phoneNumber(val);
  };

  return (
    <View style={styles.loginPopUpMain}>
      <TouchableOpacity
        style={styles.BGLayer1}
        onPress={props.onClosePress}
        activeOpacity={0}
      ></TouchableOpacity>
      <View style={styles.loginWrapper}>
        {/* borderBar */}
        <View style={styles.borderBar}></View>
        {/* header Text */}
        <View style={styles.headerRow}>
          <TouchableOpacity onPress={props.onClosePress}>
            <AntDesign name="close" size={rf(20)} color="#020433" />
          </TouchableOpacity>
          <Text style={styles.headerRowText}>Log in or sign up</Text>
        </View>
        {/* header Text */}

        {/* Phone Field wrapper */}
        {props.authMethod == "phone" && (
          <View style={styles.section2}>
            <View style={styles.phoneField}>
              <Text style={styles.phoneFieldLabel}>Phone Number</Text>
              <View style={styles.countrySelector}>
                <CountryPicker
                  {...{
                    countryCode,
                    withFilter,
                    withFlag,
                    withCallingCode,
                    onSelect,
                  }}
                  visible={false}
                />
                <Text style={[styles.countryCode, { fontSize: rf(18) }]}>
                  +
                </Text>
                <Text style={styles.countryCode}>{withCallingCode}</Text>
                <TextInput
                  style={styles.phoneInput}
                  maxLength={12}
                  onChangeText={(val) => onPhoneChange(val)}
                  value={phone}
                  keyboardType="phone-pad"
                />
              </View>
            </View>
          </View>
        )}
        {/* Phone Field wrapper */}

        {/* Email Field wrapper */}
        {props.authMethod == "email" && (
          <View style={styles.section2}>
            <View style={styles.phoneField}>
              <Text style={[styles.phoneFieldLabel, { bottom: 10 }]}>
                Email address
              </Text>
              <TextInput
                style={[styles.phoneInput, { width: "90%" }]}
                onChangeText={(val) => props.onChangeEmail(val)}
                value={props.email}
                keyboardType="email-address"
              />
            </View>
          </View>
        )}
        {/* Email Field wrapper */}

        {/* Continue wrapper */}
        <TouchableOpacity
          style={styles.continueBtnWrapper}
          onPress={props.onContinuePress}
        >
          <LinearGradient
            // Button Linear Gradient
            colors={["#0F73EE", "#C644FC"]}
            // start={{ x: 0.44, y: 0.1 }}
            end={{ x: 1.4, y: 2 }}
            style={styles.continueBtn}
          >
            <Text style={styles.continueBtnText}>CONTINUE</Text>
          </LinearGradient>
        </TouchableOpacity>
        {/* Continue wrapper */}
        {props.authMethod == "phone" && (
          <Text style={styles.warningText}>Standard messaging rates apply</Text>
        )}
        {/* <Text style={styles.orText}>or</Text> */}

        {/* Signup Actions wrapper */}
        <View style={styles.signupWrapper}>
          {/* <TouchableOpacity
            style={styles.signupBtn}
            onPress={() =>
              props.authMethod == "phone"
                ? props.setAuthMethod("email")
                : props.setAuthMethod("phone")
            }
          >
            <MaterialIcons
              name={props.authMethod == "phone" ? "email" : "phone"}
              size={rf(20)}
              color="#323232"
            />
            <Text style={styles.signupBtnText}>
              Continue with {props.authMethod == "phone" ? "Email" : "Phone"}
            </Text>
          </TouchableOpacity> */}

          {/* <TouchableOpacity
            style={styles.signupBtn}
            onPress={props.onGooglePress}
          >
            <Image source={require("../../../assets/Images/google.jpg")} />
            <Text style={styles.signupBtnText}>Continue with Google</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.signupBtn}
            onPress={props.onApplePress}
          >
            <AntDesign name="apple1" size={rf(20)} color="#323232" />
            <Text style={styles.signupBtnText}>Continue with Apple</Text>
          </TouchableOpacity> */}
        </View>
        {/* Signup Actions wrapper */}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  loginPopUpMain: {
    width: wp("100%"),
    height: "100%",
    position: "absolute",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  BGLayer1: {
    width: wp("100%"),
    height: "100%",
    backgroundColor: "#222",
    position: "absolute",
    opacity: 0,
  },
  borderBar: {
    width: "15%",
    height: 3,
    backgroundColor: "#CDCDCD",
    alignSelf: "center",
    borderRadius: 10,
    marginVertical: hp("2%"),
  },
  loginWrapper: {
    width: wp("100%"),
    height: hp("78%"),
    position: "absolute",
    bottom: 0,
    borderColor: "#CDCDCD",
    overflow: "hidden",
    backgroundColor: "#FFFFFF",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    bottom: -hp("8%"),
  },
  headerRow: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp("5%"),
  },
  headerRowText: {
    fontFamily: "MM",
    fontSize: rf(16),
    color: "#020433",
    flex: 1,
    textAlign: "center",
    right: 10,
  },
  section2: {
    paddingHorizontal: wp("10%"),
    alignItems: "center",
    marginTop: hp("5%"),
  },
  phoneField: {
    width: "100%",
    height: hp("8%"),
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#F0F0F0",
    paddingHorizontal: wp("2%"),
    alignItems: "flex-start",
    justifyContent: "center",
  },
  phoneFieldLabel: {
    fontSize: rf(12),
    fontFamily: "MR",
    color: "#404B69",
    marginTop: 5,
  },
  countrySelector: {
    flexDirection: "row",
    alignItems: "center",
  },
  countryCode: {
    fontSize: rf(14),
    fontFamily: "MM",
    color: "#95A0B6",
  },
  phoneInput: {
    left: 10,
    width: "70%",
    fontSize: rf(16),
    fontFamily: "MM",
    color: "#020433",
  },
  continueBtnWrapper: {
    marginTop: hp("4%"),
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: wp("10%"),
  },
  continueBtn: {
    width: "100%",
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  continueBtnText: {
    fontFamily: "MB",
    fontSize: rf(12),
    color: "#fff",
  },
  warningText: {
    fontFamily: "MS",
    fontSize: rf(10),
    color: "#95A0B6",
    textAlign: "center",
    marginTop: hp("1%"),
  },
  orText: {
    fontFamily: "MM",
    fontSize: rf(14),
    color: "#020433",
    textAlign: "center",
    marginTop: hp("4%"),
    marginBottom: hp("3%"),
  },
  signupWrapper: {
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: wp("10%"),
  },
  signupBtn: {
    width: "100%",
    height: hp("6%"),
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#95A0B6",
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    paddingHorizontal: wp("5%"),
    marginVertical: hp("1.6%"),
  },
  signupBtnText: {
    fontSize: rf(14),
    fontFamily: "MS",
    color: "#404B69",
    right: wp("2.5%"),
    textAlign: "center",
    flex: 1,
  },
});
