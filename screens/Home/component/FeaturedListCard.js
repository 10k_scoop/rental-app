import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { colors } from "../../../constants/colors";
import { MaterialIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
export default function FeaturedListCard(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View style={styles.thumbnail}>
        <Image
          source={props.data.thumbnail}
          resize="contain"
          style={{ width: "100%", height: "100%" }}
        />
      </View>

      <View style={styles.verifiedNameWrapper}>
        <MaterialIcons name="verified-user" size={rf(11)} color="#08C299" />
        <Text style={styles.verifiedText}>VERIFIED</Text>
      </View>
      <View style={styles.titleWrapper}>
        <Text style={styles.titleText}>{props.data.title}</Text>
      </View>
      <TouchableOpacity style={styles.priceButtonWraper}>
        <LinearGradient
          // Button Linear Gradient
          colors={["#0F73EE", "#C644FC"]}
          // start={{ x: 0.44, y: 0.1 }}
          end={{ x: 1.4, y: 2 }}
          style={styles.priceButtonlayer}
        >
          <Text style={styles.priceText}>{props.data.price}</Text>
        </LinearGradient>
      </TouchableOpacity>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("60%"),
    height: hp("33%"),
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#DADADA",
    borderRadius: 20,
    marginLeft: wp("5%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  thumbnail: {
    width: "100%",
    height: "55%",
    overflow: "hidden",
    borderWidth: 1,
    borderColor: "#979797",
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
  },
  verifiedNameWrapper: {
    flexDirection: "row",
    marginTop: "5%",
    marginLeft: wp("3%"),
  },
  verifiedText: {
    fontSize: rf(10),
    fontFamily: "MM",
    color: "#08C299",
    left: 5,
  },
  titleText: {
    fontSize: rf(14),
    fontFamily: "MB",
    color: colors.textMedium,
  },
  titleWrapper: {
    marginVertical: "2%",
    marginLeft: wp("3%"),
  },
  priceButtonWraper: {
    width: "90%",
    height: "15%",
    borderRadius: 10,
    overflow: "hidden",
    borderWidth: 0.7,
    borderColor: "#E5E5E5",
    marginHorizontal: "5%",
    position: "absolute",
    bottom: "4%",
  },
  priceText: {
    fontSize: rf(14),
    fontFamily: "MS",
    color: "#fff",
  },
  priceButtonlayer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
