import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { colors } from "../../../constants/colors";

export default function PopularLocationCard(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View style={styles.thumbnail}>
        <Image
          source={props.data.thumbnail}
          resize="contain"
          style={{ width: "100%", height: "100%" }}
        />
      </View>

      <View style={styles.categoryNameWrapper}>
        <Text style={styles.categoryName}>{props.data.city}</Text>
        <Text style={styles.country}>{props.data.country}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("35%"),
    height: hp("27%"),
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#DADADA",
    borderRadius: 15,
    marginLeft: wp("5%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  thumbnail: {
    width: "100%",
    height: "75%",
    borderTopLeftRadius: 15,
    overflow: "hidden",
    borderTopRightRadius: 15,
  },
  categoryName: {
    fontSize: rf(15),
    fontFamily: "MS",
    color: colors.textDark,
  },
  categoryNameWrapper: {
    flex: 1,
    justifyContent: "center",
    marginLeft: wp("2%"),
  },
  country: {
    fontSize: rf(10),
    fontFamily: "MR",
    color: colors.textMedium,
    top: 2,
  },
});
