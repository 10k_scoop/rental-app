import React, { useRef, useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Platform,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import SearchBar from "./component/SearchBar";
import { colors } from "../../constants/colors";
import { ScrollView } from "react-native-gesture-handler";
import ExploreCarCards from "./component/ExploreCarCards";
import { ExploreData } from "../../constants/ExploreData";
import PopularLocationCard from "./component/PopularLocationCard";
import { PopularLocationData } from "../../constants/PopularLocationData";
import { FeaturedListData } from "../../constants/FeaturedListData";
import FeaturedListCard from "./component/FeaturedListCard";
import { SelectedForYouData } from "../../constants/SelectedForYouData";
import BottomMenu from "../../components/BottomMenu";
import * as Animatable from "react-native-animatable";
import LoginPopup from "./component/LoginPopup";
import * as firebase from "firebase";
import { connect } from "react-redux";
import { getUserDetails } from "../../state-management/actions/Features/Actions";
import * as Location from "expo-location";
const Home = (props) => {
  const loginpopUp = useRef(0);
  const [showPopup, setShowPopup] = useState(true);
  const [phoneNumber, setPhoneNumber] = useState();
  const [countryCode, setCountryCode] = useState("+1");
  const [authMethod, setAuthMethod] = useState("phone");
  const [loading, setLoading] = useState(true);
  const [email, setEmail] = useState("");
  const user = firebase.auth().currentUser;
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorLocationMsg] = useState(null);
  const onLoginOpen = async () => {
    if (!showPopup) {
      setShowPopup(true);
      if (loginpopUp.current) await loginpopUp.current.bounceIn(500);
    } else {
      if (loginpopUp.current) await loginpopUp.current.bounceOut(500);
      setShowPopup(false);
    }
  };

  useEffect(() => {
    if (user) {
      setShowPopup(false)
      setLoading(true);
      props.getUserDetails(user.uid, setLoading);
    } else {
      setShowPopup(true)
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);

  useEffect(() => {
    if (!user) {
      //onLoginOpen();
    } else {
      setShowPopup(false);
    }
  }, [user]);

  useEffect(() => {
    console.log(props.get_user_details);
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="balck" />
      </View>
    );
  }


  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.sectionsWrapper}>
          {/* Search Bar */}
          <View style={styles.SearchBarWrapper}>
            <SearchBar onPressIn={() => props.navigation.navigate("Search",{location:location})} />
          </View>
          {/* Explore Section */}
          <View style={styles.exploreSection}>
            <Text style={styles.exploreSectionTitle}>Explore</Text>

            <View style={styles.exploreSectionCardsWrapper}>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {ExploreData.map((item, index) => {
                  return (
                    <ExploreCarCards
                      onPress={() => props.navigation.replace("ListView",{location:location})}
                      key={index}
                      data={item}
                    />
                  );
                })}
              </ScrollView>
            </View>
          </View>
          {/* Explore Section */}

          {/* Popular locations Section */}
          <View style={styles.popularSection}>
            <Text style={styles.popularSectionTitle}>Popluar locations</Text>
            <View style={styles.popularSectionCardsWrapper}>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {PopularLocationData.map((item, index) => {
                  return (
                    <PopularLocationCard
                      onPress={() =>
                        props.navigation.navigate("Map", {
                          data: item,
                          location:location
                        })
                      }
                      key={index}
                      data={item}
                    />
                  );
                })}
              </ScrollView>
            </View>
          </View>
          {/* Popular locations Section */}

          {/* Featured listings nearby */}
          <View style={styles.featuredListingSection}>
            <Text style={styles.featuredListingSectionTitle}>
              Featured listings nearby
            </Text>
            <View style={styles.featuredListingSectionCardsWrapper}>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {FeaturedListData.map((item, index) => {
                  return (
                    <FeaturedListCard
                      onPress={() => props.navigation.navigate("ListView")}
                      key={index}
                      data={item}
                    />
                  );
                })}
              </ScrollView>
            </View>
          </View>
          {/* Featured listings nearby */}

          {/*Selected for you */}
          <View style={styles.selectedForYouSection}>
            <Text style={styles.selectedForYouSectionTitle}>
              Selected for you
            </Text>
            <View style={styles.selectedForYouSectionCardsWrapper}>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {SelectedForYouData.map((item, index) => {
                  return (
                    <ExploreCarCards
                      onPress={() =>
                        props.navigation.navigate("HouseDetails", {
                          data: item,
                          location:location
                        })
                      }
                      key={index}
                      data={item}
                    />
                  );
                })}
              </ScrollView>
            </View>
          </View>
          {/*Selected for you */}
        </View>
      </ScrollView>

      <Animatable.View
        ref={loginpopUp}
        style={[styles.loginPopUpMain, { zIndex: showPopup ? 999 : -1 }]}
      >
        {showPopup && (
          <>
            <LoginPopup
              onContinuePress={() => {
                setShowPopup(false);
                props.navigation.navigate("VerificationCode", {
                  phone: countryCode + phoneNumber,
                  email: email,
                  authMethod: authMethod,
                });
              }}
              authMethod={authMethod}
              setAuthMethod={setAuthMethod}
              onClosePress={() => onLoginOpen()}
              phoneNumber={(val) => setPhoneNumber(val)}
              countryCode={(val) => setCountryCode(val)}
              email={email}
              onChangeEmail={(val) => setEmail(val)}
            />
          </>
        )}
      </Animatable.View>

      {/*Bottom Menu */}
      <BottomMenu
        navigation={props.navigation}
        onSavedPress={() =>
          user ? props.navigation.navigate("Saved") : onLoginOpen()
        }
        activeItem="Home"
        userData={props.get_user_details}
        user={user}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingTop:
      Platform.OS == "ios"
        ? StatusBar.currentHeight + hp("5%")
        : StatusBar.currentHeight + hp("2%"),
    alignItems: "center",
  },
  sectionsWrapper: {
    width: wp("100%"),
  },
  SearchBarWrapper: {
    marginTop: hp("3%"),
    marginBottom: 10,
    marginHorizontal: wp("5%"),
  },
  exploreSection: {
    marginTop: hp("3%"),
    width: wp("100%"),
    height: hp("30%"),
    alignItems: "flex-start",
  },
  exploreSectionTitle: {
    fontFamily: "MB",
    fontSize: rf(28),
    color: colors.textDark,
    marginLeft: wp("5%"),
  },
  exploreSectionCardsWrapper: {
    height: hp("25%"),
    marginTop: hp("2%"),
  },
  popularSection: {
    width: wp("100%"),
    height: hp("35%"),
    alignItems: "flex-start",
  },
  popularSectionTitle: {
    fontFamily: "MB",
    fontSize: rf(20),
    color: colors.textDark,
    marginLeft: wp("5%"),
  },
  popularSectionCardsWrapper: {
    marginTop: hp("2%"),
  },
  featuredListingSection: {
    width: wp("100%"),
    height: hp("40%"),
    alignItems: "flex-start",
    marginBottom: hp("2%"),
  },
  featuredListingSectionTitle: {
    fontFamily: "MB",
    fontSize: rf(20),
    color: colors.textDark,
    marginLeft: wp("5%"),
  },
  featuredListingSectionCardsWrapper: {
    marginTop: hp("2%"),
  },
  selectedForYouSection: {
    width: wp("100%"),
    height: hp("36%"),
    alignItems: "flex-start",
    marginBottom: hp("3%"),
  },
  selectedForYouSectionTitle: {
    fontFamily: "MB",
    fontSize: rf(20),
    color: colors.textDark,
    marginLeft: wp("5%"),
  },
  selectedForYouSectionCardsWrapper: {
    marginTop: hp("1%"),
  },
  loginPopUpMain: {
    width: wp("100%"),
    height: "100%",
    position: "absolute",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { getUserDetails })(Home);
