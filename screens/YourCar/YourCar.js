import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
import {
  AntDesign,
  Entypo,
  EvilIcons,
  FontAwesome,
  FontAwesome5,
  MaterialCommunityIcons,
  SimpleLineIcons,
} from "@expo/vector-icons";
export default function YourCar(props) {

  let data=props.route.params.data
  return (
    <View style={styles.container}>
      <Header navigation={props.navigation} title="Trip Detail" />

      <View style={styles.sectionWrapper}>
        <ScrollView>
          <View style={styles.thumbnail}>
            <View style={styles.listingbox}>
              <View style={styles.greenCircle}></View>
              <Text style={styles.thumbnailText1}>Listings</Text>
            </View>
            <Image
              source={{uri:data.front}}
              resizeMode="cover"
              style={{ width: "100%", height: "100%" }}
            />
            <TouchableOpacity style={styles.thumbnailText2Wrapper}>
              <Text style={styles.thumbnailText2}>View Listings</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.infoWrapper}>
            {/* Section 1 */}
            <View style={styles.infoSection}>
              <Text style={styles.modelName}>{data.title}</Text>
              <Text style={styles.modelCode}>{data.model+" "+data.vin}</Text>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={styles.modelstartText}>5.0</Text>
                <View style={styles.stars}>
                  <Image
                    source={require("../../assets/Images/Star.png")}
                    resizeMode="cover"
                    style={{
                      width: "100%",
                      height: "100%",
                    }}
                  />
                </View>
                <Text style={styles.modelstartText}>3 trips</Text>
              </View>
            </View>
            {/* Section 1 */}
          </View>
          <View style={styles.menuWrapper}>
            <TouchableOpacity style={styles.menuRow}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <AntDesign name="calendar" size={rf(15)} color="#404B69" />
                <Text style={styles.menuItemText}>Calendar</Text>
              </View>
              <Entypo name="chevron-right" size={24} color="#404B69" />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.menuRow}
              onPress={() => props.navigation.navigate("PricingAndDiscount",{data:data})}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <FontAwesome name="dollar" size={rf(15)} color="#404B69" />
                <Text style={styles.menuItemText}>Pricing & discounts</Text>
              </View>
              <Entypo name="chevron-right" size={24} color="#404B69" />
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuRow}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <SimpleLineIcons
                  name="location-pin"
                  size={rf(15)}
                  color="black"
                />
                <Text style={styles.menuItemText}>Location & delivery</Text>
              </View>
              <Entypo name="chevron-right" size={24} color="#404B69" />
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuRow}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <MaterialCommunityIcons
                  name="newspaper-variant-outline"
                  size={rf(16)}
                  color="#404B69"
                />
                <Text style={styles.menuItemText}>Guest instructions</Text>
              </View>
              <Entypo name="chevron-right" size={24} color="#404B69" />
            </TouchableOpacity>

            <TouchableOpacity style={styles.menuRow}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <FontAwesome5
                  name="photo-video"
                  size={rf(15)}
                  color="#404B69"
                />
                <Text style={styles.menuItemText}>Photos</Text>
              </View>
              <Entypo name="chevron-right" size={24} color="#404B69" />
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuRow}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <FontAwesome name="bars" size={rf(15)} color="#404B69" />
                <Text style={styles.menuItemText}>Details</Text>
              </View>
              <Entypo name="chevron-right" size={24} color="#404B69" />
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.menuRow, { borderBottomWidth: 0 }]}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <AntDesign name="bars" size={rf(17)} color="#404B69" />
                <Text style={styles.menuItemText}>Extra</Text>
              </View>
              <Entypo name="chevron-right" size={24} color="#404B69" />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
      {/*Bottom Menu */}
      <BottomMenu
        navigation={props.navigation}
        onSavedPress={() => props.navigation.navigate("Saved")}
        activeItem="Host"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  sectionWrapper: {
    flex: 1,
  },
  thumbnail: {
    width: "100%",
    height: hp("33%"),
  },
  listingbox: {
    width: "30%",
    height: hp("3%"),
    borderRadius: 100,
    backgroundColor: "#fff",
    position: "absolute",
    zIndex: 9999,
    top: 10,
    left: 10,
    alignItems: "center",
    paddingHorizontal: 10,
    flexDirection: "row",
  },
  greenCircle: {
    width: 10,
    height: 10,
    borderRadius: 100,
    backgroundColor: "lightgreen",
    marginRight: 10,
  },
  thumbnailText1: {
    fontSize: rf(12),
    fontFamily: "MB",
    color: "#404B69",
  },
  thumbnailText2Wrapper: {
    position: "absolute",
    bottom: 5,
    alignItems: "center",
    width: "100%",
  },
  thumbnailText2: {
    fontSize: rf(14),
    fontFamily: "MM",
    color: "#fff",
  },
  infoWrapper: {
    marginVertical: hp("2%"),
    paddingHorizontal: wp("5%"),
  },
  modelName: {
    fontSize: rf(18),
    fontFamily: "MB",
  },
  modelCode: {
    fontSize: rf(13),
    fontFamily: "MM",
    color: "grey",
    marginVertical: 5,
  },
  modelstartText: {
    fontSize: rf(10),
    fontFamily: "MB",
  },
  stars: {
    width: 10,
    height: 10,
    marginHorizontal: 5,
    marginVertical: 3,
  },
  infoSection: {
    width: "100%",
    height: hp("8%"),
    paddingTop: 10,
  },
  bookingDetailsSection: {
    width: "100%",
    borderColor: "#e5e5e5",
    borderBottomWidth: 0.5,
    paddingTop: 10,
  },
  sectionText1: {
    fontSize: rf(13),
    fontFamily: "MS",
    color: "#404B69",
  },
  menuWrapper: {
    paddingHorizontal: wp("5%"),
    marginBottom: hp("9%"),
    bottom: 10,
  },
  menuRow: {
    flexDirection: "row",
    width: "100%",
    height: hp("6%"),
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderColor: "#DCDCDC",
  },
  menuItemText: {
    fontSize: rf(13),
    fontFamily: "MM",
    color: "#404B69",
    marginLeft: 10,
  },
});
