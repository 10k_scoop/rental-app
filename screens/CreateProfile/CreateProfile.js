import React, { useEffect, useRef, useState } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  prompt,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "../../components/Header";
import TextField from "./components/TextField";
import GenderComponent from "./components/GenderComponent";
import { ScrollView } from "react-native-gesture-handler";
import { AntDesign } from "@expo/vector-icons";
import ImageCard from "./components/ImageCard";
import * as ImagePicker from "expo-image-picker";
import { LinearGradient } from "expo-linear-gradient";
import { connect } from "react-redux";
import {
  signup,
  linkAccountToEmailAndPassword,
  createUserWithEmailAndPassword,
} from "../../state-management/actions/auth/FirebaseAuthActions";
import Popup from "./components/Popup";
const CreateProfile = (props) => {
  const [fullName, setFullName] = useState(null);
  const [birthdate, setBirthdate] = useState(new Date(1598051730000));
  const [driversLicense, setDriversLicense] = useState(null);
  const [email, setEmail] = useState(null);
  const [pass, setPass] = useState(null);
  const [confirm_pass, setConfirm_pass] = useState(null);
  const [whatDoYouDo, setWhatDoYouDo] = useState(null);
  const [whereDYWork, setWhereDYWork] = useState(null);
  const [aboutMe, setAboutMe] = useState(null);
  const [images, setImages] = useState([]);
  const [selectedGender, setSelectedGender] = useState("male");
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState({});

  const [visible, setVisible] = useState(false);

  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || birthdate;
    setShow(Platform.OS === "ios");
    setBirthdate(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  useEffect(() => {
    if (status?.code == 200) {
      console.log("Account Created Successfully");
      setVisible(true);
    }
    if (status?.code == 500) {
      console.log(status.msg);
    }
  }, [status]);
  const onSubmit = () => {
    let data = {
      fullName: fullName,
      gender: selectedGender,
      birthdate: birthdate,
      driversLicense: driversLicense,
      email:
        props.route.params.type == "email" ? props.route.params.email : email,
      password: pass,
      whatDoYouDo: whatDoYouDo,
      whereDoYouWork: whereDYWork,
      aboutMe: aboutMe,
      images: images,
      id: props.route.params?.data,
      accountType: "host",
    };
    if (
      fullName == "" ||
      email == "" ||
      birthdate == "" ||
      email == "" ||
      pass == "" ||
      images == ""
    ) {
      console.log("Fill all details");
    } else {
      if (pass == confirm_pass) {
        setLoading(true);
        if (props.route.params?.type == "email") {
          props.createUserWithEmailAndPassword(
            data,
            props.route.params.type == "email"
              ? props.route.params.email
              : email,
            pass,
            setLoading,
            setStatus
          );
        } else {
          props.linkAccountToEmailAndPassword(
            data,
            email,
            pass,
            setLoading,
            setStatus
          );
        }
      } else {
        console.log("Password does not match");
      }
    }
  };

  const pickImage = async () => {
    if (images.length >= 5) {
      console.log("Limit end");
    } else {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });

      if (!result.cancelled) {
        setImages((prevState) => [...prevState, result.uri]);
      }
    }
  };

  return (
    <View style={styles.container}>
      <Header
        noGoback
        navigation={props.navigation}
        title="Account Information"
      />

      <Popup
        navigation={props.navigation}
        visible={visible}
        onPress={() => {
          setVisible(false);
          props.navigation.replace("LoadingScreen");
        }}
      />
      <View style={styles.section1}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.section1Title}>Create{"\n"}Your Profile</Text>
          {/* FullName Field */}
          <TextField
            label="Full Name"
            onChangeText={(val) => setFullName(val)}
            value={"Hayden Panettiere"}
            keyboardType="default"
          />
          {/* FullName Field */}

          {/* Gender Field */}
          <GenderComponent
            onMalePress={() => setSelectedGender("male")}
            onFemalePress={() => setSelectedGender("female")}
            onOtherPress={() => setSelectedGender("other")}
            selectedGender={selectedGender}
          />

          {/* Birthday Field */}
          <TextField
            label="Your Birthday"
            onChangeText={(val) => setBirthdate(val)}
            value={"Jul 6, 1990"}
            calendar
            date={birthdate}
            mode={mode}
            show={show}
            onChange={onChange}
            onCalendarPress={() => showDatepicker()}
          />

          {/* Drivers License # Field */}
          <TextField
            label="Drivers License #"
            onChangeText={(val) => setDriversLicense(val)}
            value={"P365-90-678-0"}
            keyboardType="default"
          />
          {/* Email Field */}
          <TextField
            label="Email"
            onChangeText={(val) => setEmail(val)}
            value={
              props.route.params?.type == "email"
                ? props.route.params?.email
                : "lehieuds@gmail.com"
            }
            keyboardType="email-address"
            disabled={props.route.params.type == "email" ? true : false}
          />
          {/* password Field */}
          <TextField
            label="Password"
            onChangeText={(val) => setPass(val)}
            value={"*******"}
            secureTextEntry
            keyboardType="default"
          />
          {/* Confirm password Field */}
          <TextField
            label="Confirm Password"
            onChangeText={(val) => setConfirm_pass(val)}
            value={"*******"}
            secureTextEntry
            keyboardType="default"
          />

          {/* What do you do? Field */}
          <TextField
            label="What do you do?"
            onChangeText={(val) => setWhatDoYouDo(val)}
            value={"Doctor"}
            keyboardType="default"
          />
          {/* Where do you work? Field */}
          <TextField
            label="Where do you work?"
            onChangeText={(val) => setWhereDYWork(val)}
            value={"New York"}
            keyboardType="default"
          />
          {/* About Me Field */}
          <TextField
            label="About Me"
            onChangeText={(val) => setAboutMe(val)}
            value={
              "Fashion photographer currently based in Italy.I love to discovery culture and new paths | foodie | curious of the world"
            }
            multiline
            keyboardType="default"
          />
          {/*Add Images Section */}
          <View style={styles.imagesSection}>
            <Text style={styles.imagesSectionTitle}>
              Your Photos
              <Text style={styles.imagesSectionTitleInner}>
                {" "}
                (Max 5 photos)
              </Text>
            </Text>

            {/*Add New Image Card */}
            <View style={styles.imagesWrapper}>
              <TouchableOpacity
                style={styles.addNewImageCard}
                onPress={() => pickImage()}
              >
                <AntDesign name="plus" size={rf(30)} color="#0F73EE" />
              </TouchableOpacity>

              {images.map((item, index) => {
                return <ImageCard key={index} url={item} />;
              })}
            </View>
          </View>
          {/*Add Images Section */}
        </ScrollView>
        {/* Next Button */}
        <View style={styles.BottomButtonWrapper}>
          <TouchableOpacity
            style={styles.doneBtnWrapper}
            onPress={() => (loading ? null : onSubmit())}
          >
            <LinearGradient
              // Button Linear Gradient
              colors={["#0F73EE", "#C644FC"]}
              // start={{ x: 0.44, y: 0.1 }}
              end={{ x: 1.4, y: 2 }}
              style={styles.doneBtn}
            >
              {loading ? (
                <ActivityIndicator color="white" size="small" />
              ) : (
                <Text style={styles.doneBtnText}>NEXT</Text>
              )}
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  section1: {
    marginTop: hp("4%"),
    paddingHorizontal: wp("5%"),
    paddingHorizontal: wp("5%"),
    width: wp("100%"),
    flex: 1,
  },
  section1Title: {
    fontSize: rf(28),
    color: "#020433",
    fontFamily: "MB",
  },
  imagesSection: {
    width: "100%",
    marginVertical: hp("3%"),
  },
  imagesSectionTitle: {
    fontSize: rf(14),
    color: "#020433",
    fontFamily: "MM",
  },
  imagesSectionTitleInner: {
    fontSize: rf(12),
    color: "#FC2F39",
    fontFamily: "MR",
  },
  addNewImageCard: {
    width: wp("40"),
    height: hp("18%"),
    backgroundColor: "#fff",
    borderWidth: 2,
    borderRadius: 8,
    borderColor: "#0F73EE",
    borderStyle: "dashed",
    marginTop: hp("2%"),
    alignItems: "center",
    justifyContent: "center",
  },
  imagesWrapper: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  doneBtnWrapper: {
    justifyContent: "flex-end",
    paddingHorizontal: wp("5%"),
    alignItems: "center",
  },
  doneBtn: {
    width: "100%",
    height: hp("6%"),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  doneBtnText: {
    fontFamily: "MS",
    fontSize: rf(14),
    color: "#fff",
  },
  BottomButtonWrapper: {
    width: wp("100%"),
    backgroundColor: "#FFFFFF",
    height: hp("10%"),
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    right: wp("5%"),
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
});
export default connect(mapStateToProps, {
  signup,
  createUserWithEmailAndPassword,
  linkAccountToEmailAndPassword,
})(CreateProfile);
