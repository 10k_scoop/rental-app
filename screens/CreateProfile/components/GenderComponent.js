import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function GenderComponent(props) {
  return (
    <View style={styles.genderWrapper}>
      <Text style={styles.genderTitle}>Gender</Text>
      <View style={styles.genderButtonsWrapper}>
        <TouchableOpacity
          onPress={props.onMalePress}
          style={[
            styles.genderButton,
            {
              backgroundColor:
                props.selectedGender == "male" ? "#0F73EE" : "white",
            },
          ]}
        >
          <Text
            style={[
              styles.genderButtonText,
              { color: props.selectedGender != "male" ? "#0F73EE" : "white" },
            ]}
          >
            Male
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={props.onFemalePress}
          style={[
            styles.genderButton,
            {
              backgroundColor:
                props.selectedGender == "female" ? "#0F73EE" : "white",
            },
          ]}
        >
          <Text
            style={[
              styles.genderButtonText,
              { color: props.selectedGender != "female" ? "#0F73EE" : "white" },
            ]}
          >
            Female
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={props.onOtherPress}
          style={[
            styles.genderButton,
            {
              backgroundColor:
                props.selectedGender == "other" ? "#0F73EE" : "white",
            },
          ]}
        >
          <Text
            style={[
              styles.genderButtonText,
              { color: props.selectedGender != "other" ? "#0F73EE" : "white" },
            ]}
          >
            Others
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  genderWrapper: {
    marginTop: hp("2%"),
  },
  genderTitle: {
    fontFamily: "MR",
    fontSize: rf(14),
    color: "#020433",
  },
  genderButtonsWrapper: {
    width: "100%",
    height: hp("6%"),
    borderWidth: 1,
    borderColor: "#0F73EE",
    borderRadius: 8,
    marginTop: hp("1.5%"),
    flexDirection: "row",
    overflow: "hidden",
  },
  genderButton: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
  genderButtonText: {
    fontSize: rf(13),
    fontFamily: "MR",
  },
});
