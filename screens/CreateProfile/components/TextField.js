import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";

import DateTimePicker from "@react-native-community/datetimepicker";
export default function TextField(props) {
  return (
    <View style={styles.codeField}>
      <View style={{ flex: 1 }}>
        <Text style={styles.fullNameFieldTitle}>{props.label}</Text>
        {props.show && props.calendar && (
          <DateTimePicker
            testID="dateTimePicker"
            value={props.date}
            mode={props.mode}
            is24Hour={true}
            display="default"
            onChange={props.onChange}
          />
        )}
        {!props.show && !props.calendar && <TextInput
          style={styles.fullNameField}
          placeholder={props.value}
          onChangeText={(val) => props.onChangeText(val)}
          multiline={props.multiline}
          secureTextEntry={props.secureTextEntry}
          placeholderTextColor="grey"
          keyboardType={props.keyboardType}
          editable={props.disbaled}
        />}
      </View>
      {props.calendar && (
        <TouchableOpacity onPress={props.onCalendarPress}>
          <AntDesign name="calendar" size={rf(22)} color="#0F73EE" />
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  codeField: {
    marginTop: hp("2%"),
    width: "100%",
    minHeight: hp("7%"),
    borderWidth: 2,
    borderColor: "#F0F0F0",
    fontSize: rf(34),
    color: "#020433",
    textAlign: "center",
    fontFamily: "MM",
    borderRadius: 5,
    justifyContent: "center",
    paddingHorizontal: wp("3%"),
    paddingVertical: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  fullNameFieldTitle: {
    fontSize: rf(12),
    color: "#404B69",
    fontFamily: "MR",
  },
  fullNameField: {
    flex: 1,
    fontSize: rf(16),
    color: "#020433",
    fontFamily: "MM",
    top: 5,
  },
});
