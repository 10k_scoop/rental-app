import React, { useState } from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "../../components/Header";
import { LinearGradient } from "expo-linear-gradient";
import { MaterialIcons, Octicons } from "@expo/vector-icons";
import { SliderBox } from "react-native-image-slider-box";
import ConfirmPopUp from "../../components/ConfirmPopUp";

export default function HouseDetails(props) {
  const [confirmPopup, setConfirmPopup] = useState(false);
  return (
    <View style={styles.container}>
      {/* ConFirm Popup */}
      {confirmPopup && (
        <View style={styles.confirmPopupWrapper}>
          <View style={styles.confirmPopupLayer}></View>
          <ConfirmPopUp
            data={props.route.params.data}
            onClosePress={() => setConfirmPopup(false)}
            onBookPress={() => props.navigation.navigate("Booked")}
          />
        </View>
      )}
      {/* ConFirm Popup */}

      <Header
        navigation={props.navigation}
        title={props.route.params.data.title}
      />

      <View style={styles.sectionWrapper}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.section1}>
            {/* Car Cards */}
            <View style={styles.cardHeadWrapper}>
              <View style={styles.cardThumbnailWrapper}>
                <SliderBox
                  images={props.route.params?.data.listImages}
                  dotStyle={{
                    width: 8,
                    height: 8,
                    borderRadius: 15,
                    marginHorizontal: 0,
                    padding: 0,
                    margin: 0,
                  }}
                  dotColor="#fff"
                  inactiveDotColor="#90A4AE"
                  ImageComponentStyle={{height:'100%'}}
                />
              </View>
            </View>

            <View style={styles.cardInfoBoxWrapper}>
              <View style={{ flex: 1 }}>
                <View style={styles.cardInfoBox}>
                  <Text style={styles.cardInfoBoxTitle}>Rent</Text>
                  <Text style={styles.cardInfoBoxTagline}>$450/day</Text>
                </View>
                <View style={styles.cardInfoBox}>
                  <Text style={styles.cardInfoBoxTitle}>Guests</Text>
                  <Text style={styles.cardInfoBoxTagline}>8 Adults</Text>
                </View>
                <View style={styles.cardInfoBox}>
                  <Text style={styles.cardInfoBoxTitle}>Avg Rating </Text>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={styles.cardInfoBoxTagline}>4.8</Text>
                    <Image
                      source={require("../../assets/Images/Star.png")}
                      resizeMode="contain"
                      style={{ width: 15, height: 15, left: 5, top: 3 }}
                    />
                  </View>
                </View>
              </View>

              <View style={{ flex: 1, marginHorizontal: 20 }}>
                <View style={styles.cardInfoBox2}>
                  <Text
                    style={[styles.cardInfoBoxTitle, { right: 4, bottom: 5 }]}
                  >
                    Amenities
                  </Text>
                  <View style={{ marginLeft: 10 }}>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <Octicons
                        name="primitive-dot"
                        size={rf(9)}
                        color="#404B69"
                        style={{ top: 2, right: 5 }}
                      />

                      <Text
                        style={[
                          styles.cardInfoBoxTagline,
                          { fontSize: rf(15) },
                        ]}
                      >
                        Pool
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <Octicons
                        name="primitive-dot"
                        size={rf(9)}
                        color="#404B69"
                        style={{ top: 2, right: 5 }}
                      />

                      <Text
                        style={[
                          styles.cardInfoBoxTagline,
                          { fontSize: rf(15) },
                        ]}
                      >
                        Gym
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <Octicons
                        name="primitive-dot"
                        size={rf(9)}
                        color="#404B69"
                        style={{ top: 2, right: 5 }}
                      />

                      <Text
                        style={[
                          styles.cardInfoBoxTagline,
                          { fontSize: rf(15) },
                        ]}
                      >
                        Spa
                      </Text>
                    </View>
                  </View>
                </View>
                <View
                  style={[
                    styles.cardInfoBox2,
                    { padding: 0, paddingHorizontal: 0 },
                  ]}
                >
                  <Image
                    source={require("../../assets/Images/maphouse.png")}
                    resizeMode="cover"
                    style={{
                      width: "100%",
                      height: "100%",
                      borderRadius: 20,
                    }}
                  />
                </View>
              </View>
            </View>
            {/* Car Cards ends here */}
          </View>
          <View style={styles.descBox}>
            <Text
              style={[
                styles.cardInfoBoxTitle,
                { fontFamily: "MS", marginBottom: 5 },
              ]}
            >
              Home Description
            </Text>
            <Text style={styles.vcDesc}>
              The all electric, torque filled Model S will leave stunned with
              it’s performance. The The all electric, torque filled Model S will
              leave stunned with it’s performance. TheThe all electric, torque
              filled Model S will leave stunned with it’s performance. TheThe
              all electric, torque filled Model S will leave stunned with it’s
              performance. The
            </Text>
          </View>
          <View style={[styles.descBox, { paddingBottom: 20 }]}>
            <Text
              style={[
                styles.cardInfoBoxTitle,
                { fontFamily: "MS", marginBottom: 5 },
              ]}
            >
              House Add-Ons
            </Text>
            <View style={styles.addOnsRow}>
              <View style={{ alignItems: "center" }}>
                <TouchableOpacity style={styles.addOnsBtn}>
                  <MaterialIcons
                    name="add-location"
                    size={rf(25)}
                    color="#323232"
                  />
                </TouchableOpacity>
                <Text style={styles.addOnsRowLabel}>Delivery</Text>
                <Text style={styles.addOnsRowLabel2}>+ $45</Text>
              </View>

              <View style={{ alignItems: "center" }}>
                <TouchableOpacity style={styles.addOnsBtn}>
                  <Image
                    source={require("../../assets/Images/hail.png")}
                    resizeMode="contain"
                    style={{ width: "50%", height: "50%" }}
                  />
                </TouchableOpacity>
                <Text style={styles.addOnsRowLabel}>Chauffeur</Text>
                <Text style={styles.addOnsRowLabel2}>+ $45</Text>
              </View>

              <View style={{ alignItems: "center" }}>
                <TouchableOpacity style={styles.addOnsBtn}>
                  <Image
                    source={require("../../assets/Images/local_police.png")}
                    resizeMode="contain"
                    style={{ width: "45%", height: "45%" }}
                  />
                </TouchableOpacity>
                <Text style={styles.addOnsRowLabel}>Insurance</Text>
                <Text style={styles.addOnsRowLabel2}>+ $45</Text>
              </View>
            </View>
          </View>
          <View style={styles.descBox}>
            <Text
              style={[
                styles.cardInfoBoxTitle,
                { fontFamily: "MS", marginBottom: 5 },
              ]}
            >
              Hosted by Exotic Motors
            </Text>
            <View style={styles.averageRow}>
              <View style={styles.averageRowImg}>
                <Image
                  source={require("../../assets/Images/img38.png")}
                  resizeMode="cover"
                  style={{ width: "100%", height: "100%" }}
                />
              </View>
              <View style={{ marginLeft: hp("1.5%"), flex: 1 }}>
                <Text style={styles.averageRowTitle}>Average Rating </Text>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text style={styles.cardInfoBoxTagline}>4.8</Text>
                  <Image
                    source={require("../../assets/Images/Star.png")}
                    resizeMode="contain"
                    style={{ width: 15, height: 15, left: 5, top: 3 }}
                  />
                </View>
              </View>

              <TouchableOpacity style={styles.buttomButton3}>
                <LinearGradient
                  // Button Linear Gradient
                  colors={["#0F73EE", "#C644FC"]}
                  // start={{ x: 0.44, y: 0.1 }}
                  end={{ x: 1.4, y: 2 }}
                  style={styles.gradientLayer}
                />
                <Text style={styles.buttomButton1Text}>View</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* Addon ends here */}
        </ScrollView>
      </View>
      {/* button */}
      <View style={styles.buttomButtomWrapper}>
        <TouchableOpacity
          style={styles.buttomButton1}
          onPress={() => setConfirmPopup(true)}
        >
          <LinearGradient
            // Button Linear Gradient
            colors={["#0F73EE", "#C644FC"]}
            // start={{ x: 0.44, y: 0.1 }}
            end={{ x: 1.4, y: 2 }}
            style={styles.gradientLayer}
          />
          <Text style={styles.buttomButton1Text}>BOOK NOW</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttomButton2}
          onPress={() => props.navigation.navigate("ListView")}
        >
          <Text style={styles.buttomButton2Text}>Similar</Text>
        </TouchableOpacity>
      </View>
      {/* button */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  sectionWrapper: {
    flex: 1,
    marginTop: hp("3%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
  },
  cardHeadWrapper: {
    width: "100%",
    height: hp("25%"),
    backgroundColor: "#F8F8F8",
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderWidth: 1,
    borderColor: "#E5E5E5",
  },
  cardThumbnailWrapper: {
    width: '100%',
    height: "100%",
    overflow: "hidden",
    borderRadius:20
  },
  cardThumbnail: {
    width: wp("100%"),
    height: "100%",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  cardHeadInfoTitle: {
    fontSize: rf(19),
    fontFamily: "MB",
    color: "#3F4752",
    marginVertical: 5,
  },
  cardHeadInfoTagline: {
    fontSize: rf(12),
    fontFamily: "MB",
    color: "#3F4752",
    opacity: 0.5,
  },
  cardHeadInfo: {
    marginHorizontal: wp("5%"),
    marginTop: 15,
  },
  descBox: {
    backgroundColor: "#F5F6F6",
    borderColor: "#E5E5E5",
    borderWidth: 1,
    alignItems: "flex-start",
    justifyContent: "center",
    borderRadius: 15,
    paddingVertical: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    paddingHorizontal: 20,
    elevation: 5,
    marginVertical: 7,
  },
  cardInfoBox: {
    height: hp("7.5%"),
    backgroundColor: "#F5F6F6",
    borderColor: "#E5E5E5",
    borderWidth: 1,
    alignItems: "flex-start",
    justifyContent: "center",
    borderRadius: 15,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    paddingHorizontal: 20,
    elevation: 5,
    marginVertical: 7,
  },
  cardInfoBoxTitle: {
    fontSize: wp("6%"),
    fontFamily: "MM",
    color: "#404B69",
  },
  cardInfoBoxTagline: {
    fontSize: wp("5%"),
    fontFamily: "MB",
    color: "#404B69",
    top: 3,
  },
  cardInfoBoxWrapper: {
    flex: 1,
    marginTop: hp("3%"),
    flexDirection: "row",
  },
  cardInfoBox2: {
    height: hp("12%"),
    backgroundColor: "#F5F6F6",
    borderColor: "#E5E5E5",
    borderWidth: 1,
    alignItems: "flex-start",
    justifyContent: "center",
    borderRadius: 15,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    paddingHorizontal: 20,
    elevation: 5,
    marginVertical: 7,
  },
  buttomButtomWrapper: {
    paddingHorizontal: 10,
    width: wp("100%"),
    height: hp("10%"),
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "space-around",
    alignItems: "center",
    flexDirection: "row",
  },
  buttomButton1: {
    width: "65%",
    height: hp("6%"),
    backgroundColor: "red",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
  },
  buttomButton3: {
    width: "30%",
    height: hp("5%"),
    backgroundColor: "red",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    left: 10,
  },
  buttomButton1Text: {
    fontFamily: "MS",
    fontSize: rf(12),
    color: "#FFFFFF",
  },
  gradientLayer: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
  buttomButton2: {
    width: "23%",
    height: hp("6%"),
    backgroundColor: "#F5F6F6",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
  },
  buttomButton2Text: {
    fontFamily: "MS",
    fontSize: rf(15),
    color: "#404B69",
  },
  vcDesc: {
    fontFamily: "MM",
    fontSize: rf(13),
    color: "#404B69",
  },
  addOnsRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: hp("1%"),
    width: "100%",
    paddingHorizontal: wp("3%"),
  },
  addOnsBtn: {
    width: hp("7%"),
    height: hp("7%"),
    borderRadius: 100,
    borderWidth: 1,
    borderColor: "#020433",
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 5,
  },
  addOnsRowLabel: {
    fontSize: rf(12),
    fontFamily: "MM",
  },
  addOnsRowLabel2: {
    fontSize: rf(12),
    fontFamily: "MB",
    color: "#0F73EE",
    textAlign: "center",
    right: 5,
    marginTop: 5,
  },
  averageRow: {
    flexDirection: "row",
    paddingHorizontal: wp("2%"),
    alignItems: "center",
  },
  averageRowImg: {
    width: wp("12%"),
    height: wp("12%"),
    borderRadius: 100,
    overflow: "hidden",
    marginVertical: 10,
  },
  averageRowTitle: {
    fontSize: rf(15),
    fontFamily: "MM",
    color: "#404B69",
  },
  averageRowRatingText: {
    fontSize: rf(19),
    fontFamily: "MB",
    color: "#404B69",
  },
  confirmPopupWrapper: {
    width: wp("100%"),
    height: hp("100%"),
    position: "absolute",
    zIndex: 999999,
    alignItems: "center",
    justifyContent: "center",
  },
  confirmPopupLayer: {
    width: wp("100%"),
    height: hp("100%"),
    backgroundColor: "#020433",
    opacity: 0.6,
    position: "absolute",
    zIndex: 999999,
    alignItems: "center",
    justifyContent: "center",
  },
});
