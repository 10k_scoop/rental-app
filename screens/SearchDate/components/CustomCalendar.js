import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Calendar } from "react-native-calendars";
import { RFValue as rf } from "react-native-responsive-fontsize";
export default function CustomCalendar(props) {
  return (
    <Calendar
      hideExtraDays={false}
      firstDay={1}
      style={{ backgroundColor: "#fff" }}
      theme={{
        backgroundColor: "#fff",
        calendarBackground: "#fff",
        textSectionTitleColor: "#0F73EE",
        textSectionTitleDisabledColor: "#222",
        selectedDayBackgroundColor: "#222",
        selectedDayTextColor: "#222",
        todayTextColor: "#222",
        dayTextColor: "#222",
        textDisabledColor: "#BDBDBD",
        dotColor: "#222",
        selectedDotColor: "#222",
        arrowColor: "#219653",
        disabledArrowColor: "#BDBDBD",
        monthTextColor: "black",
        textDayFontFamily: "MR",
        textMonthFontFamily: "MB",
        textDayHeaderFontFamily: "MB",
        textDayFontWeight: "300",
        textMonthFontWeight: "bold",
        textDayHeaderFontWeight: "300",
        textDayFontSize: 14,
        textMonthFontSize: 16,
        textDayHeaderFontSize: 12,
      }}
      markingType={"period"}
      enableSwipeMonths={true}
      // Collection of dates that have to be marked. Default = {}
      markedDates={{
        "2021-10-11": {
          startingDay: true,
          color: "#0F73EE",
          textColor: "white",
        },
        "2021-10-12": { color: "#0F73EE", textColor: "white" },
        "2021-10-13": { color: "#0F73EE", textColor: "#fff" },
        "2021-10-14": { color: "#0F73EE", textColor: "white" },
        "2021-10-15": { color: "#0F73EE", textColor: "white" },
        "2021-10-16": { endingDay: true, color: "#0F73EE", textColor: "white" },
      }}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
