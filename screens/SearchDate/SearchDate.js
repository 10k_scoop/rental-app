import React from "react";
import { Image, StyleSheet, Text, TextInput, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from "expo-linear-gradient";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AntDesign } from "@expo/vector-icons";
import CustomCalendar from "../Filter/components/CustomCalendar";

export default function SearchDate(props) {
  return (
    <View style={styles.container}>
      <LinearGradient
        // Button Linear Gradient
        colors={["#0F73EE", "#C644FC"]}
        start={{ x: 0, y: 0.75 }}
        end={{ x: 1.7, y: 0.25 }}
        style={{
          width: "110%",
          height: "110%",
          position: "absolute",
        }}
      />
      <Text style={styles.screenTitle}>When will you be {"\n"}there?</Text>
      <View style={styles.Section}>
        <View style={styles.sectionHeader}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <AntDesign name="arrowleft" size={rf(22)} color="#404B69" />
          </TouchableOpacity>
          <Text style={styles.sectionHeaderTitle}>
            {props.route.params?.title}
          </Text>
        </View>

        {/* Calendar Wrapper */}
        <View style={styles.calendarWrapper}>
          <View style={styles.calendar}>
            <CustomCalendar navigation={props.navigation} />
          </View>
        </View>
        {/* Calendar Wrapper */}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "flex-end",
  },
  Section: {
    width: wp("100%"),
    height: hp("60%"),
    marginTop: hp("2%"),
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    paddingTop: hp("2%"),
    alignItems: "center",
  },
  screenTitle: {
    fontSize: rf(28),
    color: "#FFFFFF",
    fontFamily: "MB",
    paddingHorizontal: wp("5%"),
  },
  sectionHeader: {
    flexDirection: "row",
    width: "100%",
    paddingHorizontal: wp("5%"),
    alignItems: "center",
  },
  sectionHeaderTitle: {
    fontSize: rf(18),
    color: "#020433",
    fontFamily: "MM",
    width: "80%",
    textAlign: "center",
  },
  calendarWrapper: {
    marginTop: hp("2%"),
    width: "90%",
  },
  calendarWrapperTitle: {
    fontFamily: "MM",
    fontSize: rf(16),
    color: "#020433",
  },
  calendar: {
    width: "100%",
    minHeight: hp("44%"),
    borderWidth: 1,
    borderRadius: 20,
    borderColor: "#E5E5E5",
    marginTop: hp("1%"),
    padding: wp("5%"),
    shadowColor: "#F1F1F1",
    shadowOffset: {
      width: 0,
      height: 20,
    },
    shadowOpacity: 0.45,
    shadowRadius: 3.84,
    backgroundColor: "#fff",
    elevation: 5,
  },
});
