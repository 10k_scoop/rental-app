import React from "react";
import {
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from "expo-linear-gradient";
import { AntDesign, FontAwesome5 } from "@expo/vector-icons";
import ConfettiCannon from "react-native-confetti-cannon";


export default function Booked(props) {

let data=props.route.params.data
  return (
    <View style={styles.container}>
      <LinearGradient
        // Button Linear Gradient
        colors={["#0F73EE", "#C644FC"]}
        start={{ x: 0, y: 0.75 }}
        end={{ x: 1.7, y: 0.25 }}
        style={{
          width: "110%",
          height: "110%",
          position: "absolute",
        }}
      />
      <ConfettiCannon count={300} origin={{ x: -10, y: 10 }} autoStart={true} fadeOut/>
      <View style={styles.section1}>
        <TouchableOpacity
          style={{
            flex: 1,
            alignItems: "flex-start",
            top: StatusBar.currentHeight + hp("5%"),
            width: "100%",
            left: 10,
          }}
          onPress={() => props.navigation.navigate("Home")}
        >
          <AntDesign name="arrowleft" size={rf(25)} color="white" />
        </TouchableOpacity>
        <View style={styles.circle1}>
          <View style={styles.circleLayer}></View>
          <View style={styles.circle2}>
            <View style={styles.circleLayer}></View>
            <View style={styles.circle3}>
              <View style={[styles.circleLayer, { opacity: 0.25 }]}></View>
              <FontAwesome5 name="check" size={wp("20%")} color="white" />
            </View>
          </View>
        </View>
      </View>

      <View style={styles.MsgSection}>
        <Text style={styles.title}>Successfully{"\n"} Booked</Text>
        <Text style={styles.tagline}>
          Congratulations, ‘{data.item.title}’ has been successfully booked. Message
          your designated agent to begin the rental process
        </Text>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => props.navigation.navigate("Message",{data:data.item})}
        >
          <Text style={styles.btnText}>Message agent</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  section1: {
    width: wp("100%"),
    height: hp("50%"),
    alignItems: "center",
    justifyContent: "flex-end",
  },
  circle1: {
    width: wp("90%"),
    height: wp("90%"),
    borderRadius: wp("50%"),
    alignItems: "center",
    justifyContent: "center",
  },
  circle2: {
    width: wp("70%"),
    height: wp("70%"),
    borderRadius: wp("50%"),
    alignItems: "center",
    justifyContent: "center",
  },
  circle3: {
    width: wp("50%"),
    height: wp("50%"),
    borderRadius: wp("50%"),
    alignItems: "center",
    justifyContent: "center",
  },
  circleLayer: {
    width: "100%",
    height: "100%",
    borderRadius: wp("50%"),
    backgroundColor: "#020433",
    position: "absolute",
    opacity: 0.05,
  },
  MsgSection: {
    marginTop: hp("4%"),
  },
  title: {
    fontSize: rf(30),
    fontFamily: "MB",
    color: "#fff",
    textAlign: "center",
  },
  tagline: {
    fontSize: rf(13),
    fontFamily: "MR",
    color: "#fff",
    textAlign: "center",
    paddingHorizontal: wp("5%"),
    marginTop: hp("3%"),
    lineHeight: 25,
  },
  btn: {
    width: wp("50%"),
    height: hp("6.5%"),
    backgroundColor: "#fff",
    borderRadius: 7,
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp("5%"),
    alignSelf: "center",
  },
  btnText: {
    fontSize: rf(14),
    fontFamily: "MM",
    color: "#020433",
  },
});
