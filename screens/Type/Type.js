import React from "react";
import { Image, StyleSheet, Text, TextInput, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from "expo-linear-gradient";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AntDesign } from "@expo/vector-icons";

export default function Type(props) {
  return (
    <View style={styles.container}>
      <LinearGradient
        // Button Linear Gradient
        colors={["#0F73EE", "#C644FC"]}
        start={{ x: 0, y: 0.75 }}
        end={{ x: 1.7, y: 0.25 }}
        style={{
          width: "110%",
          height: "110%",
          position: "absolute",
        }}
      />
      <Text style={styles.screenTitle}>What are you{"\n"}looking for?</Text>
      <View style={styles.Section}>
        <View style={styles.sectionHeader}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <AntDesign name="arrowleft" size={rf(22)} color="#404B69" />
          </TouchableOpacity>
          <Text style={styles.sectionHeaderTitle}>
            {props.route.params?.data?.title}
          </Text>
        </View>

        <View style={styles.listSection}>
          <TouchableOpacity
            style={styles.cityRow}
            onPress={() =>
              props.navigation.navigate("SearchDate", {
                title: props.route.params?.data?.title,
              })
            }
          >
            <View style={styles.info}>
              <Text style={styles.infoTitle}>Book a car</Text>
              <Text style={styles.infoTagline}>Luxury cars and SUVs</Text>
            </View>
            <View style={styles.thumbnail}>
              <Image
                source={require("../../assets/Images/img1.jpg")}
                resizeMode="cover"
                style={{ width: "100%", height: "100%" }}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.cityRow}
            onPress={() =>
              props.navigation.navigate("SearchDate", {
                title: props.route.params?.data?.title,
              })
            }
          >
            <View style={styles.info}>
              <Text style={styles.infoTitle}>Find a home</Text>
              <Text style={styles.infoTagline}>Entire homes and more</Text>
            </View>
            <View style={styles.thumbnail}>
              <Image
                source={require("../../assets/Images/selected4You1.jpg")}
                resizeMode="cover"
                style={{ width: "100%", height: "100%" }}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.cityRow}
            onPress={() =>
              props.navigation.navigate("SearchDate", {
                title: props.route.params?.data?.title,
              })
            }
          >
            <View style={styles.info}>
              <Text style={styles.infoTitle}>Find an experience</Text>
              <Text style={styles.infoTagline}>Enjoy local events</Text>
            </View>
            <View style={styles.thumbnail}>
              <Image
                source={require("../../assets/Images/selected4You2.jpg")}
                resizeMode="cover"
                style={{ width: "100%", height: "100%" }}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "flex-end",
  },
  Section: {
    width: wp("100%"),
    height: hp("45%"),
    marginTop: hp("2%"),
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    paddingTop: hp("2%"),
    alignItems: "center",
  },
  listSection: {
    marginTop: hp("4%"),
  },
  cityRow: {
    width: wp("86%"),
    height: hp("7%"),
    backgroundColor: "#fff",
    borderRadius: 12,
    paddingHorizontal: 10,
    alignItems: "center",
    flexDirection: "row",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    paddingHorizontal: wp("3%"),
    elevation: 5,
    marginVertical: hp("1%"),
  },
  listTitle: {
    fontSize: rf(17),
    color: "#020433",
    fontFamily: "MM",
    marginBottom: hp("2%"),
  },
  thumbnail: {
    width: wp("11%"),
    height: wp("11%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  info: {
    marginLeft: wp("4%"),
    flex: 1,
  },
  infoTitle: {
    fontSize: rf(14),
    color: "#020433",
    fontFamily: "MM",
  },
  infoTagline: {
    fontSize: rf(12),
    color: "#020433",
    fontFamily: "MR",
    top: 1,
  },

  screenTitle: {
    fontSize: rf(28),
    color: "#FFFFFF",
    fontFamily: "MB",
    paddingHorizontal: wp("5%"),
  },
  sectionHeader: {
    flexDirection: "row",
    width: "100%",
    paddingHorizontal: wp("5%"),
    alignItems: "center",
  },
  sectionHeaderTitle: {
    fontSize: rf(18),
    color: "#020433",
    fontFamily: "MM",
    width: "80%",
    textAlign: "center",
  },
});
