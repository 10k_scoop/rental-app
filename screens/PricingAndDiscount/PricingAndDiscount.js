import React, { useState } from "react";
import { StyleSheet, Switch, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import { AntDesign } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import BottomMenu from "../../components/BottomMenu";

export default function PricingAndDiscount(props) {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  return (
    <View style={styles.container}>
      <Header title="Pricing & discounts" navigation={props.navigation} />

      <View style={styles.sectionWrapper}>
        <View style={styles.headerInfo}>
          <Text style={styles.title}>Pricing</Text>
          <Text style={styles.titleTagline}>Manage your pricing settings</Text>
          <Text style={styles.questionText}>How much will i earn?</Text>
        </View>

        <View style={styles.option1}>
          <View style={styles.row1}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Text style={styles.row1Text}>Automatic Pricing</Text>
              <AntDesign name="questioncircleo" size={rf(13)} color="black" />
            </View>
            <Switch
              trackColor={{ false: "#767577", true: "#DCDCDC" }}
              thumbColor={isEnabled ? "#0F73EE" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
          <Text style={styles.row1DescText}>
            Boost earnings by letting your daily price {"\n"}
            automatically adjust to match guest demand
          </Text>
        </View>

        <View style={styles.option2}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Text style={styles.row1Text}>Daily Price</Text>
            <TouchableOpacity>
              <Text style={styles.row2Text2}>Edit</Text>
            </TouchableOpacity>
          </View>
          <Text style={styles.row1DescText}>${props.route.params?.data?.pricePerDay}</Text>
        </View>

        <View style={[styles.option2, { borderBottomWidth: 0 }]}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Text style={styles.row1Text}>Custom Pricing</Text>
            <TouchableOpacity>
              <Text style={styles.row2Text2}>Edit</Text>
            </TouchableOpacity>
          </View>
          <Text style={styles.row1DescText}>Set prices for specific days</Text>
        </View>
      </View>
      {/*Bottom Menu */}
      <BottomMenu
        navigation={props.navigation}
        onSavedPress={() => props.navigation.navigate("Saved")}
        activeItem="Host"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  sectionWrapper: {
    flex: 1,
    marginTop: hp("2%"),
    paddingHorizontal: wp("4%"),
  },
  title: {
    fontFamily: "MB",
    fontSize: rf(17),
    color: "#404B69",
  },
  titleTagline: {
    fontFamily: "MR",
    fontSize: rf(13),
    color: "#404B69",
    marginTop: hp("1%"),
  },
  questionText: {
    fontFamily: "MM",
    fontSize: rf(10),
    color: "#0F73EE",
    marginTop: hp("1.5%"),
  },
  headerInfo: {
    borderBottomWidth: 0.5,
    borderColor: "#DCDCDC",
    height: hp("11%"),
  },
  row1: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  row1Text: {
    fontFamily: "MR",
    fontSize: rf(13),
    color: "#404B69",
    marginRight: 7,
  },
  row2Text2: {
    fontFamily: "MB",
    fontSize: rf(11),
    color: "#0F73EE",
  },
  option1: {
    marginVertical: hp("1.5%"),
    height: hp("10%"),
    borderBottomWidth: 0.6,
    borderColor: "#DCDCDC",
  },
  option2: {
    marginVertical: hp("0.5%"),
    borderBottomWidth: 0.6,
    borderColor: "#DCDCDC",
    paddingBottom: 12,
    height: hp("7%"),
  },
  row1DescText: {
    fontFamily: "MM",
    fontSize: rf(10),
    color: "#95A0B6",
    marginTop: 10,
  },
});
