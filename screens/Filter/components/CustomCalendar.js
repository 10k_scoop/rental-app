import { LinearGradient } from "expo-linear-gradient";
import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Calendar } from "react-native-calendars";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function CustomCalendar(props) {
  const [selectedDays, setSelectedDays] = useState();
  const [date1, setDate1] = useState();
  const [date2, setDate2] = useState();

  const [backwardsSelection, setBackwardsSelection] = useState(false);
  const [count, setCount] = useState(1);

  const onPeriodSelect = (date, c) => {
    var d = new Date(date.dateString);
    var pd = new Date(date1);
    var pd2 = new Date(date2);
    //console.log(d.getDate());
    if ((count == 1 && date1 == null) || d.getDate() <= pd.getDate()) {
      setDate1(date.dateString);
      setSelectedDays((prevState) => ({
        ...prevState,
        [date.dateString]: {
          startingDay: true,
          color: "#0F73EE",
          textColor: "white",
        },
      }));
    } else if (count >= 2 && date2 != null && d.getDate() <= pd2.getDate()) {
      setDate2(date.dateString);
      console.log(date1)
      setSelectedDays(
        {
          [date1]: {
            startingDay: true,
            color: "#0F73EE",
            textColor: "white",
          },
          [date.dateString]: {
            endingDay: true,
            color: "#0F73EE",
            textColor: "white",
          }
        }
      );
      console.log("here mate")
    } else {
      setDate2(date.dateString);
      setSelectedDays((prevState) => ({
        ...prevState,
        [date.dateString]: {
          endingDay: true,
          color: "#0F73EE",
          textColor: "white",
        },
      }));
      console.log(count + "else")
    }
  };
  useEffect(() => {
    if (date2 && date1) {
      let diff = new Date(date2).getTime() - new Date(date1).getTime();
      let diffDay = Math.ceil(diff / (1000 * 60 * 60 * 24));
      let incStart = new Date(date1).getDate() + 1;
      let incEnd = new Date(date2).getDate() - 1;
      //console.log(incEnd);
      let incYear = new Date(date2).getFullYear();
      let incMon = new Date(date2).getMonth() + 1;

      for (let i = incStart; i <= incEnd; i++) {
        let day = i < 10 ? `0${i}` : i;
        let inctTotal = incYear + "-" + incMon + "-" + day;
        setSelectedDays((prevState) => ({
          ...prevState,
          [inctTotal]: {
            color: "#0F73EE",
            textColor: "white",
          },
        }));
      }
    }
    //console.log(selectedDays)
  }, [date1, date2]);


  const onDonePressSetDates = () => {
    props.setStartDate(date1)
    props.setEndDate(date2)
    props.setShowCalendar(false)
    props.setErrorMsg(null)
  }


  return (
    <>
      <View style={{ overflow: "hidden" }}>
        <Calendar
          hideExtraDays={false}
          firstDay={1}
          onDayPress={(day) => {
            onPeriodSelect(day);
            setCount(count + 1);
          }}
          style={{ backgroundColor: "#fff" }}
          theme={{
            backgroundColor: "#fff",
            calendarBackground: "#fff",
            textSectionTitleColor: "#0F73EE",
            textSectionTitleDisabledColor: "#222",
            selectedDayBackgroundColor: "#222",
            selectedDayTextColor: "#fff",
            todayTextColor: "#222",
            dayTextColor: "#222",
            textDisabledColor: "#BDBDBD",
            dotColor: "#222",
            selectedDotColor: "#222",
            arrowColor: "#219653",
            disabledArrowColor: "#BDBDBD",
            monthTextColor: "black",
            textDayFontFamily: "MR",
            textMonthFontFamily: "MB",
            textDayHeaderFontFamily: "MB",
            textDayFontWeight: "300",
            textMonthFontWeight: "bold",
            textDayHeaderFontWeight: "300",
            textDayFontSize: 14,
            textMonthFontSize: 16,
            textDayHeaderFontSize: 12,
          }}
          markingType={"period"}
          enableSwipeMonths={true}
          // Collection of dates that have to be marked. Default = {}
          markedDates={selectedDays}
        />
      </View>
      <View style={styles.calendarActionButtonWrapper}>
        <TouchableOpacity
          style={styles.calendarActionButton}
          onPress={() => {
            setSelectedDays({});
            setDate1(null);
            setDate2(null);
            setCount(1);
          }}
        >
          <Text style={styles.calendarActionButtonText}>Reset</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.calendarActionButton2}
          onPress={() => props.onDonePress ? onDonePressSetDates() : props.navigation.navigate("Home")}
        >
          <LinearGradient
            // Button Linear Gradient
            colors={["#0F73EE", "#C644FC"]}
            // start={{ x: 0.44, y: 0.1 }}
            end={{ x: 1.4, y: 2 }}
            style={styles.btnLayer}
          >
            <Text style={[styles.calendarActionButtonText, { color: "#fff" }]}>
              Done
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  calendarActionButtonWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: wp("5%"),
  },
  calendarActionButton: {},
  calendarActionButtonText: {
    fontFamily: "MB",
    fontSize: rf(13),
    color: "#333333",
  },
  calendarActionButton2: {
    width: wp("25%"),
    height: hp("4.5%"),
    borderRadius: 5,
    overflow: "hidden",
  },
  btnLayer: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
});
