import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import CustomCalendar from "./CustomCalendar";
import { LinearGradient } from "expo-linear-gradient";

export default function Section1(props) {
  return (
    <View style={styles.section1}>
      <Text style={styles.section1Title}>Search Filter</Text>
      {/* Calendar Wrapper */}
      <View style={styles.calendarWrapper}>
        <Text style={styles.calendarWrapperTitle}>Date</Text>
        <View style={styles.calendar}>
          <View style={{ overflow: "hidden" }}>
            <CustomCalendar navigation={props.navigation} />
          </View>
        </View>
      </View>
      {/* Calendar Wrapper */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  sectionWrapper: {
    flex: 1,
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    alignItems: "center",
  },
  section1: {
    marginTop: 10,
    width: wp("90%"),
  },
  section1Title: {
    fontFamily: "MB",
    fontSize: rf(28),
    color: "#020433",
  },
  calendarWrapper: {
    marginTop: hp("2%"),
  },
  calendarWrapperTitle: {
    fontFamily: "MM",
    fontSize: rf(16),
    color: "#020433",
  },
  calendar: {
    width: "100%",
    minHeight: hp("44%"),
    borderWidth: 1,
    borderRadius: 20,
    borderColor: "#E5E5E5",
    marginTop: hp("1%"),
    padding: wp("5%"),
    shadowColor: "#F1F1F1",
    shadowOffset: {
      width: 0,
      height: 20,
    },
    shadowOpacity: 0.45,
    shadowRadius: 3.84,
    backgroundColor: "#fff",
    elevation: 5,
  },
  calendarActionButtonWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: wp("5%"),
  },
  calendarActionButton: {},
  calendarActionButtonText: {
    fontFamily: "MB",
    fontSize: rf(13),
    color: "#333333",
  },
  calendarActionButton2: {
    width: wp("25%"),
    height: hp("4.5%"),
    borderRadius: 5,
    overflow: "hidden",
  },
  btnLayer: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  section2: {
    flex: 1,
    marginVertical: hp("3%"),
    width: "100%",
  },
  section2Title: {
    fontFamily: "MM",
    fontSize: rf(16),
    color: "#020433",
  },
  topItemWrapper: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: hp("2%"),
    right: 10,
  },
  topItem: {
    width: hp("7.5%"),
    height: hp("7.5%"),
    backgroundColor: "#fff",
    borderWidth: 1,
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#F1F1F1",
  },
  topItemText: {
    fontFamily: "MR",
    fontSize: rf(12),
    color: "#020433",
  },
  dailyPriceSection: {
    marginTop: hp("2%"),
    flex: 1,
  },
  dailyPriceFieldWrapper: {
    flexDirection: "row",
    marginTop: hp("2%"),
    justifyContent: "space-between",
  },
  dailyPriceField: {
    width: "45%",
    height: hp("5%"),
    borderColor: "#e5e5e5",
    borderWidth: 1,
    borderRadius: 10,
    justifyContent: "center",
    paddingHorizontal: 10,
  },
  dailyPriceTextInput: {
    fontSize: rf(14),
    fontFamily: "MR",
    flex: 1,
  },
  dailyPriceLabel: {
    fontFamily: "MR",
    fontSize: rf(10),
    color: "#95A0B6",
    top: 3,
  },
});
