import React from "react";
import { StyleSheet, Text, View, StatusBar, Platform } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{ flex: 0.2, alignItems: "center" }}
        onPress={() => props.navigation.goBack()}
      >
        <AntDesign name="arrowleft" size={rf(23)} color="black" />
      </TouchableOpacity>
      <Text style={styles.title}>Filter</Text>
      <TouchableOpacity
        style={{ alignItems: "center", justifyContent: "center", right: 10 }}
      >
        <Text style={styles.title2}>Clear Filter</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    width: wp("100%"),
    height: (Platform.OS = "ios" ? hp("8%") : hp("7%")),
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    marginTop: (Platform.OS = "ios"
      ? StatusBar.currentHeight + hp("3%")
      : StatusBar.currentHeight + hp("1%")),
  },
  title: {
    fontFamily: "MM",
    fontSize: rf(16),
    color: "#020433",
    flex: 1,
    textAlign: "center",
  },
  title2: {
    fontFamily: "MR",
    fontSize: rf(13),
    color: "#0F73EE",
    textAlign: "center",
  },
});
