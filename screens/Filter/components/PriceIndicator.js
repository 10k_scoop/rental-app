import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

import Draggable from "react-native-draggable";
import { RFValue as rf } from "react-native-responsive-fontsize";
export default function PriceIndicator(props) {
  const [startRange, setStartRange] = useState(1);
  const [endRange, setEndRange] = useState(950);
  const [currentPrice, setCurrentPrice] = useState(0);

  const caculate = (val) => {
    var percent = ((val - wp("1%")) / (wp("90%") - wp("1%"))) * 100;
    var a = Math.floor(percent);
    setStartRange(val);
    console.log(val);
  };

  return (
    <View style={styles.wrapper}>
      <View style={styles.PriceIndicator}></View>
      <View style={{ width: 30, height: 30 }}>
        <Draggable
          x={wp("1%")}
          minX={wp("1%")}
          minY={0}
          y={0}
          maxY={0}
          maxX={wp("90%")}
          onDrag={() => caculate(startRange + 1)}
        >
          <View style={styles.PriceIndicatorCircle}>
            <Text style={styles.indicatorText}>{currentPrice}</Text>
          </View>
        </Draggable>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  PriceIndicator: {
    width: "100%",
    height: 5,
    backgroundColor: "#F1F1F1",
    marginTop: 20,
    borderRadius: 10,
    position: "absolute",
  },
  PriceIndicatorCircle: {
    width: 30,
    height: 30,
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#404B69",
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
  },
  wrapper: {
    width: "100%",
    height: hp("5%"),
    justifyContent: "center",
  },
  indicatorText: {
    fontSize: rf(13),
    fontFamily: "MR",
    color: "#404B69",
  },
});
