import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import { LinearGradient } from "expo-linear-gradient";
import Section1 from "./components/Section1";
import SortBy from "./components/SortBy";
import { MaterialIcons } from "@expo/vector-icons";

export default function Filter(props) {
  const [sortByItem, setSortByItem] = useState("BestMatch");

  return (
    <View style={styles.container}>
      <Header navigation={props.navigation} />

      <View style={styles.sectionWrapper}>
        <ScrollView showsVerticalScrollIndicator={false}>
          {/* <Section1 /> */}
          <Section1 navigation={props.navigation} />
          {/* Section 2 */}
          <View style={styles.section2}>
            <Text style={styles.section2Title}>Body type</Text>
            <View style={styles.topItemWrapper}>
              <TouchableOpacity style={styles.topItem}>
                <Text style={styles.topItemText}>Sedan</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.topItem}>
                <Text style={styles.topItemText}>SUV</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.topItem}>
                <Text style={styles.topItemText}>Coupe</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.topItem}>
                <Text style={styles.topItemText}>Pickup</Text>
              </TouchableOpacity>
            </View>
            {/* Body Types Ends here */}
            <View style={styles.dailyPriceSection}>
              <Text style={styles.section2Title}>Daily Price</Text>
              <View style={styles.dailyPriceFieldWrapper}>
                <View style={styles.dailyPriceField}>
                  <Text style={styles.dailyPriceLabel}>Min</Text>
                  <TextInput
                    placeholder="$190"
                    placeholderTextColor="#222"
                    style={styles.dailyPriceTextInput}
                  />
                </View>
                <View style={styles.dailyPriceField}>
                  <Text style={styles.dailyPriceLabel}>Max</Text>
                  <TextInput
                    placeholder="$190"
                    placeholderTextColor="#222"
                    style={styles.dailyPriceTextInput}
                  />
                </View>
              </View>
            </View>
            {/* Body Types Ends here */}

            {/*Location Radius here */}
            <View style={styles.dailyPriceSection}>
              <Text style={styles.section2Title}>Location Radius</Text>
              <View style={styles.dailyPriceFieldWrapper}>
                <View style={styles.dailyPriceField}>
                  <Text style={styles.dailyPriceLabel}>Min</Text>
                  <TextInput
                    placeholder="1 mile"
                    placeholderTextColor="#222"
                    style={styles.dailyPriceTextInput}
                  />
                </View>
                <View style={styles.dailyPriceField}>
                  <Text style={styles.dailyPriceLabel}>Max</Text>
                  <TextInput
                    placeholder="100 miles"
                    placeholderTextColor="#222"
                    style={styles.dailyPriceTextInput}
                  />
                </View>
              </View>
            </View>
            {/*Location Radius here */}

            {/*Sort By here */}
            <View style={styles.dailyPriceSection}>
              <SortBy
                onBestMatchPress={() => setSortByItem("BestMatch")}
                onPricePress={() => setSortByItem("Price")}
                onRecentPress={() => setSortByItem("Recent")}
                selectedItem={sortByItem}
              />
            </View>
            {/*Sort By here */}

            {/*Add Ons */}
            <View style={styles.addOnsSection}>
              <Text style={styles.section2Title}>Add ons</Text>
              <View style={styles.addOnsRow}>
                <View style={{ alignItems: "center" }}>
                  <TouchableOpacity style={styles.addOnsBtn}>
                    <MaterialIcons
                      name="add-location"
                      size={rf(25)}
                      color="#323232"
                    />
                  </TouchableOpacity>
                  <Text style={styles.addOnsRowLabel}>Delivery</Text>
                </View>

                <View style={{ alignItems: "center" }}>
                  <TouchableOpacity style={styles.addOnsBtn}>
                    <Image
                      source={require("../../assets/Images/hail.png")}
                      resizeMode="contain"
                      style={{ width: "50%", height: "50%" }}
                    />
                  </TouchableOpacity>
                  <Text style={styles.addOnsRowLabel}>Chauffeur</Text>
                </View>

                <View style={{ alignItems: "center" }}>
                  <TouchableOpacity style={styles.addOnsBtn}>
                    <Image
                      source={require("../../assets/Images/local_police.png")}
                      resizeMode="contain"
                      style={{ width: "45%", height: "45%" }}
                    />
                  </TouchableOpacity>
                  <Text style={styles.addOnsRowLabel}>Insurance</Text>
                </View>
              </View>
            </View>
            {/*Add Ons */}
          </View>
          {/* Section 2 */}
        </ScrollView>
      </View>
      <View style={styles.BottomButtonWrapper}>
        <TouchableOpacity
          style={styles.doneBtnWrapper}
          onPress={() => props.navigation.navigate("ListView")}
        >
          <LinearGradient
            // Button Linear Gradient
            colors={["#0F73EE", "#C644FC"]}
            // start={{ x: 0.44, y: 0.1 }}
            end={{ x: 1.4, y: 2 }}
            style={styles.doneBtn}
          >
            <Text style={styles.doneBtnText}>SEE 100+ MATCHES</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  sectionWrapper: {
    flex: 1,
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    alignItems: "center",
  },
  section2: {
    flex: 1,
    marginVertical: hp("3%"),
    width: "100%",
  },
  section2Title: {
    fontFamily: "MM",
    fontSize: rf(16),
    color: "#020433",
  },
  topItemWrapper: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: hp("2%"),
    right: 10,
  },
  topItem: {
    width: hp("7.5%"),
    height: hp("7.5%"),
    backgroundColor: "#fff",
    borderWidth: 1,
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#F1F1F1",
  },
  topItemText: {
    fontFamily: "MR",
    fontSize: rf(12),
    color: "#020433",
  },
  dailyPriceSection: {
    marginTop: hp("2%"),
    flex: 1,
  },
  addOnsSection: {
    marginTop: hp("4%"),
    flex: 1,
  },
  dailyPriceFieldWrapper: {
    flexDirection: "row",
    marginTop: hp("2%"),
    justifyContent: "space-between",
  },
  dailyPriceField: {
    width: "45%",
    height: hp("5%"),
    borderColor: "#e5e5e5",
    borderWidth: 1,
    borderRadius: 10,
    justifyContent: "center",
    paddingHorizontal: 10,
  },
  dailyPriceTextInput: {
    fontSize: rf(14),
    fontFamily: "MR",
    flex: 1,
  },
  dailyPriceLabel: {
    fontFamily: "MR",
    fontSize: rf(10),
    color: "#95A0B6",
    top: 3,
  },
  addOnsRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: hp("2%"),
  },
  addOnsBtn: {
    width: hp("7%"),
    height: hp("7%"),
    borderRadius: 100,
    borderWidth: 1,
    borderColor: "#404B69",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
  },
  addOnsRowLabel: {
    fontSize: rf(14),
    fontFamily: "MM",
  },
  doneBtn: {
    width: "100%",
    height: hp("6%"),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  doneBtnText: {
    fontFamily: "MM",
    fontSize: rf(12),
    color: "#fff",
  },
  BottomButtonWrapper: {
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    backgroundColor: "#FFFFFF",
    height: hp("10%"),
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
