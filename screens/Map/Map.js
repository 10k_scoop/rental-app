import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import MapComponent from "./components/MapComponent";
import CardComponent from "./components/CardComponent";
import { FeaturedListData } from "../../constants/FeaturedListData";
import { LinearGradient } from "expo-linear-gradient";
import * as Location from "expo-location";
import { connect } from "react-redux";
import { getCars } from "../../state-management/actions/Features/Actions";
import * as geolib from "geolib";

const Map = (props) => {
  const [location, setLocation] = useState(null);
  const [searchLocation, setSearchLocation] = useState([]);
  const [address, setAddress] = useState(null);
  const [loading, setLoading] = useState(true);
  const [errorMsg, setErrorMsg] = useState(null);
  const [cars, setCars] = useState([]);
  const [search, setSearch] = useState();

  useEffect(() => {
    props.getCars("", setLoading);
  }, []);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      let address = await Location.reverseGeocodeAsync(location.coords);
      setAddress(address);
      setLocation(location);
    })();
  }, []);

  useEffect(() => {
    setCars(props.get_cars?.data);
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="black" />
      </View>
    );
  }

  const onChangeRegion = async () => {
    let getCoordinates = await Location.geocodeAsync(search);
    setSearchLocation(getCoordinates[0]);
    console.log(getCoordinates[0])
  };
  
  return (
    <View style={styles.container}>
      {!location && (
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <ActivityIndicator size="large" color="black" />
        </View>
      )}
      {location && (
        <View style={styles.sectionWrapper}>
          {/* Map Wrapper */}
          <MapComponent
            onSearch={(val) => setSearch(val)}
            carsData={cars}
            onSearchPress={() => onChangeRegion()}
            coords={location}
            searchLocation={searchLocation}
            address={address}
            searchText={search}
            onFilterPress={() => props.navigation.navigate("Filter")}
            onListPress={() => props.navigation.navigate("ListView")}
            onClosePress={() => props.navigation.goBack()}
          />
          {/* Map Wrapper */}

          <View style={styles.cardSection}>
            <View style={styles.cardIndicator}>
              <LinearGradient
                // Button Linear Gradient
                colors={["#0F73EE", "#C644FC"]}
                start={{ x: 0.44, y: 0.1 }}
                end={{ x: 1.4, y: 2 }}
                style={styles.cardIndicatorlayer}
              />
            </View>
            <ScrollView
              decelerationRate={0}
              snapToInterval={wp("55%") - 50}
              snapToAlignment={"center"}
              pagingEnabled
              horizontal
              showsHorizontalScrollIndicator={false}
            >
              {cars.map((item, index) => {
                let distance = geolib.getDistance(
                  {
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                  },
                  { latitude: item.latitude, longitude: item.longitude }
                );
                return (
                  <View key={index}>
                    {distance >= 1000 && (
                      <CardComponent
                        onPress={() => props.navigation.navigate("ListView")}
                        data={item}
                        key={index}
                      />
                    )}
                  </View>
                );
              })}
            </ScrollView>
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  sectionWrapper: {
    flex: 1,
  },
  cardSection: {
    flex: 1,
    marginVertical: hp("1%"),
  },
  cardIndicator: {
    width: wp("52%"),
    height: 4,
    marginLeft: wp("3%"),
  },
  cardIndicatorlayer: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_cars: state.main.get_cars,
});
export default connect(mapStateToProps, { getCars })(Map);
