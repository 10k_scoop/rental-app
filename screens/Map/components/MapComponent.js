import React, { useEffect, useRef, useState } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import MapView, {
  Marker,
} from "react-native-maps";
import { AntDesign, Feather } from "@expo/vector-icons";
import { MapStyle } from "./Mapstyle";
import * as geolib from "geolib";
export default function MapComponent(props) {
  const [location, setLocation] = useState(props.coords.coords);
  const [selectedMarker, setSelectedMarker] = useState();
  const [inputFocus, setInputFocus] = useState(false);
  const [carMarkers, setCarMarkers] = useState([]);

  const getDistance = (lat1, lat2) => {
    let d = geolib.getDistance(
      {
        latitude: location.latitude,
        longitude: location.longitude,
      },
      { latitude: lat1, longitude: lat2 }
    );
    return d;
  };
  let cLocation = {
    latitude: props.coords.coords.latitude,
    longitude: props.coords.coords.longitude,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  }
  console.log(cLocation)
  let sLocation = {
    latitude: props?.searchLocation[0]?.latitude,
    longitude: props?.searchLocation[0]?.longitude,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  }
 
  return (
    <>
      {/* Map Wrapper */}
      <View style={styles.mapWrapper}>
        <MapView
          //provider={PROVIDER_GOOGLE}
          customMapStyle={MapStyle}
          initialRegion={cLocation}
          //region={sLocation}
          // onRegionChange={(a) =>
          //   console.log(a)
          // }
          style={{ flex: 1 }}
          showsUserLocation
        >
          {props?.carsData.map((item, index) => {
            let selectedMarkerConfig =
              selectedMarker == index ? "#0F73EE" : "white";
            let selectedMarkerConfigText =
              selectedMarker == index ? "#fff" : "#222";
            let dd = getDistance(item.latitude, item.longitude);
            console.log(dd)
            return (
              <View key={index}>
                  <Marker
                    key={
                      index +
                      item.latitude +
                      Math.floor(Math.random() * 678675467)
                    }
                    coordinate={{
                      longitude: item.longitude,
                      latitude: item.latitude,
                    }}
                    key={index + Math.floor(Math.random * 7868768)}
                    onPress={() => setSelectedMarker(index)}
                  >
                    <View
                      style={[
                        styles.markerView,
                        { backgroundColor: selectedMarkerConfig },
                      ]}
                    >
                      <Text
                        style={[
                          styles.markerViewText,
                          { color: selectedMarkerConfigText },
                        ]}
                      >
                        ${item.pricePerDay}
                      </Text>
                      <View
                        style={[
                          styles.curve,
                          { borderColor: selectedMarkerConfig },
                        ]}
                      ></View>
                    </View>
                  </Marker>
                
              </View>
            );
          })}
        </MapView>
        <View style={styles.searchBar}>
          <Feather name="search" size={24} color="black" />
          <TextInput
            style={styles.searchInput}
            value={!inputFocus?props?.address[0]?.city:props.searchText}
            onChangeText={(val) => props.onSearch(val)}
            onFocus={()=>setInputFocus(true)}
          />
          <TouchableOpacity style={styles.searchBtn} onPress={props.onSearchPress}>
            <Text style={styles.searchBtnText}>Search</Text>
          </TouchableOpacity>
        </View>

        {/* Map Actions */}
        <View style={styles.mapActions}>
          <TouchableOpacity
            style={styles.mapActionsCancelBtn}
            onPress={props.onClosePress}
          >
            <AntDesign name="close" size={rf(14)} color="black" />
          </TouchableOpacity>
          {/* Map Filter Actions */}
          <View style={{ flex: 1, alignItems: "center", right: "25%" }}>
            <View style={styles.mapActionsfilterWrapper}>
              <TouchableOpacity
                onPress={props.onListPress}
                style={styles.mapActionsfilterBtnWrapper}
              >
                <Text style={styles.mapActionsFilterText}>List</Text>
                <Feather name="menu" size={rf(16)} color="black" />
              </TouchableOpacity>
              <View style={styles.divider}></View>
              <TouchableOpacity
                onPress={props.onFilterPress}
                style={styles.mapActionsfilterBtnWrapper}
              >
                <Text style={styles.mapActionsFilterText}>Filter</Text>
                <Image
                  source={require("../../../assets/Images/ic_filter.png")}
                  resizeMode="contain"
                  style={{ width: 18, height: 18 }}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* Map Filter Actions */}
        </View>
      </View>
      {/* Map Wrapper */}
    </>
  );
}

const styles = StyleSheet.create({
  searchBar: {
    width: wp("90%"),
    height: hp("5%"),
    borderRadius: 10,
    backgroundColor: "#fff",
    marginHorizontal: wp("5%"),
    position: "absolute",
    marginTop: hp("7%"),
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-start",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  searchInput: {
    fontSize: rf(16),
    fontFamily: "MS",
    color: "#404B69",
    left: 10,
    flex:1
  },
  mapWrapper: {
    width: wp("100%"),
    height: hp("65%"),
    backgroundColor: "red",
  },
  mapActions: {
    flexDirection: "row",
    position: "absolute",
    bottom: "5%",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: wp("5%"),
  },
  mapActionsCancelBtn: {
    width: hp("5%"),
    height: hp("5%"),
    borderRadius: 100,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  mapActionsfilterWrapper: {
    width: wp("45%"),
    height: hp("5%"),
    borderRadius: 100,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "space-evenly",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    flexDirection: "row",
    elevation: 5,
  },
  mapActionsfilterBtnWrapper: {
    flexDirection: "row",
    alignItems: "center",
  },
  divider: {
    width: 1.5,
    height: "65%",
    backgroundColor: "#404B69",
    position: "absolute",
    alignSelf: "center",
    left: "45%",
  },
  mapActionsFilterText: {
    fontSize: rf(14),
    fontFamily: "MM",
    color: "#404B69",
    right: 10,
  },
  markerView: {
    backgroundColor: "white",
    width: wp("17%"),
    height: hp("4%"),
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  markerViewText: {
    color: "#222",
    fontSize: rf(14),
    fontFamily: "MB",
  },
  curve: {
    position: "absolute",
    borderLeftWidth: 12,
    borderRightWidth: 12,
    borderTopWidth: 17,
    borderRightColor: "transparent",
    borderLeftColor: "transparent",
    borderColor: "#fff",
    bottom: -10,
  },
  searchBtn:{
    width: wp("20%"),
    height: hp("4%"),
    borderRadius:5,
    backgroundColor:'skyblue',
    alignItems:'center',
    justifyContent:'center'
  },
  searchBtnText:{
    fontFamily:'MM',
    color:'#fff',
    fontSize:rf(12)
  }
});
