import React, { useRef, useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "../../components/Header";
import { LinearGradient } from "expo-linear-gradient";
import {
  FirebaseRecaptchaVerifierModal,
  FirebaseRecaptchaBanner,
} from "expo-firebase-recaptcha";
import { firebaseConfig } from "../../state-management/actions/env";
import { connect } from "react-redux";
import {
  verifyPhoneNumber,
  verifyCode,
  verifyEmail,
  loginWithEmail,
} from "../../state-management/actions/auth/FirebaseAuthActions";
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from "react-native-confirmation-code-field";
import * as firebase from "firebase";
import TextField from "../CreateProfile/components/TextField";

const VerificationCode = (props) => {
  const CELL_COUNT = 6;
  const recaptchaVerifier = useRef(null);
  const [value, setValue] = useState("");
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });
  const [verificationId, setVerificationId] = useState();
  const [verifiedStatus, setVerifiedStatus] = useState(false);
  const [emailVerificationCode, setEmailVerificationCode] = useState();
  const [errorMsg, setErrorMsg] = useState(false);
  const [passwordPage, setPasswordPage] = useState(false);
  const attemptInvisibleVerification = false;
  const [loading, setLoading] = useState(false);
  const [emailLoading, setEmailLoading] = useState(false);
  const [pass, setPass] = useState();
  useEffect(() => {
    if (!verificationId && props.route.params?.authMethod == "phone") {
      props.verifyPhoneNumber(
        props.route.params?.phone,
        recaptchaVerifier,
        setVerificationId,
        setVerifiedStatus
      );
    }
    if (props.route.params?.authMethod == "email") {
      setEmailLoading(true);
      let data = { email: props.route.params?.email };
      props.verifyEmail(data, setVerifiedStatus, setEmailLoading);
    }
  }, [verificationId]);
  useEffect(() => {
    if (verifiedStatus?.server_res?.status == 200) {
      setEmailVerificationCode(verifiedStatus?.server_res?.verification_code);
    }
  }, [verifiedStatus]);

  useEffect(() => {
    if (verifiedStatus?.server_res?.status == 200) {
      setEmailVerificationCode(verifiedStatus?.server_res?.verification_code);
    }
  }, [verifiedStatus]);

  const resendCode = () => {
    if (!verificationId && props.route.params?.authMethod == "phone") {
      props.verifyPhoneNumber(
        props.route.params?.phone,
        recaptchaVerifier,
        setVerificationId,
        setVerifiedStatus
      );
    }
    if (props.route.params?.authMethod == "email") {
      setEmailLoading(true);
      let data = { email: props.route.params.email };
      props.verifyEmail(data, setVerifiedStatus, setEmailLoading);
    }
  };
  console.log(emailVerificationCode);
  const verifyCode = () => {
    setLoading(true);
    if (props.route.params.authMethod == "phone") {
      props.verifyCode(value, verificationId, setLoading, setVerifiedStatus);
    } else {
      if (emailVerificationCode == value) {
        setLoading(false);
        setErrorMsg("Verified");
      } else {
        setLoading(false);
        setErrorMsg("Wrong verification Code");
      }
    }
  };
  useEffect(() => {
    if (
      verifiedStatus?.userData?.msg == "User Exists" &&
      errorMsg == "Verified"
    ) {
      props.navigation.replace("LoadingScreen");
    }

    if (verifiedStatus?.server_res?.msg == "Email_already_verified") {
      console.log("email already verified");
      setPasswordPage(true);
    }
    if (
      verifiedStatus?.userData?.msg == "No such user!" &&
      errorMsg == "Verified"
    ) {
      props.navigation.replace("CreateProfile", {
        email: props.route.params.email,
        type: "email",
      });
    } else if (verifiedStatus?.code == 200 && verifiedStatus.userData?.msg == "success") {
      props.navigation.replace("LoadingScreen");
    } else if (verifiedStatus?.code == 404 && verifiedStatus.userData?.msg == "No such document!") {
      props.navigation.replace("CreateProfile", {
        number: props.route.params.phone,
        data: verifiedStatus.data,
      });
    }

    if (verifiedStatus?.msg == "LoggedInWithEmail") {
      props.navigation.replace("LoadingScreen");
    }
  }, [verifiedStatus, errorMsg]);

  const checkUser = () => {
    if (!loading) {
      if (verifiedStatus?.userData?.code == 404) {
        props.navigation.replace("CreateProfile", {
          number: props.route.params.phone,
          data: verifiedStatus.data,
        });
      }
    }
  };

  const onLogin = () => {
    setLoading(true);
    if (!loading) {
      let data = {
        email: props.route.params.email,
        password: pass,
      };
      props.loginWithEmail(data, setLoading, setVerifiedStatus);
    } else {
    }
  };
  console.log(verifiedStatus);
  return (
    <View style={styles.container}>
      <Header navigation={props.navigation} />
      {emailLoading ? (
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <ActivityIndicator size="large" color="black" />
        </View>
      ) : (
        <>
          {!passwordPage ? (
            <>
              <FirebaseRecaptchaVerifierModal
                ref={recaptchaVerifier}
                firebaseConfig={firebaseConfig}
                attemptInvisibleVerification={attemptInvisibleVerification}
                onError={(e) => console.log(e)}
                onTouchCancel={() => console.log("cancels")}
              />
              <View style={styles.section1}>
                <Text style={styles.section1Title}>Verification Code</Text>
                {props.route.params.authMethod == "email" &&
                  emailVerificationCode == null ? (
                  <Text style={styles.section1Tagline}>
                    Sending the verification Code
                  </Text>
                ) : (
                  <Text style={[styles.section1Tagline, { color: "blue" }]}>
                    {errorMsg}
                  </Text>
                )}
                {verifiedStatus.code == 201 ? (
                  <Text style={styles.section1Tagline}>
                    We texted you an access code.{"\n"}Please enter it below
                  </Text>
                ) : verifiedStatus.code == 200 ? (
                  <Text style={[styles.section1Tagline, { color: "green" }]}>
                    {verifiedStatus?.userData.code == 404 ? "Verification Successfull, Please continue to next screen..." : "Verification Successfull, redirecting..."}
                  </Text>
                ) : verifiedStatus.code == 500 ? (
                  <Text style={[styles.section1Tagline, { color: "red" }]}>
                    {verifiedStatus.res}
                  </Text>
                ) : props.errors?.msg != "" ? (
                  <Text style={[styles.section1Tagline, { color: "red" }]}>
                    {props.errors?.msg}
                  </Text>
                ) : null}
              </View>

              {/* Code Text Field */}
              <View style={styles.codeFieldWrapper}>
                <CodeField
                  ref={ref}
                  {...props}
                  // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
                  value={value}
                  onChangeText={setValue}
                  cellCount={CELL_COUNT}
                  rootStyle={styles.codeFieldRoot}
                  keyboardType="number-pad"
                  textContentType="oneTimeCode"
                  renderCell={({ index, symbol, isFocused }) => (
                    <View
                      style={[styles.cell, isFocused && styles.focusCell]}
                      key={index}
                    >
                      <Text style={styles.cellText}>
                        {symbol || (isFocused ? <Cursor /> : null)}
                      </Text>
                    </View>
                  )}
                />
              </View>
              {/* Code Text Field */}

              <Text style={styles.tagline2}>
                This help us authenticate every user on Aspect, for your peace of
                mind.
              </Text>

              {/* Try Again Text */}
              <View style={styles.tryAgainTextWrapper}>
                <Text style={styles.tryAgainText}>Didn’t get a code? </Text>
                <TouchableOpacity onPress={() => resendCode()}>
                  <Text
                    style={[
                      styles.tryAgainText,
                      { color: "#0F73EE", left: 10 },
                    ]}
                  >
                    Try again
                  </Text>
                </TouchableOpacity>
              </View>
              {/* Try Again Text */}

              {/* Done Button */}

              <View style={styles.BottomButtonWrapper}>
                <TouchableOpacity
                  style={styles.doneBtnWrapper}
                  onPress={() =>
                    verifiedStatus.code == 200 ? checkUser() : verifyCode()
                  }
                >
                  <LinearGradient
                    // Button Linear Gradient
                    colors={["#0F73EE", "#C644FC"]}
                    // start={{ x: 0.44, y: 0.1 }}
                    end={{ x: 1.4, y: 2 }}
                    style={styles.doneBtn}
                  >
                    {loading ? (
                      <ActivityIndicator size="small" color="white" />
                    ) : verifiedStatus.code == 200 ? (
                      <Text style={styles.doneBtnText}>NEXT</Text>
                    ) : (
                      <Text style={styles.doneBtnText}>DONE</Text>
                    )}
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </>
          ) : (
            <View style={[styles.section1, { top: hp("5%") }]}>
              <Text style={styles.section1Title}>Enter your password</Text>
              <TextField
                label="Password"
                onChangeText={(val) => setPass(val)}
                value={"*******"}
                secureTextEntry
                keyboardType="default"
              />
              <TouchableOpacity style={styles.doneBtnWrapper} onPress={onLogin}>
                <LinearGradient
                  // Button Linear Gradient
                  colors={["#0F73EE", "#C644FC"]}
                  // start={{ x: 0.44, y: 0.1 }}
                  end={{ x: 1.4, y: 2 }}
                  style={[styles.doneBtn, { top: hp("6%") }]}
                >
                  {loading ? (
                    <ActivityIndicator size="small" color="white" />
                  ) : (
                    <Text style={styles.doneBtnText}>Login</Text>
                  )}
                </LinearGradient>
              </TouchableOpacity>
            </View>
          )}
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  section1: {
    marginTop: hp("5%"),
    paddingHorizontal: wp("5%"),
  },
  section1Title: {
    fontSize: rf(28),
    color: "#020433",
    fontFamily: "MB",
  },
  section1Tagline: {
    fontSize: rf(13),
    color: "#020433",
    fontFamily: "MR",
    lineHeight: rf(22),
    marginTop: hp("1%"),
  },
  codeFieldWrapper: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp("7%"),
    flexDirection: "row",
  },
  tagline2: {
    fontFamily: "MR",
    fontSize: rf(12),
    color: "#404B69",
    textAlign: "center",
    width: "70%",
    alignSelf: "center",
    marginTop: hp("3%"),
  },
  tryAgainTextWrapper: {
    width: "100%",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center",
    marginTop: hp("8%"),
    flex: 1,
  },
  tryAgainText: {
    fontFamily: "MR",
    fontSize: rf(14),
    color: "#020433",
    textAlign: "center",
  },
  doneBtnWrapper: {
    justifyContent: "flex-end",
    paddingHorizontal: wp("5%"),
    alignItems: "center",
  },
  doneBtn: {
    width: "100%",
    height: hp("6%"),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  doneBtnText: {
    fontFamily: "MS",
    fontSize: rf(14),
    color: "#fff",
  },
  BottomButtonWrapper: {
    backgroundColor: "#FFFFFF",
    height: hp("10%"),
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  root: { flex: 1, padding: 20 },
  codeFieldRoot: { marginTop: 20 },
  cell: {
    width: wp("10%"),
    height: hp("8%"),
    borderColor: "#00000030",
    marginLeft: wp("3%"),
    alignItems: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
  },
  cellText: {
    fontSize: rf(33),
    color: "#020433",
    textAlign: "center",
    fontFamily: "MM",
  },
  focusCell: {
    borderColor: "blue",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  phone_verification: state.main.phone_verification,
  phone_verification_code: state.main.phone_verification_code,
});
export default connect(mapStateToProps, {
  verifyPhoneNumber,
  verifyEmail,
  verifyCode,
  loginWithEmail,
})(VerificationCode);
