import React from "react";
import { Image, StyleSheet, Text, TextInput, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from "expo-linear-gradient";
import { AntDesign, Feather } from "@expo/vector-icons";
import { SearchData } from "../../constants/SearchData";
import { TouchableOpacity } from "react-native-gesture-handler";
export default function Search(props) {
  return (
    <View style={styles.container}>
      <LinearGradient
        // Button Linear Gradient
        colors={["#0F73EE", "#C644FC"]}
        start={{ x: 0, y: 0.75 }}
        end={{ x: 1.7, y: 0.25 }}
        style={{
          width: "110%",
          height: "110%",
          position: "absolute",
        }}
      />
      <View style={styles.Section}>
        {/* Search Bar */}
        <View style={{alignItems:'flex-start',width:'90%',marginBottom:10}}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <AntDesign name="arrowleft" size={rf(22)} color="#404B69" />
          </TouchableOpacity>
        </View>
        <View style={styles.SearchBarView}>
          <Feather name="search" size={24} color="black" />
          <TextInput
            style={styles.searchBarInput}
            placeholderTextColor="#404B69"
            placeholder="Where are you going?"
          />
        </View>
        {/* Search Bar */}

        {/* Popular Cities */}

        <View style={styles.listSection}>
          <Text style={styles.listTitle}>Popular cities</Text>
          {SearchData.map((item, index) => {
            return (
              <TouchableOpacity
                style={styles.cityRow}
                key={index}
                onPress={() =>
                  props.navigation.navigate("Type", { data: item })
                }
              >
                <View style={styles.thumbnail}>
                  <Image
                    source={item.image}
                    resizeMode="cover"
                    style={{ width: "100%", height: "100%" }}
                  />
                </View>
                <View style={styles.info}>
                  <Text style={styles.infoTitle}>{item.title}</Text>
                  <Text style={styles.infoTagline}>{item.tagline}</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>

        {/* Saved Cities */}

        <View style={styles.listSection}>
          <Text style={styles.listTitle}>Saved cities</Text>
          {SearchData.map((item, index) => {
            return (
              <TouchableOpacity
                style={styles.cityRow}
                key={index}
                onPress={() =>
                  props.navigation.navigate("Type", { data: item })
                }
              >
                <View style={styles.thumbnail}>
                  <Image
                    source={item.image}
                    resizeMode="cover"
                    style={{ width: "100%", height: "100%" }}
                  />
                </View>
                <View style={styles.info}>
                  <Text style={styles.infoTitle}>{item.title}</Text>
                  <Text style={styles.infoTagline}>{item.tagline}</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  Section: {
    width: wp("100%"),
    height: hp("90%"),
    marginTop: hp("10%"),
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    paddingTop: hp("2%"),
    alignItems: "center",
  },
  SearchBarView: {
    width: wp("86%"),
    height: hp("6%"),
    backgroundColor: "#fff",
    borderRadius: 20,
    paddingHorizontal: 10,
    alignItems: "center",
    flexDirection: "row",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    paddingHorizontal: wp("5%"),
    elevation: 5,
  },
  searchBarInput: {
    fontSize: rf(16),
    color: "#404B69",
    fontFamily: "MS",
    marginLeft: wp("4%"),
  },
  listSection: {
    marginTop: hp("4%"),
  },
  cityRow: {
    width: wp("86%"),
    height: hp("7%"),
    backgroundColor: "#fff",
    borderRadius: 12,
    paddingHorizontal: 10,
    alignItems: "center",
    flexDirection: "row",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    paddingHorizontal: wp("3%"),
    elevation: 5,
    marginVertical: hp("1%"),
  },
  listTitle: {
    fontSize: rf(17),
    color: "#020433",
    fontFamily: "MM",
    marginBottom: hp("2%"),
  },
  thumbnail: {
    width: wp("11%"),
    height: wp("11%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  info: {
    marginLeft: wp("4%"),
  },
  infoTitle: {
    fontSize: rf(14),
    color: "#020433",
    fontFamily: "MM",
  },
  infoTagline: {
    fontSize: rf(12),
    color: "#020433",
    fontFamily: "MR",
    top: 3,
  },
});
