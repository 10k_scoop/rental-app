import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
export default function Reciever(props) {
  return (
    <View style={styles.main}>
      <View style={styles.main2}>
        <View style={styles.thumbnail}>
          <Image
            source={require("../../../assets/Images/img1.jpg")}
            resizeMode="cover"
            style={{ width: "100%", height: "100%", borderRadius: 100 }}
          />
          <View style={styles.onlineWrapper}></View>
        </View>
        <View style={styles.container}>
          <Text style={styles.text}>{props.msg}</Text>
          <View style={styles.curver}></View>
        </View>
      </View>
      <Text style={styles.time}>{props.date}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  main2: {
    marginVertical: hp("1%"),
    flexDirection: "row",
    alignItems: "center",
  },
  container: {
    backgroundColor: "#F0F0F0",
    maxWidth: wp("60%"),
    paddingVertical: 15,
    borderRadius: 10,
    padding: 10,
    borderBottomLeftRadius: 0,
  },
  text: {
    fontSize: rf(12.5),
    fontFamily: "MR",
    color: "#020433",
  },
  curver: {
    width: 0,
    height: 0,
    position: "absolute",
    left: -10,
    bottom: 0,
    borderLeftWidth: 20,
    borderRightWidth: 20,
    borderBottomWidth: 20,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderColor: "#F0F0F0",
  },
  time: {
    fontSize: rf(10),
    fontFamily: "MR",
    color: "#95A0B6",
    textAlign: "left",
    marginLeft: wp("10%"),
  },
  thumbnail: {
    width: wp("10%"),
    height: wp("10%"),
    borderRadius: 100,
    marginRight: 10,
  },
  onlineWrapper: {
    width: 15,
    height: 15,
    backgroundColor: "lightgreen",
    borderWidth: 3,
    borderColor: "#fff",
    borderRadius: 100,
    position: "absolute",
    right: 0,
    bottom: 0,
  },
});
