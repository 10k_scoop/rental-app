import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Ionicons } from "@expo/vector-icons";
export default function BottomBar(props) {
  return (
    <View style={styles.container}>
      <View style={styles.msgTextField}>
        <TextInput
          onChangeText={props.onChangeText}
          placeholder="Write a message"
          style={styles.msgFieldInput}
          value={props.value}
        />
      </View>
      <View style={styles.actions}>
        <TouchableOpacity>
          <AntDesign name="pluscircleo" size={rf(20)} color="#0F73EE" />
        </TouchableOpacity>

        <TouchableOpacity onPress={props.onSend}>
          <Ionicons name="send-sharp" size={rf(20)} color="#0F73EE" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    width: wp("100%"),
    height: hp("9%"),
    flexDirection: "row",
    paddingHorizontal: wp("5%"),
    alignItems: "center",
  },
  msgTextField: {
    width: "80%",
    height: "80%",
  },
  actions: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  msgFieldInput: {
    flex: 1,
    fontFamily: "MR",
    fontSize: rf(14),
    color: "#95A0B6",
  },
});
