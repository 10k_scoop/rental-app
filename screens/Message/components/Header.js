import React from "react";
import {
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{ flex: 0, alignItems: "flex-start" }}
        onPress={() => props.navigation.navigate("Home")}
      >
        <AntDesign name="arrowleft" size={rf(25)} color="black" />
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.titleWrapper}
        onPress={() => props.navigation.navigate("Profile")}
      >
        <Text style={styles.title}>{props?.title}</Text>
        <Image source={require("../../../assets/Images/ic_verified.jpg")} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    width: wp("100%"),
    height: (Platform.OS = "ios" ? hp("8%") : hp("7%")),
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    marginTop: (Platform.OS = "ios"
      ? StatusBar.currentHeight + hp("4%")
      : StatusBar.currentHeight + hp("3%")),
    paddingHorizontal: wp("4%"),
    borderBottomWidth: 1,
    borderColor: "#DCDCDC",
  },
  title: {
    fontFamily: "MM",
    fontSize: rf(15),
    color: "#020433",
    marginRight: 10,
  },
  titleWrapper: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
    justifyContent:'center'
  },
  shareBtn: {
    flex: 0.2,
    alignItems: "center",
    paddingRight: 5,
  },
});
