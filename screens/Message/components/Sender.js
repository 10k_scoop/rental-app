import React from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
export default function Sender(props) {
  return (
    <View style={{ marginVertical: hp("1%") }}>
      <View style={styles.container}>
        <Text style={styles.text}>{props.msg}</Text>
        <View style={styles.curver}></View>
      </View>
      <Text style={styles.time}>{props.date}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#0F73EE",
    maxWidth: wp("60%"),
    paddingVertical: 15,
    borderRadius: 10,
    padding: 10,
  },
  text: {
    fontSize: rf(12.5),
    fontFamily: "MR",
    color: "#fff",
  },
  curver: {
    width: 0,
    height: 0,
    position: "absolute",
    right: -10,
    bottom: 0,
    borderLeftWidth: 20,
    borderRightWidth: 20,
    borderBottomWidth: 20,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderColor: "#0F73EE",
  },
  time: {
    fontSize: rf(10),
    fontFamily: "MR",
    color: "#95A0B6",
    textAlign: "right",
    marginTop: 7,
  },
});
