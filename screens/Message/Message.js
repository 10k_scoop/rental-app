import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  FlatList,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import { LinearGradient } from "expo-linear-gradient";
import Sender from "./components/Sender";
import Reciever from "./components/Reciever";
import BottomBar from "./components/BottomBar";
import * as firebase from "firebase";
import {
  getMessage,
  sendMessage,
} from "../../state-management/actions/Features/Actions";
import { connect } from "react-redux";

const Message = (props) => {
  const [loading, setLoading] = useState(true);
  const [msgs, setMsg] = useState([]);
  const [msgInput, setMsgInput] = useState();
  let data = props.route.params.data;
  const user = firebase.auth().currentUser;
  const convoId = user.email + data.host;
  
  useEffect(() => {
    props.getMessage(convoId, setLoading, setMsg);
  }, []);
  
  const sendMessage = () => {
    if (msgInput == "" || msgInput == null) {
      console.log("enter message");
    } else {
      var time=new Date()
      let d = {
        convoId: convoId,
        msgs: {
          msg: msgInput,
          sender: user.email,
        },
        time:new Date(),
        sendingTime:time.getHours()+":"+time.getMinutes()+":"+time.getSeconds()
      };
      props.sendMessage(d, setLoading);
      setMsgInput("");
    }
  };
  const renderItem = ({ item }) => <Item title={item.msgs.msg} />;
  return (
    <View style={styles.container}>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
        {/* Header */}
        <Header title={data.hostName} navigation={props.navigation} />
        <View style={styles.topBar}>
          <View>
            <Text style={styles.topBarText}>{data.title}</Text>
            <Text style={styles.topBarText2}>
              ${parseInt(data.pricePerDay) * 30} per month
            </Text>
          </View>
          <View style={styles.barBtn}>
            <LinearGradient
              // Button Linear Gradient
              colors={["#0F73EE", "#C644FC"]}
              start={{ x: 0.44, y: 0.1 }}
              end={{ x: 1.4, y: 2 }}
              style={styles.layer}
            />
            <Text style={styles.barBtnText}>Details</Text>
          </View>
        </View>
        {/* Header */}
        <View style={styles.chatWrapper}>
          {msgs == null && <Text>No conversation found</Text>}
          {loading ? (
            <ActivityIndicator size="large" color="black" />
          ) : (
            <FlatList
              data={msgs}
              inverted
              renderItem={(item, index) => {
                var t = item.item.msgs.sendingTime
                return (
                  <View
                    key={
                      item.item.convoId + Math.floor(Math.random() * 564165645)
                    }
                  >
                    {item.item.msgs.sender == user.email ? (
                      <View style={styles.senderWrapper}>
                        <Sender msg={item.item.msgs.msg} date={t} />
                      </View>
                    ) : item.item.msgs.sender == data.host ? (
                      <View style={styles.recieverWrapper}>
                        <Reciever msg={item.item.msgs.msg} date={t} />
                      </View>
                    ) : null}
                  </View>
                );
              }}
              keyExtractor={item=>item.convoId + Math.floor(Math.random() * 564165645)}
            />
          )}
        </View>

        <BottomBar
          onSend={() => sendMessage()}
          onChangeText={(val) => setMsgInput(val)}
          value={msgInput}
        />
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FF",
  },
  chatWrapper: {
    flex: 1,
  },
  topBar: {
    width: wp("100%"),
    height: hp("8%"),
    backgroundColor: "#fff",
    paddingHorizontal: wp("5%"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    bottom: 5,
    zIndex: -1,
  },
  topBarText: {
    fontSize: rf(13),
    fontFamily: "MB",
    color: "#020433",
  },
  topBarText2: {
    fontSize: rf(11),
    fontFamily: "MR",
    color: "#020433",
    top: 3,
  },
  barBtn: {
    width: wp("25%"),
    height: hp("5%"),
    backgroundColor: "blue",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
  },
  barBtnText: {
    fontSize: rf(11),
    fontFamily: "MR",
    color: "#fff",
  },
  layer: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
  senderWrapper: {
    alignItems: "flex-end",
    paddingHorizontal: wp("5%"),
    marginVertical: 5,
  },
  recieverWrapper: {
    alignItems: "flex-start",
    paddingHorizontal: wp("5%"),
    marginVertical: 5,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
});
export default connect(mapStateToProps, { getMessage, sendMessage })(Message);
