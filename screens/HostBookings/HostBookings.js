import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import BottomMenu from "../../components/BottomMenu";
import CarCard from "./components/CarCard";
import { AntDesign } from "@expo/vector-icons";
import { ScrollView } from "react-native-gesture-handler";
import MessageCard from "./components/MessageCard";

export default function HostBooking(props) {
  const [activatedTab, setActivatedTab] = useState("trips");

  return (
    <View style={styles.container}>
      <Header
        activatedTab={activatedTab}
        onPress={(val) => setActivatedTab(val)}
      />

      <View style={styles.sectionWrapper}>
        <ScrollView>
          {/* Body */}
          <View style={styles.sectionDataBody}>
            <View style={styles.headerTextWrapper}>
              <Text style={styles.headerText}>
                {activatedTab == "trips" ? "UPCOMING" : "CONVERSATIONS"}
              </Text>
              <View style={[styles.headerText, { flexDirection: "row" }]}>
                <Text style={styles.headerText}>TRIP DATE </Text>
                <AntDesign name="arrowdown" size={rf(12)} color="#404B69" />
              </View>
            </View>

            {activatedTab == "trips" ? (
              <CarCard
                onDetailsPress={() => props.navigation.navigate("TripDetails")}
                onMsgPress={() => props.navigation.navigate("Message")}
              />
            ) : (
              <>
                <MessageCard new />
                <MessageCard read />
                <MessageCard read />
              </>
            )}
          </View>
          {/* Body */}
        </ScrollView>
      </View>

      {/*Bottom Menu */}
      <BottomMenu
        navigation={props.navigation}
        onSavedPress={() => props.navigation.navigate("Saved")}
        activeItem="Bookings"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FF",
  },
  sectionWrapper: {
    flex: 1,
    paddingHorizontal: wp("5%"),
    marginTop: hp("2%"),
  },
  sectionDataBody: {
    width: "100%",
    marginVertical: hp("2%"),
  },
  headerTextWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    height: hp("4%"),
    alignItems: "center",
  },
  headerText: {
    fontSize: rf(12),
    fontFamily: "MM",
    color: "#404B69",
  },
});
