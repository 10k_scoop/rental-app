import React from "react";
import { Image, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Entypo, Ionicons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";

export default function CarCard(props) {
  return (
    <View style={styles.container}>
      <View style={styles.thumbnail}>
        <Image
          source={require("../../../assets/Images/img1.png")}
          resizeMode="cover"
          style={{
            width: "100%",
            height: "100%",
          }}
        />
      </View>

      <View style={styles.info}>
        <Text style={styles.title}>McLaren 720S</Text>
        <Text style={styles.text1}>Booked by Josh</Text>
        <Text style={styles.text3}>1527t17</Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginVertical: 2,
          }}
        >
          <Text style={styles.text2}>Location: Airport</Text>
        </View>

        <Text style={styles.text2}>Date: Oct 16 @ 2:00 pm</Text>
      </View>

      {/* Top Right Button */}
      <TouchableOpacity style={styles.barCardTopTag} onPress={props.onMsgPress}>
        <View style={styles.barCardTopTagTextWrapper}>
          <Text style={styles.barCardTopTagText}>Message</Text>
          <Ionicons name="chevron-forward" size={rf(13)} color="#fff" />
        </View>
      </TouchableOpacity>
      {/* Top Right Button */}

      {/* Bottom Right Button */}
      <TouchableOpacity
        style={styles.barCardBottomTag}
        onPress={props.onDetailsPress}
      >
        <LinearGradient
          // Button Linear Gradient
          colors={["#0F73EE", "#C644FC"]}
          start={{ x: 0.44, y: 0.1 }}
          end={{ x: 1.4, y: 2 }}
          style={styles.layer}
        />
        <View style={styles.barCardBottomTagTextWrapper}>
          <Text style={styles.barCardBottomTagText}>Details</Text>
          <Ionicons name="chevron-forward" size={rf(13)} color="#fff" />
        </View>
      </TouchableOpacity>
      {/* Bottom Right Button */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: hp("14%"),
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#DADADA",
    backgroundColor: "#fff",
    flexDirection: "row",
    marginBottom: hp("1%"),
    overflow: "hidden",
  },
  thumbnail: {
    width: "30%",
    height: "100%",
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    overflow: "hidden",
  },
  info: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  title: {
    fontSize: rf(14),
    fontFamily: "MB",
    color: "#404B69",
    marginBottom: 5,
  },
  text1: {
    fontSize: rf(10),
    fontFamily: "MM",
    color: "#404B69",
    marginBottom: 5,
  },
  text2: {
    fontSize: rf(10),
    fontFamily: "MM",
    color: "#404B69",
    marginVertical: 2,
  },
  text3: {
    fontSize: rf(12),
    fontFamily: "MR",
    color: "#404B69",
    marginVertical: 2,
  },
  star: {
    width: 10,
    height: 10,
    marginHorizontal: 5,
  },
  arrow: {
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 5,
  },

  barCardTopTag: {
    width: wp("28%"),
    height: hp("4%"),
    borderRadius: wp("50%"),
    borderBottomLeftRadius: wp("5%"),
    backgroundColor: "#95A0B6",
    transform: [{ rotateX: "-180deg" }],
    right: -wp("5%"),
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 99999,
  },
  barCardTopTagTextWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  barCardTopTagText: {
    fontSize: rf(10),
    transform: [{ rotateX: "-180deg" }],
    fontFamily: "MB",
    color: "#FFFFFF",
  },
  barCardBottomTag: {
    width: wp("28%"),
    height: hp("4%"),
    borderRadius: wp("50%"),
    borderTopLeftRadius: wp("5%"),
    backgroundColor: "#F5CA48",
    transform: [{ rotateX: "-180deg" }],
    right: -wp("5%"),
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 99999,
    bottom: 0,
    overflow: "hidden",
  },
  barCardBottomTagTextWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  barCardBottomTagText: {
    fontSize: rf(10),
    transform: [{ rotateX: "-180deg" }],
    fontFamily: "MB",
    color: "#FFFFFF",
  },
  layer: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
});
