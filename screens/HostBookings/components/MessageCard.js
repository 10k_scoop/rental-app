import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
export default function MessageCard(props) {
  return (
    <View
      style={[
        styles.container,
        { backgroundColor: props.read ? "#fff" : "#D9E7FD" },
      ]}
    >
      <View style={styles.profile}>
        <Image
          source={require("../../../assets/Images/selected4You1.jpg")}
          resizeMode="cover"
          style={{
            width: "100%",
            height: "100%",
          }}
        />
      </View>

      <View style={styles.col2}>
        <Text style={styles.username}>Darlene Switzeland</Text>
        <Text style={styles.msgText} numberOfLines={1}>
          Pls take a look at the images. I was I was I was I was
        </Text>
      </View>

      <View style={styles.col3}>
        <Text style={styles.time}>10:53</Text>

        <View
          style={[
            styles.msgCount,
            { backgroundColor: props.new ? "#2F80ED" : "#fff" },
          ]}
        >
          <Text style={styles.msgCountText}>10</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: hp("8%"),
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#DADADA",
    borderRadius: 10,
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: wp("3%"),
    marginBottom: hp("1%"),
  },
  col2: {
    flex: 0.8,
    marginLeft: 10,
  },
  profile: {
    width: wp("11%"),
    height: wp("11%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  username: {
    fontSize: rf(13),
    fontFamily: "MM",
    color: "#020433",
  },
  msgText: {
    fontSize: rf(12),
    fontFamily: "MR",
    color: "#020433",
  },
  col3: {
    flex: 0.2,
    height: "80%",
    alignItems: "flex-end",
    justifyContent: "space-around",
  },
  time: {
    fontSize: rf(12),
    fontFamily: "MR",
    color: "#333333",
  },
  msgCount: {
    width: wp("6%"),
    height: wp("6%"),
    backgroundColor: "#2F80ED",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
  },
  msgCountText: {
    fontSize: rf(10),
    fontFamily: "MS",
    color: "#fff",
  },
});
