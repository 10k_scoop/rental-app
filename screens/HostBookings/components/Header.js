import React from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { TouchableOpacity } from "react-native";

export default function Header(props) {
  return (
    <>
      {/*Header */}

      <View style={styles.header}>
        <Text style={styles.title}>Bookings</Text>
        {/* Tabs */}
        <View style={styles.tabWrapper}>
          <TouchableOpacity
            onPress={() => props.onPress("trips")}
            style={[
              styles.tabItem,
              {
                borderBottomWidth: props.activatedTab == "trips" ? 2 : 0,
              },
            ]}
          >
            <Text
              style={[
                styles.tabItemText,
                {
                  color: props.activatedTab == "trips" ? "#0F73EE" : "#DADADA",
                },
              ]}
            >
              TRIPS
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => props.onPress("messages")}
            style={[
              styles.tabItem,
              {
                borderBottomWidth: props.activatedTab == "messages" ? 2 : 0,
              },
            ]}
          >
            <Text
              style={[
                styles.tabItemText,
                {
                  color:
                    props.activatedTab == "messages" ? "#0F73EE" : "#DADADA",
                },
              ]}
            >
              MESSAGES
            </Text>
          </TouchableOpacity>
        </View>
        {/* Tabs */}
      </View>
      {/* Header */}
    </>
  );
}

const styles = StyleSheet.create({
  header: {
    width: wp("100%"),
    height: hp("20%"),
    alignItems: "center",
    justifyContent: "flex-end",
    backgroundColor: "#fff",
    paddingTop: hp("5%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  title: {
    fontSize: rf(16),
    fontFamily: "MS",
    color: "#020433",
    flex: 1,
    top: "25%",
  },
  tabWrapper: {
    width: "100%",
    flexDirection: "row",
  },
  tabItem: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#0F73EE",
    height: hp("5%"),
  },
  tabItemText: {
    fontSize: rf(13),
    fontFamily: "MM",
    color: "#0F73EE",
  },
});
