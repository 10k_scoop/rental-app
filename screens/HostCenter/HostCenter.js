import React, { useEffect, useState } from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import BottomMenu from "../../components/BottomMenu";
import SearchBar from "./components/SearchBar";
import CarCard from "./components/CarCard";
import { AntDesign } from "@expo/vector-icons";
import HomesCard from "./components/HomesCard";
import ReviewsSection from "./components/ReviewsSection";
import { ScrollView } from "react-native-gesture-handler";
import EarningSection from "./components/EarningSection";
import { connect } from "react-redux";
import * as firebase from "firebase";
import { getHostCars } from "../../state-management/actions/Features/Actions"; ``

const HostCenter = (props) => {
  const [activatedTab, setActivatedTab] = useState("cars");
  const [cars, setCars] = useState([]);
  const [filteredData, setfilteredData] = useState([]);
  const [loading, setLoading] = useState(true);
  const user = firebase.auth().currentUser;
  const [searchText, setSearchText] = useState('');

  useEffect(() => {
    props.getHostCars(user.email, setLoading);
  }, []);

  useEffect(() => {
    setCars(props.get_host_cars?.data)
  }, [props]);

  const onSearch = (val) => {
    let target = val.toLowerCase();
    setSearchText(target);
    let filteredData = cars.filter(function (item) {
      return item.title.toLowerCase().includes(val);
    });
    setfilteredData(filteredData);
  };


  return (
    <View style={styles.container}>
      <Header
        activatedTab={activatedTab}
        onPress={(val) => setActivatedTab(val)}
      />

      <View style={styles.sectionWrapper}>
        <ScrollView>
          {(activatedTab == "cars" || activatedTab == "homes") && (
            <SearchBar
              onAddPress={() => props.navigation.navigate("PostACar")}
              onChangeText={(val) => onSearch(val)}
            />
          )}

          {/* Body */}
          <View style={styles.sectionDataBody}>
            {/* Body Header */}

            {/*Cars ands homes wraper */}
            {activatedTab == "cars" || activatedTab == "homes" ? (
              <>
                <View style={styles.headerTextWrapper}>
                  <Text style={styles.headerText}>
                    {activatedTab == "cars" ? "VEHICLES" : "HOMES"}
                  </Text>
                  <View style={[styles.headerText, { flexDirection: "row" }]}>
                    <Text style={styles.headerText}>TRIP DATE </Text>
                    <AntDesign name="arrowdown" size={rf(12)} color="#404B69" />
                  </View>
                </View>
                {/* Body Header */}
                {loading ? (
                  <ActivityIndicator size="large" color="black" />
                ) : (
                  <>
                    {activatedTab == "cars" ? (
                      searchText != "" ?
                        <>
                          {
                            filteredData?.map((item, index) => {
                              return (
                                <CarCard
                                  key={index}
                                  data={item}
                                  thumbnail={item.front}
                                  onPress={() => props.navigation.navigate("YourCar", { data: item })}
                                />
                              );
                            })
                          }
                        </>
                        :
                        <>
                          {
                            cars?.map((item, index) => {
                              return (
                                <CarCard
                                  key={index}
                                  data={item}
                                  thumbnail={item.front}
                                  onPress={() => props.navigation.navigate("YourCar", { data: item })}
                                />
                              );
                            })
                          }
                        </>
                    ) : activatedTab == "homes" ? (
                      <HomesCard
                        onPress={() => props.navigation.navigate("Saved")}
                      />
                    ) : null}
                  </>
                )}
              </>
            ) : activatedTab == "reviews" ? (
              <ReviewsSection />
            ) : activatedTab == "earnings" ? (
              <EarningSection navigation={props.navigation} data={cars} />
            ) : null}
            {/*Cars ands homes wraper */}
          </View>
          {/* Body */}
        </ScrollView>
      </View>

      {/*Bottom Menu */}
      <BottomMenu
        navigation={props.navigation}
        onSavedPress={() => props.navigation.navigate("Saved")}
        activeItem="Host"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FF",
  },
  sectionWrapper: {
    flex: 1,
    paddingHorizontal: wp("5%"),
    marginTop: hp("2%"),
  },
  sectionDataBody: {
    width: "100%",
    marginVertical: hp("2%"),
  },
  headerTextWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    height: hp("4%"),
    alignItems: "center",
  },
  headerText: {
    fontSize: rf(12),
    fontFamily: "MM",
    color: "#404B69",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_host_cars: state.main.get_host_cars,
});
export default connect(mapStateToProps, { getHostCars })(HostCenter);
