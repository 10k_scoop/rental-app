import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function ReviewCard(props) {
  return (
    <View style={styles.container}>
      <View style={styles.innerCont}>
        <View style={styles.profile}>
          <Image
            source={require("../../../assets/Images/selected4You1.jpg")}
            resizeMode="cover"
            style={{
              width: "100%",
              height: "100%",
            }}
          />
        </View>

        <View style={styles.Info}>
          <View style={styles.starsWrapper}>
            <View style={styles.stars}>
              <Image
                source={require("../../../assets/Images/Star.png")}
                resizeMode="cover"
                style={{
                  width: "100%",
                  height: "100%",
                }}
              />
            </View>
            <View style={styles.stars}>
              <Image
                source={require("../../../assets/Images/Star.png")}
                resizeMode="cover"
                style={{
                  width: "100%",
                  height: "100%",
                }}
              />
            </View>
            <View style={styles.stars}>
              <Image
                source={require("../../../assets/Images/Star.png")}
                resizeMode="cover"
                style={{
                  width: "100%",
                  height: "100%",
                }}
              />
            </View>
            <View style={styles.stars}>
              <Image
                source={require("../../../assets/Images/Star.png")}
                resizeMode="cover"
                style={{
                  width: "100%",
                  height: "100%",
                }}
              />
            </View>
            <View style={styles.stars}>
              <Image
                source={require("../../../assets/Images/Star.png")}
                resizeMode="cover"
                style={{
                  width: "100%",
                  height: "100%",
                }}
              />
            </View>
          </View>

          <View style={{ flexDirection: "row", marginVertical: 5 }}>
            <Text style={styles.reviewsUserName}>Jennifer Smith</Text>
            <Text style={styles.date}>Aug 21, 2021</Text>
          </View>
          <Text style={styles.modelName}>BMW 3-Series</Text>
        </View>
      </View>
      {props.comment && <Text style={styles.comment}>{props.comment}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    width: "100%",
    minHeight: hp("10%"),
    borderRadius: 15,
    paddingTop: 7,
    paddingHorizontal: wp("3%"),
    borderWidth: 1,
    borderColor: "#DADADA",
    marginTop: hp("1%"),
  },
  innerCont: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 7,
  },
  profile: {
    width: wp("10%"),
    height: wp("10%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  Info: {
    marginHorizontal: wp("3%"),
  },
  stars: {
    width: 18,
    height: 18,
  },
  starsWrapper: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: "53%",
  },
  reviewsUserName: {
    fontSize: rf(10),
    fontFamily: "MS",
    color: "#404B69",
  },
  date: {
    left: 5,
    color: "#DADADA",
    fontSize: rf(10),
    fontFamily: "MM",
  },
  modelName: {
    color: "#404B69",
    fontSize: rf(10),
    fontFamily: "MM",
  },
  comment: {
    marginVertical: 10,
    color: "#404B69",
    fontSize: rf(9.5),
    fontFamily: "MM",
  },
});
