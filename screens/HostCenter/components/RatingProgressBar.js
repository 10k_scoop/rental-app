import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from "expo-linear-gradient";

export default function RatingProgressBar(props) {
  return (
    <View style={styles.ratingBarsWrapper}>
      <View style={styles.ratingBars}>
        <Text style={styles.reviewsBoxText}>{props.stars} stars</Text>
        <View style={styles.progressBar}>
          <View style={[styles.progressBarInner, { width: props.percent }]}>
            <LinearGradient
              // Button Linear Gradient
              colors={["#0F73EE", "#C644FC"]}
              start={{ x: 0.44, y: 0.1 }}
              end={{ x: 1.4, y: 2 }}
              style={styles.layer}
            />
          </View>
        </View>
        <Text style={styles.reviewsBoxText}>{props.percent}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  ratingBarsWrapper: {
    marginVertical: hp("0.5%"),
  },
  ratingBars: {
    flexDirection: "row",
    alignItems: "center",
  },
  progressBar: {
    width: "77%",
    height: hp("2%"),
    backgroundColor: "#F0F0F0",
    borderRadius: 100,
    marginHorizontal: 3,
    overflow: "hidden",
  },
  progressBarInner: {
    height: "100%",
    borderRadius: 100,
    overflow: "hidden",
  },
  layer: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
});
