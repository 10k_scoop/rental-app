import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from "expo-linear-gradient";
import RatingProgressBar from "./RatingProgressBar";
import ReviewCard from "./ReviewCard";

export default function ReviewsSection(props) {
  return (
    <View style={styles.container}>
      <View style={styles.reviewsWrapper}>
        <Text style={styles.text1}>RATINGS</Text>
        {/* Ratings Box */}
        <View style={styles.reviewsBoxWrapper}>
          <View style={styles.reviewsBox1}>
            <Text style={styles.reviewsBox1Text}>4.3</Text>
            <View style={styles.star}>
              <Image
                source={require("../../../assets/Images/Star.png")}
                resizeMode="cover"
                style={{
                  width: "100%",
                  height: "100%",
                }}
              />
            </View>
          </View>
          <View style={styles.reviewsBox}>
            <Text style={styles.reviewsBox1Text}>100%</Text>
            <Text style={styles.reviewsBoxText}>trips rated</Text>
          </View>
        </View>
        {/* Ratings Box */}

        {/* Ratings barss */}
        <View style={{ marginTop: hp("1%"), marginBottom: hp("3%") }}>
          <RatingProgressBar stars="5" percent="95%" />
          <RatingProgressBar stars="4" percent="75%" />
          <RatingProgressBar stars="3" percent="95%" />
          <RatingProgressBar stars="2" percent="5%" />
          <RatingProgressBar stars="1" percent="0%" />
        </View>
        {/* Ratings barss */}
        <Text style={styles.text1}>REVIEW DETAILS</Text>
        <View style={styles.reviewCardDetailsWrapper}>
          <ReviewCard />
          <ReviewCard
            comment=" An absolute awesome riding car. The pickup was quick and easy. Overall
        good experience"
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  reviewsWrapper: {
    width: "100%",
    height: hp("20%"),
  },
  text1: {
    fontFamily: "MM",
    fontSize: rf(13),
    color: "#404B69",
  },
  reviewsBoxWrapper: {
    flexDirection: "row",
    marginVertical: hp("1%"),
    alignItems: "center",
    justifyContent: "space-between",
  },
  reviewsBox1: {
    flexDirection: "row",
    flex: 1,
    height: hp("6%"),
    backgroundColor: "#fff",
    borderWidth: 1,
    borderRadius: 8,
    borderColor: "#DADADA",
    marginRight: wp("7%"),
    alignItems: "center",
    justifyContent: "center",
  },
  reviewsBox1Text: {
    fontFamily: "MS",
    fontSize: rf(22),
    color: "#404B69",
  },
  reviewsBox: {
    flexDirection: "row",
    flex: 1,
    height: hp("6%"),
    backgroundColor: "#fff",
    borderWidth: 1,
    borderRadius: 8,
    borderColor: "#DADADA",
    alignItems: "center",
    justifyContent: "center",
  },
  star: {
    width: 22,
    height: 22,
    marginHorizontal: 10,
  },
  reviewsBoxText: {
    fontFamily: "MM",
    fontSize: rf(10),
    color: "#404B69",
    marginHorizontal: 5,
  },
  ratingBarsWrapper: {
    marginVertical: hp("1%"),
    flex: 1,
  },
  ratingBars: {
    flexDirection: "row",
    alignItems: "center",
  },
  progressBar: {
    width: "77%",
    height: hp("2%"),
    backgroundColor: "#F0F0F0",
    borderRadius: 100,
    marginHorizontal: 3,
    overflow: "hidden",
  },
  progressBarInner: {
    height: "100%",
    borderRadius: 100,
    overflow: "hidden",
  },
  layer: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
});
