import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo } from "@expo/vector-icons";
import CarCard from "./CarCard";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from "react-native-chart-kit";

export default function EarningSection(props) {
  const data = {
    labels: ["January", "February", "March", "April", "May"],
    datasets: [
      {
        data: [20, 45, 28, 80, 99],
      },
    ],
  };
  const chartConfig = {
    backgroundGradientFrom: "#fff",
    backgroundGradientTo: "#fff",
    color: (opacity = 1) => "#222",
    strokeWidth: 0, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false, // optional
  };

  return (
    <View style={styles.container}>
      <View style={styles.topActionWrapper}>
        <View style={styles.action}>
          <Text style={styles.actionText}>Vehicles</Text>
          <Entypo name="chevron-down" size={rf(12)} color="#0C0D0D" />
        </View>
        <View style={styles.action}>
          <Text style={styles.actionText}>All Units</Text>
          <Entypo name="chevron-down" size={rf(12)} color="#0C0D0D" />
        </View>
        <View style={styles.action}>
          <Text style={styles.actionText}>Monthly</Text>
          <Entypo name="chevron-down" size={rf(12)} color="#0C0D0D" />
        </View>
      </View>

      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Text style={styles.earningText}>$120,297</Text>
        <Text style={styles.earningText2}>total earnings</Text>
      </View>

      <View style={styles.barImg}>
        <Image
          source={require("../../../assets/Images/Chart.png")}
          resizeMode="stretch"
          style={{
            width: "100%",
            height: "100%",
          }}
        />
      </View>
      <View style={styles.pastSellsWrapper}>
        <Text style={styles.pastSellsText}>PAST SALES</Text>
        {
          props.data?.map((item, index) => {
            return (
              <CarCard
                key={index}
                data={item}
                thumbnail={item.front}
                onPress={() => props.navigation.navigate("YourCar", { data: item })}
              />
            );
          })
        }
        <TouchableOpacity
          style={styles.payoutSettingsBtn}
          onPress={() => props.navigation.navigate("TransactionHistory")}
        >
          <Text style={styles.payoutSettingsText}>PAYOUT SETTINGS</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topActionWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: hp("2%"),
  },
  action: {
    flex: 1,
    height: hp("5%"),
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#DADADA",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    marginRight: 10,
    flexDirection: "row",
  },
  actionText: {
    fontFamily: "MR",
    fontSize: rf(12),
    color: "#0C0D0D",
    marginRight: 7,
  },
  earningText: {
    fontFamily: "MS",
    fontSize: rf(27),
    color: "#404B69",
  },
  earningText2: {
    fontFamily: "MM",
    fontSize: rf(13),
    color: "#404B69",
    marginLeft: 10,
  },
  pastSellsWrapper: {
    marginVertical: hp("2%"),
  },
  pastSellsText: {
    fontFamily: "MM",
    fontSize: rf(13),
    color: "#404B69",
    marginBottom: hp("1.5%"),
  },
  payoutSettingsBtn: {
    width: "100%",
    height: hp("6%"),
    backgroundColor: "#DADADA",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    marginVertical: hp("2%"),
  },
  payoutSettingsText: {
    fontFamily: "MM",
    fontSize: rf(14),
    color: "#404B69",
  },
  barImg: {
    width: "100%",
    height: hp("25%"),
    marginTop: hp("1%"),
  },
});
