import React from "react";
import { StyleSheet, Text, TextInput, View,TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
export default function SearchBar(props) {
  return (
    <View style={styles.container}>
      <View style={styles.seacrWrapper}>
        <Feather name="search" size={rf(15)} color="black" />
        <TextInput
          style={styles.searchTextField}
          placeholder="Try “1527t17” or “McLaren”"
          onChangeText={props.onChangeText}
        />
      </View>

      <TouchableOpacity style={styles.btnWrapper} onPress={props.onAddPress}>
        <LinearGradient
          // Button Linear Gradient
          colors={["#0F73EE", "#C644FC"]}
          style={styles.layer}
        />
        <AntDesign name="plus" size={rf(14)} color="#fff" />
        <Text style={styles.btnText}>Add</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  seacrWrapper: {
    width: "73%",
    height: hp("5%"),
    borderRadius: 15,
    borderWidth: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: "3%",
    borderColor: "#DADADA",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    backgroundColor: "#fff",
    elevation: 5,
  },
  searchTextField: {
    height: "90%",
    width: "90%",
    left: "5%",
  },
  btnWrapper: {
    width: "23%",
    height: hp("5%"),
    borderRadius: 15,
    borderWidth: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#DADADA",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    overflow: "hidden",
  },
  btnText: {
    fontSize: rf(13),
    fontFamily: "MS",
    color: "#fff",
    marginLeft: "8%",
  },
  layer: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
});
