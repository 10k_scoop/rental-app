import React from "react";
import { Image, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Entypo } from "@expo/vector-icons";

export default function HomesCard(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View style={styles.thumbnail}>
        <Image
          source={require("../../../assets/Images/selected4You1.jpg")}
          resizeMode="cover"
          style={{
            width: "100%",
            height: "100%",
          }}
        />
      </View>

      <View style={styles.info}>
        <Text style={styles.title}>650 Lakeview Ave</Text>
        <Text style={styles.text1}>San Diego, California</Text>
        <Text style={styles.text2}>5 bedroom | 5.5 bath</Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginVertical: 2,
          }}
        >
          <Text style={styles.text2}>4.3</Text>
          <View style={styles.star}>
            <Image
              source={require("../../../assets/Images/Star.png")}
              resizeMode="cover"
              style={{
                width: "100%",
                height: "100%",
              }}
            />
          </View>
          <Text style={styles.text2}>(7 trips)</Text>
        </View>

        <Text style={styles.text2}>Next trip: Oct 15 - 18</Text>
      </View>
      <View style={styles.arrow}>
        <Entypo name="chevron-right" size={24} color="black" />
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: hp("13%"),
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#DADADA",
    backgroundColor: "#fff",
    flexDirection: "row",
    marginBottom: hp("1%"),
    overflow: "hidden",
  },
  thumbnail: {
    width: "40%",
    height: "100%",
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    overflow: "hidden",
  },
  info: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  title: {
    fontSize: rf(14),
    fontFamily: "MB",
    color: "#404B69",
    marginBottom: 5,
  },
  text1: {
    fontSize: rf(10),
    fontFamily: "MM",
    color: "#404B69",
    marginBottom: 5,
  },
  text2: {
    fontSize: rf(12),
    fontFamily: "MR",
    color: "#404B69",
    marginVertical: 2,
  },
  star: {
    width: 10,
    height: 10,
    marginHorizontal: 5,
  },
  arrow: {
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 5,
  },
});
