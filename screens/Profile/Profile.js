import React from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import CardComponent2 from "./components/CardComponent2";
import { FeaturedListData } from "../../constants/FeaturedListData";
import { LinearGradient } from "expo-linear-gradient";

export default function Profile(props) {
  return (
    <View style={styles.container}>
      <Header navigation={props.navigation} />

      <View style={styles.sectionWrapper}>
        <ScrollView>
          <View style={styles.thumbnail}>
            <Image
              source={require("../../assets/Images/profile.png")}
              resizeMode="cover"
              style={{ width: "100%", height: "100%" }}
            />
          </View>

          <View style={styles.infoSection}>
            <Text style={styles.infoSectionTitle}>Exotic Motors</Text>
            {/* Review Section */}
            <View style={styles.reviewSection}>
              <Text style={styles.reviewSectionTitle}>Customer Reviews</Text>

              <View style={styles.reviewSectionRow}>
                <Text style={styles.reviewSectionRowTitle}>Cleanliness</Text>
                <Text style={styles.reviewSectionRowRating}>4.2</Text>
                <Image
                  source={require("../../assets/Images/Star.png")}
                  resizeMode="contain"
                  style={{ width: 15, height: 15 }}
                />
              </View>

              <View style={styles.reviewSectionRow}>
                <Text style={styles.reviewSectionRowTitle}>Friendliness</Text>
                <Text style={styles.reviewSectionRowRating}>4.3</Text>
                <Image
                  source={require("../../assets/Images/Star.png")}
                  resizeMode="contain"
                  style={{ width: 15, height: 15 }}
                />
              </View>

              <View style={styles.reviewSectionRow}>
                <Text style={styles.reviewSectionRowTitle}>Responsiveness</Text>
                <Text style={styles.reviewSectionRowRating}>4.8</Text>
                <Image
                  source={require("../../assets/Images/Star.png")}
                  resizeMode="contain"
                  style={{ width: 15, height: 15 }}
                />
              </View>
              <View style={styles.reviewSectionRow}>
                <Text style={styles.reviewSectionRowTitle}>
                  Would Rent Again
                </Text>
                <Text style={styles.reviewSectionRowRating}>4.2</Text>
                <Image
                  source={require("../../assets/Images/Star.png")}
                  resizeMode="contain"
                  style={{ width: 15, height: 15 }}
                />
              </View>
              <View style={styles.reviewSectionRow}>
                <Text style={styles.reviewSectionRowTitle}>Selection</Text>
                <Text style={styles.reviewSectionRowRating}>4.1</Text>
                <Image
                  source={require("../../assets/Images/Star.png")}
                  resizeMode="contain"
                  style={{ width: 15, height: 15 }}
                />
              </View>
            </View>
            {/* Review Section */}

            {/* Location Section */}
            <View style={styles.locationSection}>
              <Text style={styles.reviewSectionTitle}>Location</Text>
              <Text style={styles.locationSectionTagline}>Brooklyn, NY</Text>

              <View style={styles.mapWrapper}>
                <Image
                  source={require("../../assets/Images/profileMap.png")}
                  resizeMode="cover"
                  style={{ width: "100%", height: "100%" }}
                />
              </View>
            </View>
            {/* Location Section */}
            <View style={styles.viewAllWrapper}>
              <Text style={styles.reviewSectionTitle}>
                View All from Exotic Motors
              </Text>
              <View style={styles.cardIndicator}>
                <LinearGradient
                  // Button Linear Gradient
                  colors={["#0F73EE", "#C644FC"]}
                  start={{ x: 0.44, y: 0.1 }}
                  end={{ x: 1.4, y: 2 }}
                  style={styles.cardIndicatorlayer}
                />
              </View>
              <View style={styles.cardWrapperMain}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {FeaturedListData.map((item, index) => {
                    return (
                      <CardComponent2
                        onPress={() => props.navigation.navigate("ListView")}
                        data={item}
                        key={index}
                      />
                    );
                  })}
                </ScrollView>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
      <View style={styles.BottomButtonWrapper}>
        <TouchableOpacity
          style={styles.doneBtnWrapper}
          onPress={() => props.navigation.navigate("Message")}
        >
          <LinearGradient
            // Button Linear Gradient
            colors={["#0F73EE", "#C644FC"]}
            // start={{ x: 0.44, y: 0.1 }}
            end={{ x: 1.4, y: 2 }}
            style={styles.doneBtn}
          >
            <Text style={styles.doneBtnText}>REQUEST CHAT</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  sectionWrapper: {
    flex: 1,
  },
  thumbnail: {
    width: "100%",
    height: hp("28%"),
  },
  infoSection: {
    width: "100%",
    flex: 1,
    backgroundColor: "white",
    borderTopRightRadius: 23,
    borderTopLeftRadius: 23,
    bottom: hp("5%"),
    paddingHorizontal: wp("5%"),
  },
  infoSectionTitle: {
    fontSize: rf(20),
    fontFamily: "MS",
    color: "#020433",
    textAlign: "center",
    marginTop: hp("3%"),
  },
  reviewSection: {
    marginTop: hp("2%"),
  },
  reviewSectionTitle: {
    fontSize: rf(15),
    fontFamily: "MS",
    color: "#020433",
    marginBottom: hp("1%"),
  },
  reviewSectionRow: {
    flexDirection: "row",
    marginVertical: hp("1%"),
    alignItems: "center",
  },
  reviewSectionRowTitle: {
    fontSize: rf(13),
    fontFamily: "MR",
    color: "#020433",
    flex: 1,
  },
  reviewSectionRowRating: {
    fontSize: rf(15),
    fontFamily: "MS",
    color: "#404B69",
    right: 10,
  },
  locationSection: {
    marginTop: hp("2%"),
  },
  locationSectionTagline: {
    fontSize: rf(11),
    fontFamily: "MR",
    color: "#020433",
  },
  mapWrapper: {
    width: wp("100%"),
    height: hp("30%"),
    right: wp("5%"),
    marginTop: hp("2%"),
  },
  viewAllWrapper: {
    marginTop: hp("3%"),
  },
  cardIndicator: {
    width: wp("52%"),
    height: 4,
    marginLeft: wp("3%"),
  },
  cardIndicatorlayer: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },

  cardWrapperMain: {
    width: wp("100%"),
    right: wp("3%"),
  },
  doneBtnWrapper: {
    justifyContent: "flex-end",
    paddingHorizontal: wp("5%"),
    alignItems: "center",
  },
  doneBtn: {
    width: "100%",
    height: hp("6%"),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  doneBtnText: {
    fontFamily: "MM",
    fontSize: rf(14),
    color: "#fff",
  },
  BottomButtonWrapper: {
    backgroundColor: "#FFFFFF",
    height: hp("10%"),
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
