import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Home from "../screens/Home/Home";
import VerificationCode from "../screens/VerificationCode/VerificationCode";
import ListView from "../screens/ListView/ListView";
import Map from "../screens/Map/Map";
import CreateProfile from "../screens/CreateProfile/CreateProfile";
import Filter from "../screens/Filter/Filter";
import CarDetails from "../screens/CarDetails/CarDetails";
import HouseDetails from "../screens/HouseDetails/HouseDetails";
import Search from "../screens/Search/Search";
import Type from "../screens/Type/Type";
import SearchDate from "../screens/SearchDate/SearchDate";
import Booked from "../screens/Booked/Booked";
import Profile from "../screens/Profile/Profile";
import Settings from "../screens/Settings/Settings";
import Saved from "../screens/Saved/Saved";
import HostCenter from "../screens/HostCenter/HostCenter";
import HostBooking from "../screens/HostBookings/HostBookings";
import TripDetails from "../screens/TripDetails/TripDetails";
import YourCar from "../screens/YourCar/YourCar";
import PricingAndDiscount from "../screens/PricingAndDiscount/PricingAndDiscount";
import TransactionHistory from "../screens/TransactionHistory/TransactionHistory";
import Message from "../screens/Message/Message";
import AddPaymentMethod from "../screens/PaymentMethod/AddPaymentMethod";
import LoadingScreen from "../screens/LoadingScreen";
import PostACar from "../screens/PostACar/PostACar";

const { Navigator, Screen } = createStackNavigator();

function AppNavigation() {
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      <Screen name="LoadingScreen" component={LoadingScreen} />
      <Screen name="Home" component={Home} />
      <Screen name="VerificationCode" component={VerificationCode} />
      <Screen name="ListView" component={ListView} />
      <Screen name="Map" component={Map} />
      <Screen name="CreateProfile" component={CreateProfile} />
      <Screen name="Filter" component={Filter} />
      <Screen name="CarDetails" component={CarDetails} />
      <Screen name="HouseDetails" component={HouseDetails} />
      <Screen name="Search" component={Search} />
      <Screen name="Type" component={Type} />
      <Screen name="SearchDate" component={SearchDate} />
      <Screen name="Booked" component={Booked} />
      <Screen name="Profile" component={Profile} />
      <Screen name="Settings" component={Settings} />
      <Screen name="Saved" component={Saved} />
      <Screen name="HostCenter" component={HostCenter} />
      <Screen name="HostBooking" component={HostBooking} />
      <Screen name="TripDetails" component={TripDetails} />
      <Screen name="YourCar" component={YourCar} />
      <Screen name="PricingAndDiscount" component={PricingAndDiscount} />
      <Screen name="TransactionHistory" component={TransactionHistory} />
      <Screen name="Message" component={Message} />
      <Screen name="AddPaymentMethod" component={AddPaymentMethod} />
      <Screen name="PostACar" component={PostACar} />
    </Navigator>
  );
}
export const AppNavigator = () => (
  <NavigationContainer>
    <AppNavigation />
  </NavigationContainer>
);
