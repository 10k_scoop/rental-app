import React, { useEffect } from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import { AppNavigator } from "./routes/AppNavigator";
import { useFonts } from "expo-font";
import { Provider } from "react-redux";
import store from "./state-management/store";
import * as firebase from "firebase";
import { firebaseConfig } from "./state-management/actions/env";

export default function App() {
  let [fontsLoaded] = useFonts({
    MB: require("./assets/fonts/Montserrat-Bold.ttf"),
    MM: require("./assets/fonts/Montserrat-Medium.ttf"),
    MR: require("./assets/fonts/Montserrat-Regular.ttf"),
    MS: require("./assets/fonts/Montserrat-SemiBold.ttf"),
  });

  useEffect(() => {
    try {
      if (firebase.apps.length === 0) {
        firebase.initializeApp(firebaseConfig);
      }
    } catch (error) {
      alert("Check you internet connection!!");
    }
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        var uid = user.uid;
      } else {
        // User is signed out
        // ...
      }
    });
  }, []);

  if (!fontsLoaded) {
    return (
      <ActivityIndicator
        size="large"
        color="#222"
        style={{ alignSelf: "center" }}
      />
    );
  }

  return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  );
}
