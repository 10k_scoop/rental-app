export const colors = [
  {
    gradient: ["#0F73EE", "#C644FC"],
    primary: "#0F73EE",
    secondary: "#C644FC",
    textMedium: "#404B69",
    textLight: "#91959b",
    textDark: "#020433",
  },
];
