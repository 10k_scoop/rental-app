export const SearchData = [
  {
    title: "Los Angeles",
    image: require("../assets/Images/explore1.png"),
    tagline: "3 hour drive",
  },
  {
    title: "San Francisco",
    image: require("../assets/Images/selected4You2.jpg"),
    tagline: "4 hour drive",
  },
  {
    title: "New York",
    image: require("../assets/Images/selected4You1.jpg"),
    tagline: "4 hour drive",
  },
];
