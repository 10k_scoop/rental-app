export const FeaturedListData = [
  {
    verified: true,
    title: "McLaren 720S",
    thumbnail: require("../assets/Images/img1.jpg"),
    price: "$550 per day",
    model: "2021 - Special Edition",
    listImages:[require("../assets/Images/img1.jpg"),require("../assets/Images/img2.jpg")]
  },
  {
    verified: true,
    title: "Bentley Bentayga",
    thumbnail: require("../assets/Images/img2.jpg"),
    price: "$5750 per day",
    model: "2020 - Special Edition",
    listImages:[require("../assets/Images/img2.jpg"),require("../assets/Images/img1.jpg")]
  },
];
