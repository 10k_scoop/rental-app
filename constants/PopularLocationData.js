export const PopularLocationData = [
  {
    country: "USA",
    city: "New York",
    thumbnail: require("../assets/Images/popular2.png"),
    type: "newyork",
  },
  {
    country: "USA",
    city: "Los Angeles",
    thumbnail: require("../assets/Images/popular1.png"),
    type: "los",
  },
  {
    country: "USA",
    city: "San Francisco",
    thumbnail: require("../assets/Images/popular3.png"),
    type: "San",
  },
];
