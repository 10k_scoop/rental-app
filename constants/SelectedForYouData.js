export const SelectedForYouData = [
  {
    category: "Beach houses",
    title: "650 Lakeview Ave",
    beds: "5 bedroom | 5.5 bathroom",
    thumbnail: require("../assets/Images/selected4You1.jpg"),
    price: "$540 per day",
    listImages:[require("../assets/Images/selected4You1.jpg"),require("../assets/Images/selected4You2.jpg")]
  },
  {
    category: "Large Event",
    title: "Large Event",
    title: "650 Lakeview Ave",
    beds: "5 bedroom | 5.5 bathroom",
    thumbnail: require("../assets/Images/selected4You2.jpg"),
    price: "$690 per day",
    listImages:[require("../assets/Images/selected4You2.jpg"),require("../assets/Images/selected4You1.jpg")]
  },
];
