import React, { useEffect, useState } from "react";
import {
  Platform,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { colors } from "../constants/colors";
import {
  Entypo,
  Feather,
  FontAwesome,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import * as firebase from "firebase";
import { connect } from "react-redux";
import { getUserDetails } from "../state-management/actions/Features/Actions";
import Popup from "./Popup";
const BottomMenu = (props) => {
  const [userData, setUserData] = useState();
  const [popup, setPopup] = useState(false);

  useEffect(() => {
    setUserData(props.user);
  }, []);

  const onHostPress = () => {
    if (props.user) {
      if (props.userData?.data?.accountType == "host") {
        props.navigation.navigate("HostCenter");
      } else {
        props.navigation.navigate("HostCenter");
      }
    } else {
      props.navigation.replace("Home");
    }
  };

  return (
    <View style={styles.container}>
      <Popup
        icon={<Entypo name="emoji-sad" size={24} color="white" />}
        msg="Please contact support@aspectlifestyle.com for help becoming a host."
        visible={popup}
        onPress={() => setPopup(false)}
      />
      <TouchableOpacity
        style={styles.iconRow}
        onPress={() => props.navigation.navigate("Home")}
      >
        <Feather
          name="home"
          size={24}
          color={props.activeItem == "Home" ? "#C644FC" : "#404B69"}
        />
        <Text
          style={[
            styles.iconText,
            { color: props.activeItem == "Home" ? "#404B69" : "#E5E5E5" },
          ]}
        >
          Home
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.iconRow}
        onPress={() =>
          props.user
            ? props.navigation.navigate("Saved")
            : props.navigation.replace("Home")
        }
      >
        <FontAwesome
          name="star-o"
          size={24}
          color={props.activeItem == "Saved" ? "#C644FC" : "#404B69"}
        />
        <Text
          style={[
            styles.iconText,
            { color: props.activeItem == "Saved" ? "#404B69" : "#E5E5E5" },
          ]}
        >
          Saved
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.iconRow}
        onPress={() =>
          props.user
            ? props.navigation.navigate("HostBooking")
            : props.navigation.replace("Home")
        }
      >
        <Ionicons
          name="newspaper-outline"
          size={24}
          color={props.activeItem == "Bookings" ? "#C644FC" : "#404B69"}
        />
        <Text
          style={[
            styles.iconText,
            { color: props.activeItem == "Bookings" ? "#404B69" : "#E5E5E5" },
          ]}
        >
          Bookings
        </Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.iconRow} onPress={() => onHostPress()}>
        <MaterialCommunityIcons
          name="key-outline"
          size={24}
          color={props.activeItem == "Host" ? "#C644FC" : "#404B69"}
        />
        <Text
          style={[
            styles.iconText,
            { color: props.activeItem == "Host" ? "#404B69" : "#E5E5E5" },
          ]}
        >
          Host
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.iconRow}
        onPress={() => props.navigation.replace("Settings")}
      >
        <MaterialCommunityIcons
          name="cog-outline"
          size={24}
          color={props.activeItem == "Settings" ? "#C644FC" : "#404B69"}
        />
        <Text
          style={[
            styles.iconText,
            { color: props.activeItem == "Settings" ? "#404B69" : "#E5E5E5" },
          ]}
        >
          Settings
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F9F9FB",
    width: wp("100%"),
    height: Platform.OS == "ios" ? hp("8%") : hp("7%"),
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "row",
    position: "absolute",
    paddingBottom: Platform.OS === "ios" ? 10 : 5,
    bottom: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  iconRow: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  iconText: {
    fontSize: rf(10),
    fontFamily: "MB",
    color: "#404B69",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { getUserDetails })(BottomMenu);
