import React from "react";
import {
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{ flex: 0.8, alignItems: "flex-start", left: 10 }}
        onPress={() => props.navigation.goBack()}
      >
        <AntDesign name="arrowleft" size={rf(25)} color="black" />
      </TouchableOpacity>
      {props.title && <Text style={styles.title}>{props.title}</Text>}
      <View style={styles.gradientBorder}>
        <LinearGradient
          // Button Linear Gradient
          colors={["#0F73EE", "#C644FC"]}
          style={styles.gradientBorder}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    width: wp("100%"),
    height: (Platform.OS = "ios" ? hp("8%") : hp("7%")),
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    marginTop: (Platform.OS = "ios"
      ? StatusBar.currentHeight + hp("3%")
      : StatusBar.currentHeight + hp("1%")),
  },
  gradientBorder: {
    width: "100%",
    height: 2,
    backgroundColor: "red",
    position: "absolute",
    bottom: 0,
  },
  title: {
    fontFamily: "MM",
    fontSize: rf(15),
    color: "#020433",
    width: "80%",
    textAlign: "center",
    right: wp("6%"),
  },
});
