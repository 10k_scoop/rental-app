import React from "react";
import { View, StyleSheet, SafeAreaView, TouchableOpacity } from "react-native";
import Feather from "react-native-vector-icons/Feather";
import colors from "../assets/colors/colors";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Entypo } from "@expo/vector-icons";
export default function BackButton(props) {
  return (
    <View style={styles.headerWrapper}>
      <TouchableOpacity
        style={[
          styles.backButton,
          {
            marginHorizontal: props.marginL ? wp("4%") : 0,
            backgroundColor: props.dark ? "#222" : "transparent",
            borderColor: props.dark ? "#fff" : "#CDCDCD",
          },
        ]}
        onPress={() => props.navigation.goBack()}
      >
        <Feather
          name="chevron-left"
          size={rf(15)}
          color={props.dark ? colors.white : colors.textDark}
        />
      </TouchableOpacity>
      {props.special && (
        <View style={styles.star}>
          <Entypo name="star" size={rf(13)} color="#fff" />
        </View>
      )}
    </View>
  );
}
const styles = StyleSheet.create({
  backButton: {
    width: wp("10%"),
    height: wp("10%"),
    borderWidth: 1,
    borderRadius: 7,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#CDCDCD",
    marginTop: hp("1%"),
  },
  headerWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  star: {
    width: wp("10%"),
    height: wp("10%"),
    borderRadius: 7,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: wp("4%"),
    marginTop: hp("1%"),
    backgroundColor: "#F5CA48",
  },
});
