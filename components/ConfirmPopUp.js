import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Ionicons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
export default function ConfirmPopUp(props) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={{ width: "100%", alignItems: "flex-end" }}>
          <TouchableOpacity onPress={props.onClosePress}>
            <Ionicons name="close-sharp" size={24} color="black" />
          </TouchableOpacity>
        </View>
        <Text style={styles.headerTitle}>
          Book ‘{props.data.title}’ from{"\n"} Exotic Motors?
        </Text>
      </View>

      <View style={styles.thumbnailWrapper}>
        <View style={styles.thumbnail}>
          <Image
            source={
              props.data?.thumbnail
                ? props.data.thumbnail
                : require("../assets/Images/cardetail.png")
            }
            resizeMode="cover"
            style={{ width: "100%", height: "100%" }}
          />
        </View>
      </View>

      <View style={styles.bookingDetails}>
        <View style={styles.bookingDetailsRow}>
          <Text style={styles.bookingDetailsTextLeft}>Booking amount</Text>
          <Text style={styles.bookingDetailsTextRight}>$650</Text>
        </View>

        <View style={styles.bookingDetailsRow}>
          <Text style={styles.bookingDetailsTextLeft}>Service free</Text>
          <Text style={[styles.bookingDetailsTextRight, { color: "#0F73EE" }]}>
            Free
          </Text>
        </View>

        <View style={styles.bookingDetailsRow}>
          <Text style={styles.bookingDetailsTextLeft}>Add ons</Text>
          <Text style={styles.bookingDetailsTextRight}>$250</Text>
        </View>

        <View style={styles.divider}></View>

        <View style={styles.bookingDetailsRow}>
          <Text
            style={[
              styles.bookingDetailsTextLeft,
              { fontSize: rf(13), fontFamily: "MM" },
            ]}
          >
            Booking amount
          </Text>
          <Text style={[styles.bookingDetailsTextRight, { fontSize: rf(15) }]}>
            $950
          </Text>
        </View>
      </View>

      <TouchableOpacity
        style={styles.buttomButton1}
        onPress={props.onBookPress}
      >
        <LinearGradient
          // Button Linear Gradient
          colors={["#0F73EE", "#C644FC"]}
          // start={{ x: 0.44, y: 0.1 }}
          end={{ x: 1.4, y: 2 }}
          style={styles.gradientLayer}
        />
        <Text style={styles.buttomButton1Text}>BOOK NOW</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    width: wp("85%"),
    height: hp("61%"),
    borderRadius: 20,
    zIndex: 99999999999,
    padding: 20,
  },
  headerTitle: {
    fontSize: rf(16),
    fontFamily: "MB",
    color: "#020433",
    textAlign: "center",
  },
  thumbnailWrapper: {
    width: "100%",
    height: hp("17%"),
    alignItems: "center",
    marginTop: hp("2%"),
  },
  thumbnail: {
    width: "77%",
    height: "82%",
    borderRadius: 15,
    overflow: "hidden",
  },
  bookingDetails: {
    width: "95%",
    height: hp("20%"),
    borderRadius: 10,
    backgroundColor: "#F7F9FF",
    marginHorizontal: "2.5%",
    padding: 20,
  },
  bookingDetailsRow: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginVertical: 7,
  },
  bookingDetailsTextLeft: {
    fontSize: rf(12.5),
    fontFamily: "MR",
    color: "#404B69",
  },
  bookingDetailsTextRight: {
    fontSize: rf(12.5),
    fontFamily: "MB",
    color: "#020433",
  },
  divider: {
    width: "100%",
    height: 1,
    backgroundColor: "#F0F0F0",
    marginVertical: 10,
  },
  buttomButton1: {
    width: "95%",
    height: hp("6%"),
    backgroundColor: "red",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    marginHorizontal: "2.5%",
    marginTop: "8%",
  },
  buttomButton1Text: {
    fontFamily: "MS",
    fontSize: rf(12),
    color: "#FFFFFF",
  },
  gradientLayer: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
});
