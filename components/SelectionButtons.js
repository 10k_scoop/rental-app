import { AntDesign, Entypo, FontAwesome5 } from "@expo/vector-icons";
import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function SelectedButtons(props) {
  return (
    <View style={styles.selectionButtons}>
      <TouchableOpacity
        style={[
          styles.selectionCreditButton,
          { zIndex: props.selectedOption == "Entry" ? 9999 : -1 },
        ]}
        onPress={() => props.onCreditCardPress()}
      >
        <View
          style={[
            styles.selectionCreditButtonLayer,
            {
              backgroundColor:
                props.selectedOption == "Entry" ? "#F5CA48" : "#D8DBDD",
              borderTopRightRadius: props.selectedOption == "Entry" ? 25 : 0,
              borderBottomRightRadius: props.selectedOption == "Entry" ? 25 : 0,
              opacity: props.selectedOption == "Entry" ? 1 : 0.8,
            },
          ]}
        ></View>
        <FontAwesome5
          name="tag"
          size={rf(15)}
          color={props.selectedOption == "Entry" ? "#fff" : "#3A3C3F"}
        />
        <Text
          style={[
            styles.selectionCreditButtonText,
            {
              color: props.selectedOption == "Entry" ? "#fff" : "#3A3C3F",
            },
          ]}
        >
          Entry
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          styles.selectionApplePayButton,
          { zIndex: props.selectedOption == "Tables" ? 9999 : -1 },
        ]}
        onPress={() => props.onApplePayPress()}
      >
        <View
          style={[
            styles.selectionApplePayButtonLayer,
            {
              backgroundColor:
                props.selectedOption == "Tables" ? "#F5CA48" : "#D8DBDD",
              borderTopLeftRadius: props.selectedOption == "Tables" ? 25 : 0,
              borderBottomLeftRadius: props.selectedOption == "Tables" ? 25 : 0,
              opacity: props.selectedOption == "Tables" ? 1 : 0.8,
            },
          ]}
        ></View>
        <FontAwesome5
          name="users"
          size={rf(15)}
          color={props.selectedOption == "Tables" ? "#fff" : "#3A3C3F"}
        />
        <Text
          style={[
            styles.selectionApplePayButtonText,
            {
              color: props.selectedOption == "Tables" ? "#fff" : "#3A3C3F",
            },
          ]}
        >
          Tables
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  selectionButtons: {
    flexDirection: "row",
    position: "absolute",
    bottom: "-12%",
    alignSelf: "center",
    shadowColor: "#F26C68",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    alignItems: "center",
    justifyContent: "center",
    right: 20,
  },
  selectionCreditButton: {
    width: "48%",
    height: hp("8%"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
  },
  selectionCreditButtonText: {
    fontFamily: "Montserrat-Bold",
    fontSize: rf(15),
    color: "#fff",
    marginLeft: 10,
  },
  selectionApplePayButton: {
    width: "48%",
    height: hp("8%"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    right: 20,
    zIndex: -1,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    overflow: "hidden",
  },
  selectionApplePayButtonText: {
    fontFamily: "Montserrat-Bold",
    fontSize: rf(18),
    color: "#3A3C3F",
    marginLeft: 10,
  },
  selectionApplePayButtonLayer: {
    width: "100%",
    height: "100%",
    position: "absolute",
    backgroundColor: "#D8DBDD",
    borderRadius: 20,
    opacity: 0.4,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
  },
  selectionCreditButtonLayer: {
    width: "100%",
    height: "100%",
    position: "absolute",
    backgroundColor: "#D8DBDD",
    borderRadius: 25,
    opacity: 0.4,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
});
