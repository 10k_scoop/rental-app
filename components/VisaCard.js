import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Ionicons } from "@expo/vector-icons";

export default function VisaCard(props) {
  return (
    <View style={[styles.visacard, { backgroundColor: props.bgColor }]}>
      <View style={styles.visaCardLogo}>
        <Image source={require("../assets/visa.png")} />
      </View>
      <View style={styles.visaCardNumberWrapper}>
        <Text style={styles.visaCardNumber}>* * * *</Text>
        <Text style={styles.visaCardNumber}>* * * *</Text>
        <Text style={styles.visaCardNumber}>* * * *</Text>
        <Text style={styles.visaCardNumber}>{props.last4?props.last4:'****'}</Text>
      </View>
      <View style={styles.visaCardDetailsWrapper}>
        <View style={styles.visaCardDetails1}>
          <Text style={[styles.visaCardDetailstitle, { textAlign: "left" }]}>
            Card Holder
          </Text>
          <Text style={styles.visaCardDetailsName}>{props.data?props.data.name:'None'}</Text>
        </View>
        <View style={styles.visaCardDetails2}>
          <Text style={styles.visaCardDetailstitle}>Expires</Text>
          <Text style={styles.visaCardDetailsName}>{props.expiry?props.expiry:'None'}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  visacard: {
    width: wp("85%"),
    height: hp("25%"),
    backgroundColor: "#8688BC",
    borderRadius: 15,
    padding: 10,
    justifyContent: "flex-start",
    alignItems: "center",
    marginHorizontal: wp("7%"),
    paddingVertical: hp("4%"),
  },

  visaCardLogo: {
    width: "100%",
    paddingHorizontal: wp("4%"),
    height: "40%",
  },
  visaCardNumberWrapper: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  visaCardNumber: {
    fontSize: rf(15),
    fontFamily: "MR",
    color: "#FFFFFF",
  },
  visaCardDetailsWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    paddingHorizontal: wp("5%"),
    marginTop: "10%",
  },
  visaCardDetailstitle: {
    fontSize: rf(8),
    fontFamily: "MR",
    color: "#FFFFFF",
    marginVertical: 2,
    textAlign: "right",
  },
  visaCardDetailsName: {
    fontSize: rf(13),
    fontFamily: "MR",
    color: "#FFFFFF",
  },
});
